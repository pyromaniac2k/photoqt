<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de" sourcelanguage="en">
  <context>
    <name>MainMenu</name>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="168"/>
      <source>navigation</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>Navigation</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="187"/>
      <source>previous</source>
      <extracomment>as in: PREVIOUS image. Please keep short.</extracomment>
      <translation>vorheriges</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="199"/>
      <source>next</source>
      <extracomment>as in: NEXT image. Please keep short.</extracomment>
      <translation>nächstes</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="214"/>
      <source>first</source>
      <extracomment>as in: FIRST image. Please keep short.</extracomment>
      <translation>erstes</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="223"/>
      <source>last</source>
      <extracomment>as in: LAST image. Please keep short.</extracomment>
      <translation>letztes</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="233"/>
      <source>Browse images</source>
      <translation>Bilder durchsuchen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="242"/>
      <source>Map Explorer</source>
      <translation>Kartenansicht</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="264"/>
      <source>current image</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>Aktuelles Bild</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="291"/>
      <source>Zoom</source>
      <extracomment>Entry in main menu. Please keep short.</extracomment>
      <translation>Zoom</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="326"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="374"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="422"/>
      <source>reset</source>
      <extracomment>Used as in RESET zoom.
----------
Used as in RESET rotation.
----------
Used as in RESET mirroring.</extracomment>
      <translation>zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="348"/>
      <source>Rotation</source>
      <extracomment>Entry in main menu. Please keep short.</extracomment>
      <translation>Drehung</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="396"/>
      <source>Mirror</source>
      <extracomment>Mirroring (or flipping) an image. Please keep short.</extracomment>
      <translation>Spiegeln</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="434"/>
      <source>Hide histogram</source>
      <translation>Histogramm verstecken</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="434"/>
      <source>Show histogram</source>
      <translation>Histogramm anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="440"/>
      <source>Hide current location</source>
      <translation>Aktuellen Standort ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="440"/>
      <source>Show current location</source>
      <translation>Aktuellen Standort anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="463"/>
      <source>all images</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>Alle Bilder</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="490"/>
      <source>Slideshow</source>
      <extracomment>Entry in main menu. Please keep short.</extracomment>
      <translation>Diaschau</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="500"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="543"/>
      <source>Start</source>
      <extracomment>Used as in START slideshow. Please keep short
----------
Used as in START advanced sort. Please keep short</extracomment>
      <translation>Start</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="510"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="553"/>
      <source>Setup</source>
      <extracomment>Used as in SETUP slideshow. Please keep short
----------
Used as in SETUP advanced sort. Please keep short</extracomment>
      <translation>Einrichten</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="533"/>
      <source>Sort</source>
      <extracomment>Entry in main menu. Please keep short.</extracomment>
      <translation>Sortieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="566"/>
      <source>Filter images</source>
      <translation>Dateien filtern</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="575"/>
      <source>Streaming (Chromecast)</source>
      <translation>Streaming (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="583"/>
      <source>Open in default file manager</source>
      <translation>Im Dateimanager öffnen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="606"/>
      <source>general</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>Allgemein</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="623"/>
      <source>Settings</source>
      <translation>Einstellungen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="631"/>
      <source>About</source>
      <translation>Über</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="643"/>
      <source>Online help</source>
      <translation>Online-Hilfe</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="651"/>
      <source>Quit</source>
      <translation>Beenden</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="677"/>
      <source>custom</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>Benutzerdefiniert</translation>
    </message>
  </context>
  <context>
    <name>PQCImageFormats</name>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="66"/>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="79"/>
      <source>ERROR getting default image formats</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>FEHLER beim Erhalten der Standard-Bildformate</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="67"/>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="80"/>
      <source>Not even a read-only version of the database of default image formats could be opened.</source>
      <translation>Nicht einmal eine schreibgeschützte Version der Datenbank mit den Standard-Bildformaten konnte geöffnet werden.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="67"/>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="80"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Irgendetwas ist schrecklich schiefgelaufen!</translation>
    </message>
  </context>
  <context>
    <name>PQCScriptsMetaData</name>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="64"/>
      <source>yes</source>
      <extracomment>This string identifies that flash was fired, stored in image metadata</extracomment>
      <translation>ja</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="66"/>
      <source>no</source>
      <extracomment>This string identifies that flash was not fired, stored in image metadata</extracomment>
      <translation>nein</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="68"/>
      <source>No flash function</source>
      <extracomment>This string refers to the absense of a flash, stored in image metadata</extracomment>
      <translation>Keine Blitzfunktion</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="70"/>
      <source>strobe return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Stroboskop-Lichtreflexion nicht erkannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="72"/>
      <source>strobe return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Stroboskop-Lichtreflexion erkannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="74"/>
      <source>compulsory flash mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>obligatorischer Blitzmodus</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="76"/>
      <source>auto mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>automatisch</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="78"/>
      <source>red-eye reduction mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Rote-Augen-Reduktionsmodus</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="80"/>
      <source>return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Lichtreflexion erkannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="82"/>
      <source>return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Lichtreflexion nicht erkannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="130"/>
      <source>Invalid flash</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Ungültiger Blitz</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="140"/>
      <source>Standard</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Standard</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="143"/>
      <source>Landscape</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Querformat</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="146"/>
      <source>Portrait</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Hochformat</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="149"/>
      <source>Night Scene</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Nachtszene</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="152"/>
      <source>Invalid Scene Type</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Ungültiger Szenentyp</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="202"/>
      <source>Unknown</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Unbekannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="205"/>
      <source>Daylight</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Tageslicht</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="208"/>
      <source>Fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluoreszierend</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="211"/>
      <source>Tungsten (incandescent light)</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Tungsten (Glühbirne)</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="214"/>
      <source>Flash</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Blitz</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="217"/>
      <source>Fine weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Heiteres Wetter</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="220"/>
      <source>Cloudy Weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Bewölktes Wetter</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="223"/>
      <source>Shade</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Schatten</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="226"/>
      <source>Daylight fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluoreszierendes Tageslicht</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="229"/>
      <source>Day white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Weißes fluoreszierendes Tageslicht</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="232"/>
      <source>Cool white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Weißes fluoreszierendes kaltes Licht</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="235"/>
      <source>White fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Weißes fluoreszierendes Licht</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="238"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="241"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="244"/>
      <source>Standard light</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Standard-Licht</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="257"/>
      <source>Other light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Andere Lichtquelle</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="260"/>
      <source>Invalid light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Ungültige Lichtquelle</translation>
    </message>
  </context>
  <context>
    <name>PQCScriptsOther</name>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsother.cpp" line="83"/>
      <source>Print</source>
      <translation>Drucken</translation>
    </message>
  </context>
  <context>
    <name>PQCStartup</name>
    <message>
      <location filename="../cplusplus/other/pqc_startup.cpp" line="47"/>
      <source>SQLite error</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>SQLite Fehler</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_startup.cpp" line="48"/>
      <source>You seem to be missing the SQLite driver for Qt. This is needed though for a few different things, like reading and writing the settings. Without it, PhotoQt cannot function!</source>
      <translation>Der Qt Treiber für SQLite scheint zu fehlen. Dieser wird benötigt für ein paar verschiedene Dinge, z. B. das Lesen und Schreiben der Einstellungen. Ohne ihn kann PhotoQt nicht funktionieren!</translation>
    </message>
  </context>
  <context>
    <name>PQSettings</name>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="72"/>
      <source>ERROR getting database with default settings</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>FEHLER beim Abrufen der Datenbank mit Standardeinstellungen</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="73"/>
      <source>I tried hard, but I just cannot open even a read-only version of the settings database.</source>
      <translation>Ich habe alles versucht, aber ich kann nicht einmal eine schreibgeschützte Version der Einstellungsdatenbank öffnen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="73"/>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="85"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Irgendetwas ist schrecklich schief gelaufen!</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="84"/>
      <source>ERROR opening database with default settings</source>
      <translation>FEHLER beim Öffnen der Datenbank mit Standardeinstellungen</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="85"/>
      <source>I tried hard, but I just cannot open the database of default settings.</source>
      <translation>Ich habe alles versucht, aber die Datenbank mit den Standardeinstellungen kann einfach nicht geladen werden.</translation>
    </message>
  </context>
  <context>
    <name>PQShortcuts</name>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="61"/>
      <source>ERROR getting database with default shortcuts</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>FEHLER beim Abrufen der Datenbank mit Standard-Kurzbefehlen</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="62"/>
      <source>I tried hard, but I just cannot open even a read-only version of the shortcuts database.</source>
      <translation>Ich habe alles versucht, aber ich kann nicht einmal eine schreibgeschützte Version der Datenbank für die Kurzbefehle öffnen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="62"/>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="74"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Irgendetwas ist schrecklich schief gelaufen!</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="73"/>
      <source>ERROR opening database with default settings</source>
      <translation>FEHLER beim Öffnen der Datenbank mit Standardeinstellungen</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="74"/>
      <source>I tried hard, but I just cannot open the database of default shortcuts.</source>
      <translation>Ich habe alles versucht, aber die Datenbank mit den voreingestellten Kurzbefehlen kann einfach nicht geladen werden.</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="50"/>
      <source>Alt</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Alt</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="52"/>
      <source>Ctrl</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Strg</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="54"/>
      <source>Shift</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Umschalt</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="56"/>
      <source>Page Up</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Bild Hoch</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="58"/>
      <source>Page Down</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Bild Runter</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="60"/>
      <source>Meta</source>
      <extracomment>Refers to the key that usually has the Windows symbol on it</extracomment>
      <translation>Meta</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="62"/>
      <source>Keypad</source>
      <extracomment>Refers to the key that triggers the number block on keyboards</extracomment>
      <translation>Ziffernblock</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="64"/>
      <source>Escape</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Esc</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="66"/>
      <source>Right</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Rechts</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="68"/>
      <source>Left</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Links</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="70"/>
      <source>Up</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Hoch</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="72"/>
      <source>Down</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Runter</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="74"/>
      <source>Space</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Leertaste</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="76"/>
      <source>Delete</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Löschen</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="78"/>
      <source>Backspace</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Rücktaste</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="80"/>
      <source>Home</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Pos 1</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="82"/>
      <source>End</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Ende</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="84"/>
      <source>Insert</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Einfg</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="86"/>
      <source>Tab</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Tab</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="88"/>
      <source>Return</source>
      <extracomment>Return refers to the enter key of the number block - please try to make the translations of Return and Enter (the main button) different if possible!</extracomment>
      <translation>Enter</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="90"/>
      <source>Enter</source>
      <extracomment>Enter refers to the main enter key - please try to make the translations of Return (in the number block) and Enter different if possible!</extracomment>
      <translation>Eingabe</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="95"/>
      <source>Left Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Linke Taste</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="97"/>
      <source>Right Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Rechte Taste</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="99"/>
      <source>Middle Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Mittlere Taste</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="101"/>
      <source>Back Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Zurück</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="103"/>
      <source>Forward Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Vorwärts</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="105"/>
      <source>Task Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Task</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="107"/>
      <source>Button #7</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Taste #7</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="109"/>
      <source>Button #8</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Taste #8</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="111"/>
      <source>Button #9</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Taste #9</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="113"/>
      <source>Button #10</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Taste #10</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="115"/>
      <source>Double Click</source>
      <extracomment>Refers to a mouse event</extracomment>
      <translation>Doppelklick</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="117"/>
      <source>Wheel Up</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Mausrad hoch</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="119"/>
      <source>Wheel Down</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Mausrad runter</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="121"/>
      <source>Wheel Left</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Mausrad links</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="123"/>
      <source>Wheel Right</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Mausrad rechts</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="125"/>
      <source>East</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Osten</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="127"/>
      <source>South</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Süden</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="129"/>
      <source>West</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Westen</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="131"/>
      <source>North</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Norden</translation>
    </message>
  </context>
  <context>
    <name>about</name>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="39"/>
      <source>About</source>
      <translation>Über</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="73"/>
      <source>Show configuration overview</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Zeige Konfigurationsübersicht</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="80"/>
      <source>License:</source>
      <translation>Lizenz:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="85"/>
      <source>Open license in browser</source>
      <translation>Lizenz im Browser öffnen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="98"/>
      <source>Website:</source>
      <translation>Webseite:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="103"/>
      <source>Open website in browser</source>
      <translation>Webseite im Browser öffnen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="111"/>
      <source>Contact:</source>
      <translation>Kontakt:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="116"/>
      <source>Send an email</source>
      <translation>Sende eine E-Mail</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="158"/>
      <source>Configuration</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Konfiguration</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="173"/>
      <source>Copy to clipboard</source>
      <translation>In die Zwischenablage kopieren</translation>
    </message>
  </context>
  <context>
    <name>actions</name>
    <message>
      <location filename="../qml/actions/popout/PQExportPopout.qml" line="32"/>
      <source>Export image</source>
      <extracomment>Window title</extracomment>
      <translation>Bild exportieren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/popout/PQFileDialogPopout.qml" line="33"/>
      <source>File Dialog</source>
      <extracomment>Window title</extracomment>
      <translation>Dateidialog</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQMainMenuPopout.qml" line="33"/>
      <source>Main Menu</source>
      <extracomment>Window title</extracomment>
      <translation>Hauptmenü</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQMetaDataPopout.qml" line="33"/>
      <source>Metadata</source>
      <extracomment>Window title</extracomment>
      <translation>Metadaten</translation>
    </message>
    <message>
      <location filename="../qml/actions/popout/PQAboutPopout.qml" line="32"/>
      <source>About</source>
      <extracomment>Window title</extracomment>
      <translation>Über</translation>
    </message>
  </context>
  <context>
    <name>advancedsort</name>
    <message>
      <location filename="../qml/actions/popout/PQAdvancedSortPopout.qml" line="32"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="40"/>
      <source>Advanced image sort</source>
      <extracomment>Window title</extracomment>
      <translation>Erweiterte Bildsortierung</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="42"/>
      <source>Sort images</source>
      <translation>Bilder sortieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="78"/>
      <source>Sorting criteria:</source>
      <translation>Sortier-Kriterien:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="85"/>
      <source>Resolution</source>
      <extracomment>The image resolution (width/height in pixels)</extracomment>
      <translation>Auflösung</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="87"/>
      <source>Dominant color</source>
      <extracomment>The color that is most common in the image</extracomment>
      <translation>Dominante Farbe</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="89"/>
      <source>Average color</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Durchschnittsfarbe</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="91"/>
      <source>Luminosity</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Helligkeit</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="92"/>
      <source>Exif date</source>
      <translation>Exif Datum</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="149"/>
      <source>Sort by image resolution</source>
      <translation>Nach Bildauflösung sortieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="166"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="233"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="330"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="424"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="519"/>
      <source>in ascending order</source>
      <extracomment>as is: sort in ascending order</extracomment>
      <translation>in aufsteigender Reihenfolge</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="172"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="238"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="335"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="429"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="524"/>
      <source>in descending order</source>
      <extracomment>as is: sort in descending order</extracomment>
      <translation>in absteigender Reihenfolge</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="217"/>
      <source>Sort by dominant color</source>
      <translation>Nach dominanter Farbe sortieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="254"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="350"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="444"/>
      <source>low quality (fast)</source>
      <extracomment>quality and speed of advanced sorting of images</extracomment>
      <translation>geringe Qualität (schnell)</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="256"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="351"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="445"/>
      <source>medium quality</source>
      <extracomment>quality and speed of advanced sorting of images</extracomment>
      <translation>mittlere Qualität</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="258"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="352"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="446"/>
      <source>high quality (slow)</source>
      <extracomment>quality and speed of advanced sorting of images</extracomment>
      <translation>hohe Qualität (langsam)</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="314"/>
      <source>Sort by average color</source>
      <translation>Nach Durchschnittsfarbe sortieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="408"/>
      <source>Sort by luminosity</source>
      <translation>Nach Helligkeit sortieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="503"/>
      <source>Sort by date</source>
      <translation>Nach Datum sortieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="536"/>
      <source>Order of priority:</source>
      <translation>Prioritätsordnung:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="549"/>
      <source>Exif tag: Original date/time</source>
      <translation>Exif-Tag: Originale(s) Datum/Zeit</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="550"/>
      <source>Exif tag: Digitized date/time</source>
      <translation>Exif-Tag: Digitalisierte(s) Datum/Zeit</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="551"/>
      <source>File creation date</source>
      <translation>Erstellungsdatum der Datei</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="552"/>
      <source>File modification date</source>
      <translation>Änderungsdatum der Datei</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="625"/>
      <source>If a value cannot be found, PhotoQt will proceed to the next item in the list.</source>
      <translation>Falls ein Wert nicht gefunden werden konnte, wird PhotoQt zum nächsten Eintrag in der Liste übergehen.</translation>
    </message>
  </context>
  <context>
    <name>buttongeneric</name>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="55"/>
      <location filename="../qml/elements/PQButtonElement.qml" line="41"/>
      <source>Ok</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>OK</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="57"/>
      <location filename="../qml/elements/PQButtonElement.qml" line="43"/>
      <source>Cancel</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="59"/>
      <location filename="../qml/elements/PQButtonElement.qml" line="45"/>
      <source>Save</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Speichern</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="61"/>
      <location filename="../qml/elements/PQButtonElement.qml" line="47"/>
      <source>Close</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Schließen</translation>
    </message>
  </context>
  <context>
    <name>commandlineparser</name>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="36"/>
      <source>Image Viewer</source>
      <translation>Bildbetrachter</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="38"/>
      <source>Image file to open.</source>
      <translation>Zu öffnendes Bild.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="45"/>
      <source>Make PhotoQt ask for a new file.</source>
      <translation>PhotoQt zwingen nach einer neuen Datei zu fragen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="47"/>
      <source>Shows PhotoQt from system tray.</source>
      <translation>Zeigt PhotoQt aus der Systemleiste an.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="49"/>
      <source>Hides PhotoQt to system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Versteckt PhotoQt in die Systemleiste.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="51"/>
      <source>Quit PhotoQt.</source>
      <translation>PhotoQt beenden.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="53"/>
      <source>Show/Hide PhotoQt.</source>
      <translation>PhotoQt zeigen/verstecken.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="55"/>
      <source>Enable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Symbol im Infobereich der Systemleiste anzeigen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="57"/>
      <source>Disable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Symbol im Infobereich der Systemleiste deaktivieren.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="59"/>
      <source>Start PhotoQt hidden to the system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>PhotoQt versteckt in der Systemleiste starten.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="61"/>
      <source>Simulate a shortcut sequence</source>
      <extracomment>Command line option</extracomment>
      <translation>Einen Kurzbefehl simulieren</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="63"/>
      <source>Change setting to specified value.</source>
      <extracomment>Command line option</extracomment>
      <translation>Einstellung auf angegebenen Wert ändern.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="65"/>
      <source>settingname:value</source>
      <extracomment>Command line option</extracomment>
      <translation>Einstellung:Wert</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="67"/>
      <source>Switch on debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Debugmeldungen einschalten.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="69"/>
      <source>Switch off debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Debugmeldungen ausschalten.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="71"/>
      <source>Export configuration to given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Konfiguration in angegebene Datei exportieren.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="73"/>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="77"/>
      <source>filename</source>
      <extracomment>Command line option</extracomment>
      <translation>Dateiname</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="75"/>
      <source>Import configuration from given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Konfiguration von angegebener Datei importieren.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="79"/>
      <source>Check the configuration and correct any detected issues.</source>
      <extracomment>Command line option</extracomment>
      <translation>Prüft die Konfiguration und korrigiert alle gefundenen Probleme.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="81"/>
      <source>Reset default configuration.</source>
      <extracomment>Command line option</extracomment>
      <translation>Konfiguration auf Standardwerte zurücksetzen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="83"/>
      <source>Show configuration overview.</source>
      <extracomment>Command line option</extracomment>
      <translation>Zeige Konfigurationsübersicht.</translation>
    </message>
  </context>
  <context>
    <name>contextmenu</name>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="16"/>
      <source>Rename file</source>
      <translation>Datei umbenennen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="23"/>
      <source>Copy file</source>
      <translation>Datei kopieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="30"/>
      <source>Move file</source>
      <translation>Datei verschieben</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="37"/>
      <source>Delete file</source>
      <translation>Datei löschen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="46"/>
      <source>Copy to clipboard</source>
      <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="53"/>
      <source>Export to different format</source>
      <translation>In ein anderes Format exportieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="60"/>
      <source>Scale image</source>
      <translation>Bild skalieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="67"/>
      <source>Tag faces</source>
      <translation>Gesichter markieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="74"/>
      <source>Set as wallpaper</source>
      <translation>Als Hintergrundbild setzen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="83"/>
      <source>Show histogram</source>
      <translation>Histogramm anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="90"/>
      <source>Show on map</source>
      <translation>Auf Karte anzeigen</translation>
    </message>
  </context>
  <context>
    <name>export</name>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="22"/>
      <source>Export image</source>
      <extracomment>title of action element</extracomment>
      <translation>Bild exportieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="25"/>
      <location filename="../qml/actions/PQExport.qml" line="32"/>
      <source>Export</source>
      <extracomment>written on button</extracomment>
      <translation>Exportieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="67"/>
      <source>Something went wrong during export to the selected format...</source>
      <translation>Beim Export in das ausgewählte Format ist etwas schiefgelaufen...</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="98"/>
      <source>Favorites:</source>
      <extracomment>These are the favorite image formats for exporting images to</extracomment>
      <translation>Favoriten:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="111"/>
      <source>no favorites set</source>
      <extracomment>the favorites are image formats for exporting images to</extracomment>
      <translation>Keine Favoriten gesetzt</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="194"/>
      <location filename="../qml/actions/PQExport.qml" line="338"/>
      <source>Click to select this image format</source>
      <translation>Klicken, um dieses Bildformat auszuwählen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="216"/>
      <location filename="../qml/actions/PQExport.qml" line="360"/>
      <source>Click to remove this image format from your favorites</source>
      <translation>Klicken, um dieses Bildformat aus deinen Favoriten zu entfernen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="361"/>
      <source>Click to add this image format to your favorites</source>
      <translation>Klicken, um dieses Bildformat zu deinen Favoriten hinzuzufügen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="387"/>
      <source>Selected target format:</source>
      <extracomment>The target format is the format the image is about to be exported to</extracomment>
      <translation>Ausgewähltes Zielformat:</translation>
    </message>
  </context>
  <context>
    <name>facetagging</name>
    <message>
      <location filename="../qml/image/PQFaceTagger.qml" line="49"/>
      <source>Click to exit face tagging mode</source>
      <translation>Klicken, um den Modus zum Markieren von Gesichtern zu verlassen</translation>
    </message>
    <message>
      <location filename="../qml/image/PQFaceTagger.qml" line="205"/>
      <source>Who is this?</source>
      <translation>Wer ist das?</translation>
    </message>
  </context>
  <context>
    <name>filedialog</name>
    <message>
      <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="80"/>
      <source>Show files as icons</source>
      <translation>Dateien als Symbole anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="92"/>
      <source>Show files as list</source>
      <translation>Dateien als Liste anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="235"/>
      <source>no subfolders found</source>
      <translation>keine Unterordner gefunden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="471"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="473"/>
      <source>%1 image</source>
      <translation>%1 Bild</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="471"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="475"/>
      <source>%1 images</source>
      <translation>%1 Bilder</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="656"/>
      <source># images</source>
      <translation># Bilder</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="215"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="657"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="692"/>
      <source>Date:</source>
      <translation>Datum:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="216"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="658"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="693"/>
      <source>Time:</source>
      <translation>Zeit:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="217"/>
      <source>Location:</source>
      <translation>Standort:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="213"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="690"/>
      <source>File size:</source>
      <translation>Dateigröße:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="214"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="691"/>
      <source>File type:</source>
      <translation>Dateityp:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="104"/>
      <source>no supported files/folders found</source>
      <translation>keine unterstützten Dateien/Ordner gefunden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1112"/>
      <source>Load this folder</source>
      <translation>Diesen Ordner laden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1112"/>
      <source>Load this file</source>
      <translation>Diese Datei laden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1124"/>
      <source>Add to Favorites</source>
      <translation>Zu den Favoriten hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1133"/>
      <source>Remove file selection</source>
      <translation>Auswahl aufheben</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1133"/>
      <source>Select file</source>
      <translation>Datei auswählen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1144"/>
      <source>Remove all file selection</source>
      <translation>Auswahl aufheben</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1144"/>
      <source>Select all files</source>
      <translation>Alle Dateien auswählen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1153"/>
      <source>Delete selection</source>
      <translation>Markierte löschen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1154"/>
      <source>Delete file</source>
      <translation>Datei löschen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1155"/>
      <source>Delete folder</source>
      <translation>Ordner löschen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1156"/>
      <source>Delete file/folder</source>
      <translation>Datei/Ordner löschen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1163"/>
      <source>Cut selection</source>
      <translation>Auswahl ausschneiden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1164"/>
      <source>Cut file</source>
      <translation>Datei ausschneiden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1165"/>
      <source>Cut folder</source>
      <translation>Ordner ausschneiden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1166"/>
      <source>Cut file/folder</source>
      <translation>Datei/Ordner ausschneiden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1173"/>
      <source>Copy selection</source>
      <translation>Markierte kopieren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1174"/>
      <source>Copy file</source>
      <translation>Datei kopieren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1175"/>
      <source>Copy folder</source>
      <translation>Ordner kopieren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1176"/>
      <source>Copy file/folder</source>
      <translation>Datei/Ordner kopieren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1182"/>
      <source>Paste files from clipboard</source>
      <translation>Dateien aus Zwischenablage einfügen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1202"/>
      <source>Show hidden files</source>
      <translation>Versteckte Dateien anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1209"/>
      <source>Show tooltip with image details</source>
      <translation>Tooltip mit Bilddetails anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="67"/>
      <source>bookmarks and devices disabled</source>
      <translation>Lesezeichen und Geräte deaktiviert</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="403"/>
      <source>Show entry</source>
      <translation>Eintrag anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="403"/>
      <source>Hide entry</source>
      <translation>Eintrag verstecken</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="422"/>
      <source>Remove entry</source>
      <translation>Eintrag entfernen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="441"/>
      <source>Hide hidden entries</source>
      <translation>Versteckte Einträge verbergen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="441"/>
      <source>Show hidden entries</source>
      <translation>Versteckte Einträge anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="469"/>
      <source>Hide bookmarked places</source>
      <translation>Lesezeichen ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="469"/>
      <source>Show bookmarked places</source>
      <translation>Lesezeichen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="475"/>
      <source>Hide storage devices</source>
      <translation>Speichergeräte ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="475"/>
      <source>Show storage devices</source>
      <translation>Speichergeräte anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="498"/>
      <source>Storage Devices</source>
      <translation>Speichergeräte</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="521"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="203"/>
      <source>Bookmarks</source>
      <extracomment>file manager settings popdown: menu title</extracomment>
      <translation>Lesezeichen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="42"/>
      <source>Zoom:</source>
      <translation>Zoom:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="46"/>
      <source>Adjust size of files and folders</source>
      <translation>Größe von Dateien und Ordner anpassen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="116"/>
      <source>Cancel and close</source>
      <translation>Abbrechen und Schließen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="135"/>
      <source>Sort by:</source>
      <translation>Sortieren nach:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="142"/>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="143"/>
      <source>Natural Name</source>
      <translation>Natürlicher Name</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="144"/>
      <source>Time modified</source>
      <translation>Bearbeitungszeit</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="145"/>
      <source>File size</source>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="146"/>
      <source>File type</source>
      <translation>Dateityp</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="147"/>
      <source>reverse order</source>
      <translation>Reihenfolge umkehren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="201"/>
      <source>All supported images</source>
      <translation>Alle unterstützten Bilder</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="206"/>
      <source>Video files</source>
      <translation>Videodateien</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="207"/>
      <source>All files</source>
      <translation>Alle Dateien</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="18"/>
      <source>View</source>
      <extracomment>file manager settings popdown: menu title</extracomment>
      <translation>Ansicht</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="21"/>
      <source>layout</source>
      <extracomment>file manager settings popdown: submenu title</extracomment>
      <translation>Layout</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="23"/>
      <source>list view</source>
      <translation>Listenansicht</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="31"/>
      <source>icon view</source>
      <translation>Symbolansicht</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="42"/>
      <source>padding</source>
      <extracomment>file manager settings popdown: submenu title</extracomment>
      <translation>Abstand</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="60"/>
      <source>drag and drop</source>
      <extracomment>file manager settings popdown: submenu title</extracomment>
      <translation>Klicken und ziehen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="63"/>
      <source>enable for list view</source>
      <extracomment>file manager settings popdown: the thing to enable here is drag-and-drop</extracomment>
      <translation>für Listenansicht aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="71"/>
      <source>enable for grid view</source>
      <extracomment>file manager settings popdown: the thing to enable here is drag-and-drop</extracomment>
      <translation>für Symbolansicht aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="79"/>
      <source>enable for bookmarks</source>
      <extracomment>file manager settings popdown: the thing to enable here is drag-and-drop</extracomment>
      <translation>für Lesezeichen aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="89"/>
      <source>select with single click</source>
      <extracomment>file manager settings popdown: how to select files</extracomment>
      <translation>mit einem Klick auswählen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="96"/>
      <source>hidden files</source>
      <translation>Versteckte Dateien</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="103"/>
      <source>tooltips</source>
      <translation>Tooltips</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="112"/>
      <source>Thumbnails</source>
      <extracomment>file manager settings popdown: menu title</extracomment>
      <translation>Miniaturbilder</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="116"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="137"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="226"/>
      <source>show</source>
      <extracomment>file manager settings popdown: show thumbnails
----------
file manager settings popdown: show folder thumbnails
----------
file manager settings popdown: show image previews</extracomment>
      <translation>Anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="124"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="145"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="252"/>
      <source>scale and crop</source>
      <extracomment>file manager settings popdown: scale and crop the thumbnails
----------
file manager settings popdown: scale and crop the folder thumbnails
----------
file manager settings popdown: scale and crop image previews</extracomment>
      <translation>skalieren und zuschneiden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="132"/>
      <source>folder thumbnails</source>
      <translation>Miniaturbilder für Ordner</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="154"/>
      <source>autoload</source>
      <extracomment>file manager settings popdown: automatically load the folder thumbnails</extracomment>
      <translation>Automatisch laden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="163"/>
      <source>loop</source>
      <extracomment>file manager settings popdown: loop through the folder thumbnails</extracomment>
      <translation>Endlosschleife</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="172"/>
      <source>timeout</source>
      <extracomment>file manager settings popdown: timeout between switching folder thumbnails</extracomment>
      <translation>Timeout</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="183"/>
      <source>1 second</source>
      <translation>1 Sekunde</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="191"/>
      <source>half a second</source>
      <translation>halbe Sekunde</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="205"/>
      <source>show bookmarks</source>
      <translation>Lesezeichen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="213"/>
      <source>show devices</source>
      <extracomment>file manager settings popdown: the devices here are the storage devices</extracomment>
      <translation>Geräte anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="222"/>
      <source>Preview</source>
      <extracomment>file manager settings popdown: menu title</extracomment>
      <translation>Vorschau</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="234"/>
      <source>higher resolution</source>
      <extracomment>file manager settings popdown: use higher resolution for image previews</extracomment>
      <translation>höhere Auflösung</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="243"/>
      <source>blur</source>
      <extracomment>file manager settings popdown: blur image previews</extracomment>
      <translation>verwischen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="262"/>
      <source>color intensity</source>
      <extracomment>file manager settings popdown: color intensity of image previews</extracomment>
      <translation>Farbintensität</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="35"/>
      <source>%1 files already exist in the target directory.</source>
      <translation>%1 Dateien sind bereits im Zielverzeichnis vorhanden.</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="40"/>
      <source>Check the files below that you want to paste anyways. Files left unchecked will not be pasted.</source>
      <translation>Markiere die untenstehenden Dateien, die trotzdem eingefügt werden sollen. Nicht markierte Dateien werden nicht eingefügt.</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="162"/>
      <source>Select all</source>
      <translation>Alles auswählen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="169"/>
      <source>Select none</source>
      <translation>Nichts auswählen</translation>
    </message>
  </context>
  <context>
    <name>filemanagement</name>
    <message>
      <location filename="../qml/actions/popout/PQDeletePopout.qml" line="32"/>
      <location filename="../qml/actions/PQDelete.qml" line="42"/>
      <source>Delete file?</source>
      <extracomment>Window title</extracomment>
      <translation>Datei löschen?</translation>
    </message>
    <message>
      <location filename="../qml/actions/popout/PQRenamePopout.qml" line="32"/>
      <location filename="../qml/actions/PQRename.qml" line="41"/>
      <location filename="../qml/actions/PQRename.qml" line="44"/>
      <source>Rename file</source>
      <extracomment>Window title</extracomment>
      <translation>Datei umbenennen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQDelete.qml" line="44"/>
      <location filename="../qml/actions/PQDelete.qml" line="112"/>
      <source>Move to trash</source>
      <translation>In den Papierkorb verschieben</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQDelete.qml" line="48"/>
      <location filename="../qml/actions/PQDelete.qml" line="114"/>
      <source>Delete permanently</source>
      <translation>Dauerhaft löschen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQDelete.qml" line="78"/>
      <source>Are you sure you want to delete this file?</source>
      <translation>Soll diese Datei wirklich gelöscht werden?</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQDelete.qml" line="99"/>
      <source>An error occured, file could not be deleted!</source>
      <translation>Ein Fehler ist aufgetreten, die Datei konnte nicht gelöscht werden!</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQRename.qml" line="62"/>
      <source>old filename:</source>
      <translation>alter Dateiname:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQRename.qml" line="87"/>
      <source>new filename:</source>
      <translation>neuer Dateiname:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQRename.qml" line="140"/>
      <source>An error occured, file could not be renamed</source>
      <translation>Ein Fehler ist aufgetreten, die Datei konnte nicht umbenannt werden</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQRename.qml" line="152"/>
      <source>A file with this filename already exists</source>
      <translation>Eine Datei mit diesem Namen existiert bereits</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQCopy.qml" line="55"/>
      <source>Copy here</source>
      <translation>Hierher kopieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQCopy.qml" line="91"/>
      <location filename="../qml/actions/PQMove.qml" line="94"/>
      <source>An error occured</source>
      <translation>Ein Fehler ist aufgetreten</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQCopy.qml" line="96"/>
      <source>File could not be copied</source>
      <translation>Datei konnte nicht kopiert werden</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQMove.qml" line="56"/>
      <source>Move here</source>
      <translation>Hierher verschieben</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQMove.qml" line="99"/>
      <source>File could not be moved</source>
      <translation>Datei konnte nicht verschoben werden</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="555"/>
      <source>File successfully deleted</source>
      <translation>Datei erfolgreich gelöscht</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="558"/>
      <source>Could not delete file</source>
      <translation>Datei konnte nicht gelöscht werden</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="565"/>
      <source>File successfully moved to trash</source>
      <translation>Datei erfolgreich in den Papierkorb verschoben</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="568"/>
      <source>Could not move file to trash</source>
      <translation>Datei konnte nicht in den Papierkorb verschoben werden</translation>
    </message>
  </context>
  <context>
    <name>filter</name>
    <message>
      <location filename="../qml/actions/popout/PQFilterPopout.qml" line="32"/>
      <location filename="../qml/actions/PQFilter.qml" line="38"/>
      <source>Filter images in current directory</source>
      <extracomment>Window title</extracomment>
      <translation>Bilder im aktuellen Verzeichnis filtern</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="41"/>
      <source>Filter</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Filter</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="48"/>
      <source>Remove filter</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Filter entfernen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="79"/>
      <source>To filter by file extension, start the term with a dot. Setting the width or height of the resolution to 0 ignores that dimension.</source>
      <translation>Um nach Dateierweiterung zu filtern, starte den Begriff mit einem Punkt. Wenn die Breite oder Höhe der Auflösung auf 0 gesetzt ist, dann wird diese Dimension ignoriert.</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="108"/>
      <source>File name/extension:</source>
      <translation>Dateiname/-erweiterung:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="128"/>
      <source>Enter terms</source>
      <translation>Begriffe eingeben</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="142"/>
      <source>Image Resolution</source>
      <translation>Bildauflösung</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="160"/>
      <location filename="../qml/actions/PQFilter.qml" line="225"/>
      <source>greater than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution GREATER THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size GREATER THAN 123 KB/MB&apos;</extracomment>
      <translation>größer als</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="162"/>
      <location filename="../qml/actions/PQFilter.qml" line="227"/>
      <source>less than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution LESS THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size LESS THAN 123 KB/MB&apos;</extracomment>
      <translation>kleiner als</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="207"/>
      <source>File size</source>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="276"/>
      <source>Please note that filtering by image resolution can take a little while, depending on the number of images in the folder.</source>
      <translation>Bitte beachte, dass das Filtern nach Bildauflösung eine Weile dauern kann, abhängig von der Anzahl der Bilder im Ordner.</translation>
    </message>
  </context>
  <context>
    <name>histogram</name>
    <message>
      <location filename="../qml/ongoing/popout/PQHistogramPopout.qml" line="33"/>
      <location filename="../qml/ongoing/PQHistogram.qml" line="193"/>
      <source>Histogram</source>
      <extracomment>Window title</extracomment>
      <translation>Histogramm</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQHistogram.qml" line="57"/>
      <source>Click-and-drag to move.</source>
      <translation>Zum Verschieben klicken und ziehen.</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQHistogram.qml" line="57"/>
      <source>Right click to switch version.</source>
      <translation>Rechtsklick, um Version zu wechseln.</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQHistogram.qml" line="165"/>
      <source>Loading...</source>
      <translation>Lade...</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQHistogram.qml" line="179"/>
      <source>Error loading histogram</source>
      <translation>Fehler beim Laden des Histogramms</translation>
    </message>
  </context>
  <context>
    <name>image</name>
    <message>
      <location filename="../qml/image/PQImage.qml" line="757"/>
      <source>Click here to enter viewer mode</source>
      <translation>Hier klicken, um den Betrachtermodus zu aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/image/PQImage.qml" line="776"/>
      <source>Hide central button for entering viewer mode</source>
      <translation>Mittigen Knopf für den Betrachtermodus ausblenden</translation>
    </message>
  </context>
  <context>
    <name>imageprovider</name>
    <message>
      <location filename="../cplusplus/images/provider/pqc_providerfull.cpp" line="47"/>
      <location filename="../cplusplus/images/provider/pqc_providerthumb.cpp" line="195"/>
      <source>File failed to load, it does not exist!</source>
      <translation>Laden der Datei fehlgeschlagen, sie existiert nicht!</translation>
    </message>
  </context>
  <context>
    <name>imgur</name>
    <message>
      <location filename="../qml/actions/popout/PQImgurPopout.qml" line="32"/>
      <location filename="../qml/actions/PQImgur.qml" line="40"/>
      <location filename="../qml/actions/PQImgur.qml" line="41"/>
      <source>Upload to imgur.com</source>
      <extracomment>Window title</extracomment>
      <translation>Hochladen auf imgur.com</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="48"/>
      <source>Show past uploads</source>
      <translation>Vorherige Uploads anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="99"/>
      <source>This seems to take a long time...</source>
      <translation>Dies scheint eine lange Zeit zu dauern...</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="100"/>
      <source>There might be a problem with your internet connection or the imgur.com servers.</source>
      <translation>Es könnte ein Problem mit deiner Internetverbindung oder den Servern von imgur.com geben.</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="116"/>
      <source>An Error occurred while uploading image!</source>
      <translation>Beim Hochladen des Bildes ist ein Fehler aufgetreten!</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="117"/>
      <source>Error code:</source>
      <translation>Fehlercode:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="137"/>
      <source>You do not seem to be connected to the internet...</source>
      <translation>Du scheinst nicht mit dem Internet verbunden zu sein...</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="138"/>
      <source>Unable to upload!</source>
      <translation>Hochladen nicht möglich!</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="191"/>
      <source>Access Image</source>
      <translation>Bild aufrufen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="206"/>
      <location filename="../qml/actions/PQImgur.qml" line="243"/>
      <location filename="../qml/actions/PQImgur.qml" line="408"/>
      <location filename="../qml/actions/PQImgur.qml" line="440"/>
      <source>Click to open in browser</source>
      <translation>Zum Öffnen im Browser klicken</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="228"/>
      <source>Delete Image</source>
      <translation>Bild löschen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="305"/>
      <source>No past uploads found</source>
      <extracomment>The uploads are uploads to imgur.com</extracomment>
      <translation>Keine vorherigen Uploads gefunden</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="399"/>
      <source>Access:</source>
      <extracomment>Used as in: access this image</extracomment>
      <translation>Zugriff:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="431"/>
      <source>Delete:</source>
      <extracomment>Used as in: delete this image</extracomment>
      <translation>Löschen:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="479"/>
      <source>Clear all</source>
      <extracomment>Written on button, please keep short. Used as in: clear all entries</extracomment>
      <translation>Alles löschen</translation>
    </message>
  </context>
  <context>
    <name>logging</name>
    <message>
      <location filename="../qml/ongoing/popout/PQLoggingPopout.qml" line="108"/>
      <source>enable</source>
      <extracomment>Used as in: enable debug message</extracomment>
      <translation>aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQLoggingPopout.qml" line="140"/>
      <source>copy to clipboard</source>
      <extracomment>the thing being copied here are the debug messages</extracomment>
      <translation>in die Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQLoggingPopout.qml" line="146"/>
      <source>save to file</source>
      <extracomment>the thing saved to files here are the debug messages</extracomment>
      <translation>In Datei speichern</translation>
    </message>
  </context>
  <context>
    <name>mapcurrent</name>
    <message>
      <location filename="../qml/ongoing/popout/PQMapCurrentPopout.qml" line="33"/>
      <location filename="../qml/ongoing/PQMapCurrent.qml" line="186"/>
      <source>Current location</source>
      <extracomment>Window title</extracomment>
      <translation>Aktueller Standort</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMapCurrent.qml" line="62"/>
      <source>Click-and-drag to move.</source>
      <translation>Zum Verschieben klicken und ziehen.</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMapCurrent.qml" line="166"/>
      <source>No location data</source>
      <translation>Keine Standortdaten</translation>
    </message>
  </context>
  <context>
    <name>mapexplorer</name>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="257"/>
      <source>no images in currently visible area</source>
      <extracomment>the currently visible area refers to the latitude/longitude selection in the map explorer</extracomment>
      <translation>keine Bilder im aktuell sichtbaren Bereich</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="269"/>
      <source>no images with location data in current folder</source>
      <translation>keine Bilder mit Standortdaten im aktuellen Ordner</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImagesTweaks.qml" line="46"/>
      <source>Image zoom:</source>
      <translation>Bildzoom:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImagesTweaks.qml" line="69"/>
      <source>scale and crop thumbnails</source>
      <translation>Miniaturbilder skalieren und zuschneiden</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerMapTweaks.qml" line="47"/>
      <source>Map zoom:</source>
      <translation>Kartenzoom:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerMapTweaks.qml" line="73"/>
      <source>Reset view</source>
      <extracomment>The view here is the map layout in the map explorer</extracomment>
      <translation>Ansicht zurücksetzen</translation>
    </message>
  </context>
  <context>
    <name>metadata</name>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="172"/>
      <source>No file loaded</source>
      <translation>Keine Datei geladen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="194"/>
      <source>Metadata</source>
      <extracomment>The title of the floating element</extracomment>
      <translation>Metadaten</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="236"/>
      <source>File name</source>
      <translation>Dateiname</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="242"/>
      <source>Dimensions</source>
      <translation>Bildgröße</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="248"/>
      <source>Image</source>
      <translation>Bild</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="254"/>
      <source>File size</source>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="260"/>
      <source>File type</source>
      <translation>Dateityp</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="271"/>
      <source>Make</source>
      <translation>Hersteller</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="277"/>
      <source>Model</source>
      <translation>Modell</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="283"/>
      <source>Software</source>
      <translation>Software</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="294"/>
      <source>Time Photo was Taken</source>
      <translation>Aufnahmezeit</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="300"/>
      <source>Exposure Time</source>
      <translation>Belichtungszeit</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="306"/>
      <source>Flash</source>
      <translation>Blitz</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="318"/>
      <source>Scene Type</source>
      <translation>Szenenart</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="324"/>
      <source>Focal Length</source>
      <translation>Brennweite</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="330"/>
      <source>F Number</source>
      <translation>Blende</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="336"/>
      <source>Light Source</source>
      <translation>Lichtquelle</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="347"/>
      <source>Keywords</source>
      <translation>Schlüsselwörter</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="353"/>
      <source>Location</source>
      <translation>Ort</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="359"/>
      <source>Copyright</source>
      <translation>Urheberrecht</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="370"/>
      <source>GPS Position</source>
      <translation>GPS-Position</translation>
    </message>
  </context>
  <context>
    <name>navigate</name>
    <message>
      <location filename="../qml/ongoing/PQNavigation.qml" line="79"/>
      <source>Click and drag to move</source>
      <translation>Zum Verschieben klicken und ziehen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQNavigation.qml" line="117"/>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="101"/>
      <source>Navigate to previous image in folder</source>
      <translation>Vorhergehendes Bild im Ordner anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQNavigation.qml" line="151"/>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="123"/>
      <source>Navigate to next image in folder</source>
      <translation>Nächstes Bild im Ordner anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQNavigation.qml" line="182"/>
      <source>Show main menu</source>
      <translation>Hauptmenü anzeigen</translation>
    </message>
  </context>
  <context>
    <name>other</name>
    <message>
      <location filename="../qml/other/PQBackgroundMessage.qml" line="87"/>
      <source>Click anywhere to open a file</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation>Zum Öffnen einer Datei irgendwo klicken</translation>
    </message>
    <message>
      <location filename="../qml/other/PQBackgroundMessage.qml" line="99"/>
      <source>Move your cursor to the indicated window edges for various actions</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation>Bewege den Mauszeiger an die gezeigten Fensterkanten für verschiedene Aktionen</translation>
    </message>
  </context>
  <context>
    <name>popinpopout</name>
    <message>
      <location filename="../qml/actions/PQMapExplorer.qml" line="132"/>
      <location filename="../qml/elements/PQTemplateFloating.qml" line="133"/>
      <location filename="../qml/elements/PQTemplateFullscreen.qml" line="227"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="205"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="816"/>
      <location filename="../qml/ongoing/PQMetaData.qml" line="481"/>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="340"/>
      <source>Merge into main interface</source>
      <extracomment>Tooltip of small button to merge a popped out element (i.e., one in its own window) into the main interface</extracomment>
      <translation>In das Hauptfenster integrieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQMapExplorer.qml" line="134"/>
      <location filename="../qml/elements/PQTemplateFloating.qml" line="135"/>
      <location filename="../qml/elements/PQTemplateFullscreen.qml" line="229"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="207"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="818"/>
      <location filename="../qml/ongoing/PQMetaData.qml" line="483"/>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="342"/>
      <source>Move to its own window</source>
      <extracomment>Tooltip of small button to show an element in its own window (i.e., not merged into main interface)</extracomment>
      <translation>In eigenem Fenster anzeigen</translation>
    </message>
  </context>
  <context>
    <name>quickinfo</name>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="152"/>
      <source>Click here to show main menu</source>
      <translation>Hier klicken, um das Hauptmenü anzuzeigen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="176"/>
      <source>Click here to not keep window in foreground</source>
      <translation>Hier klicken, um das Fenster nicht im Vordergrund zu halten</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="176"/>
      <source>Click here to keep window in foreground</source>
      <translation>Hier klicken, um das Fenster im Vordergrund zu halten</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="206"/>
      <source>Click here to minimize window</source>
      <translation>Hier klicken, um Fenster zu minimieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="239"/>
      <source>Click here to restore window</source>
      <translation>Hier klicken, um Fenster wiederherzustellen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="240"/>
      <source>Click here to maximize window</source>
      <translation>Hier klicken, um Fenster zu maximieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="272"/>
      <source>Click here to enter fullscreen mode</source>
      <translation>Hier klicken, um den Vollbildmodus zu aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="273"/>
      <source>Click here to exit fullscreen mode</source>
      <translation>Hier klicken, um den Vollbildmodus zu beenden</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="305"/>
      <source>Click here to close PhotoQt</source>
      <translation>Hier klicken, um die PhotoQt zu schließen</translation>
    </message>
  </context>
  <context>
    <name>scale</name>
    <message>
      <location filename="../qml/actions/popout/PQScalePopout.qml" line="32"/>
      <location filename="../qml/actions/PQScale.qml" line="42"/>
      <source>Scale image</source>
      <extracomment>Window title</extracomment>
      <translation>Bild skalieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="44"/>
      <location filename="../qml/actions/PQScale.qml" line="394"/>
      <source>Scale</source>
      <translation>Skalieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="71"/>
      <source>An error occured, file could not be scaled</source>
      <translation>Ein Fehler ist aufgetreten, die Datei konnte nicht skaliert werden</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="81"/>
      <source>Scaling of this file format is not yet supported</source>
      <translation>Skalierung dieses Dateiformats wird noch nicht unterstützt</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="100"/>
      <source>Width:</source>
      <extracomment>The number of horizontal pixels of the image</extracomment>
      <translation>Breite:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="106"/>
      <source>Height:</source>
      <extracomment>The number of vertical pixels of the image</extracomment>
      <translation>Höhe:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="206"/>
      <source>New size:</source>
      <translation>Neue Größe:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="209"/>
      <source>pixels</source>
      <extracomment>This is used as in: 100x100 pixels</extracomment>
      <translation>Pixel</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="292"/>
      <source>Quality:</source>
      <translation>Qualität:</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager</name>
    <message>
      <location filename="../qml/settingsmanager/popout/PQSettingsManagerPopout.qml" line="33"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="16"/>
      <source>Settings Manager</source>
      <extracomment>Window title</extracomment>
      <translation>Einstellungen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="21"/>
      <source>Apply changes</source>
      <translation>Änderungen übernehmen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="25"/>
      <source>Revert changes</source>
      <translation>Änderungen verwerfen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="55"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="74"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="271"/>
      <source>Interface</source>
      <extracomment>A settings category
----------
This is a shortcut category</extracomment>
      <translation>Benutzeroberfläche</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="58"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="60"/>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="91"/>
      <source>Language</source>
      <extracomment>A settings subcategory and the qml filename
----------
A settings title</extracomment>
      <translation>Sprache</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="65"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="66"/>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="56"/>
      <source>Background</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Hintergrund</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="85"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="86"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="110"/>
      <source>Popout</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Popout</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="123"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="124"/>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="59"/>
      <source>Context menu</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Kontextmenü</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="144"/>
      <source>Window</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Fenster</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="167"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="215"/>
      <source>Image</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="182"/>
      <source>Interaction</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Interaktion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="193"/>
      <source>Folder</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Ordner</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="204"/>
      <source>Share online</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Online teilen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>All thumbnails</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Alle Miniaturbilder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="243"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="380"/>
      <source>Manage</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Verwalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="294"/>
      <source>Face tags</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Gesichter</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="312"/>
      <source>Instance</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Instanz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="324"/>
      <source>Tray icon</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Symbol im Infobereich der Systemleiste</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="164"/>
      <source>Image view</source>
      <extracomment>A settings category</extracomment>
      <translation>Bildansicht</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="296"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="70"/>
      <source>Look</source>
      <extracomment>Settings title</extracomment>
      <translation>Aussehen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="285"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="343"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="368"/>
      <source>Behavior</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Verhalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="113"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="114"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="68"/>
      <source>Edges</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Kanten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="212"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="45"/>
      <source>Thumbnails</source>
      <extracomment>A settings category
----------
Used as descriptor for a screen edge action</extracomment>
      <translation>Miniaturbilder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="258"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="49"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="58"/>
      <source>Metadata</source>
      <extracomment>A settings category
----------
Used as descriptor for a screen edge action
----------
Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Metadaten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="261"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="262"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="104"/>
      <source>Labels</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Labels</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="309"/>
      <source>Session</source>
      <extracomment>A settings category</extracomment>
      <translation>Sitzung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="317"/>
      <source>Remember</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Merken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="335"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="338"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="339"/>
      <source>File types</source>
      <extracomment>A settings category
----------
A settings subcategory</extracomment>
      <translation>Dateitypen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="360"/>
      <source>Keyboard &amp; Mouse</source>
      <extracomment>A settings category</extracomment>
      <translation>Tastatur &amp; Maus</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="363"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="364"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="204"/>
      <source>Shortcuts</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Kurzbefehle</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="382"/>
      <source>Reset</source>
      <translation>Zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="383"/>
      <source>Reset settings</source>
      <translation>Einstellungen zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="384"/>
      <source>Reset shortcuts</source>
      <translation>Kurzbefehle zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="387"/>
      <source>Export/Import</source>
      <translation>Export/Import</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="388"/>
      <source>Export settings</source>
      <translation>Einstellungen exportieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="389"/>
      <source>Import settings</source>
      <translation>Einstellungen importieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="460"/>
      <source>Ctrl+S = Apply changes, Ctrl+R = Revert changes, Esc = Close</source>
      <translation>Strg+S = Änderungen anwenden, Strg+R = Änderungen zurücksetzen, Esc = Schließen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="493"/>
      <source>Unsaved changes</source>
      <translation>Ungespeicherte Änderungen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="501"/>
      <source>The settings on this page have changed. Do you want to apply or discard them?</source>
      <translation>Die Einstellungen auf dieser Seite haben sich geändert. Sollen sie angewendet oder verworfen werden?</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="513"/>
      <source>Apply</source>
      <extracomment>written on button, used as in: apply changes</extracomment>
      <translation>Anwenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="532"/>
      <source>Discard</source>
      <extracomment>written on button, used as in: discard changes</extracomment>
      <translation>Verwerfen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="344"/>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="45"/>
      <source>PDF</source>
      <extracomment>Settings title</extracomment>
      <translation>PDF</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="51"/>
      <source>PhotoQt can show PDF and Postscript documents alongside your images, you can even enter a multi-page document and browse its pages as if they were images in a folder. The quality setting here - specified in dots per pixel (dpi) - affects the resolution and speed of loading such pages.</source>
      <translation>PhotoQt kann PDF- und Postscript-Dokumente zusammen mit den Bildern anzeigen. Mehrseitige Dokument können sogar betreten werden und deren Seiten betrachtet werden als wären es Bilder in einem Ordner. Die hier angegebene Qualitätseinstellung - in DPI - beeinflusst die Auflösung und die Geschwindigkeit beim Laden solcher Seiten.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="80"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="78"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="216"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="254"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="72"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="113"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="153"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="257"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="301"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="384"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="170"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="253"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="122"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="208"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="75"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="169"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="78"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="219"/>
      <source>current value:</source>
      <extracomment>The current value of the slider specifying the PDF quality
----------
The current value of the slider specifying the font size for the status information
----------
The current value of the slider specifying the size of the window buttons</extracomment>
      <translation>aktueller Wert:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="345"/>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="90"/>
      <source>Archive</source>
      <extracomment>Settings title</extracomment>
      <translation>Archiv</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="96"/>
      <source>PhotoQt allows the browsing of all images contained in an archive file (ZIP, RAR, etc.) as if they all are located in a folder. By default, PhotoQt uses Libarchive for this purpose, but for RAR archives in particular PhotoQt can call the external tool unrar to load and display the archive and its contents. Note that this requires unrar to be installed and located in your path.</source>
      <translation>PhotoQt erlaubt das Durchsuchen aller Bilder, die in einer Archivdatei (ZIP, RAR, etc.) enthalten sind, als ob sie alle in einem Ordner lägen. Standardmäßig verwendet PhotoQt Libarchive für diesen Zweck, aber insbesondere für RAR-Archive kann PhotoQt das externe Werkzeug unrar aufrufen, um das Archiv und dessen Inhalt zu laden und anzuzeigen. Bitte beachte, dass unrar installiert ist und sich im Pfad befindet.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="103"/>
      <source>use external tool: unrar</source>
      <translation>externes Werkzeug verwenden: unrar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="346"/>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="115"/>
      <source>Video</source>
      <extracomment>Settings title</extracomment>
      <translation>Video</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="121"/>
      <source>PhotoQt can treat video files the same as image files, as long as the respective video formats are enabled. There are a few settings available for managing how videos behave in PhotoQt: Whether they should autoplay when loaded, whether they should loop from the beginning when the end is reached, whether to prefer libmpv (if available) or Qt for video playback, and which video thumbnail generator to use.</source>
      <translation>PhotoQt kann Videos zusammen mit Bildern anzeigen, solange die entsprechenden Videoformate aktiviert sind. Es gibt ein paar Einstellungen zur Verwaltung des Verhaltens von Videos in PhotoQt: Ob sie beim Laden automatisch abgespielt werden sollen, ob sie in einer Schleife durchspielt werden sollen, ob libmpv (falls verfügbar) oder Qt für die Video-Wiedergabe bevorzugt werden soll, und welcher Generator für Miniaturbilder von Videos verwendet werden soll.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="129"/>
      <source>Autoplay</source>
      <translation>Autoplay</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="135"/>
      <source>Loop</source>
      <translation>Schleife</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="145"/>
      <source>prefer Qt Multimedia</source>
      <translation>Qt Multimedia bevorzugen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="150"/>
      <source>prefer Libmpv</source>
      <translation>libmpv bevorzugen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="161"/>
      <source>Video thumbnail generator:</source>
      <translation>Generator für Miniaturbilder von Videos:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="347"/>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="179"/>
      <source>Viewer mode</source>
      <extracomment>Settings title</extracomment>
      <translation>Betrachtermodus</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="185"/>
      <source>When a document or achive is loaded in PhotoQt, it is possible to enter such a file. This means that PhotoQt will act as if the content of the file is located in some folder and loads the content as thumbnails allowing for the usual interaction and navigation to browse around. This viewer mode can be entered either by a small button that will show up below the status info, or it is possible to also show a big central button to activate this mode.</source>
      <translation>Wenn ein Dokument oder Achiv in PhotoQt geladen wird, ist es möglich, eine solche Datei zu betreten. Das bedeutet, dass PhotoQt so handelt, als ob sich der Inhalt der Datei in einem Ordner befindet und lädt den Inhalt als Miniaturbilder. Die übliche Interaktion und Navigation zum Durchsuchen sind dann möglich. Dieser Betrachtermodus kann entweder über eine kleine Schaltfläche aktiviert werden, die unter der Statusinformation angezeigt wird. Es ist auch möglich einen großen Zentralknopf anzuzeigen, um diesen Modus zu aktivieren.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="192"/>
      <source>show big button to enter viewer mode</source>
      <translation>große Taste zum Betreten des Betrachtermodus anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="47"/>
      <source>images</source>
      <extracomment>This is a category of files PhotoQt can recognize: any image format</extracomment>
      <translation>Bilder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="49"/>
      <source>compressed files</source>
      <extracomment>This is a category of files PhotoQt can recognize: compressed files like zip, tar, cbr, 7z, etc.</extracomment>
      <translation>Komprimierte Dateien</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="51"/>
      <source>documents</source>
      <extracomment>This is a category of files PhotoQt can recognize: documents like pdf, txt, etc.</extracomment>
      <translation>Dokumente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="53"/>
      <source>videos</source>
      <extracomment>This is a type of category of files PhotoQt can recognize: videos like mp4, avi, etc.</extracomment>
      <translation>Videos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="59"/>
      <source>Enable</source>
      <extracomment>As in: &quot;Enable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="65"/>
      <source>Disable</source>
      <extracomment>As in: &quot;Disable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Deaktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="90"/>
      <source>Enable everything</source>
      <extracomment>As in &quot;Enable every single file format PhotoQt can open in any category&quot;</extracomment>
      <translation>Alles aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="102"/>
      <source>Currently there are %1 file formats enabled</source>
      <extracomment>The %1 will be replaced with the number of file formats, please don&apos;t forget to add it.</extracomment>
      <translation>Momentan sind %1 Dateiformate aktiviert</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="131"/>
      <source>Search by description or file ending</source>
      <translation>Suche nach Beschreibung oder Dateiendung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="140"/>
      <source>Search by image library or category</source>
      <translation>Suche nach Bildbibliothek oder Kategorie</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="239"/>
      <source>File endings:</source>
      <translation>Dateiendungen:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="194"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="41"/>
      <source>Looping</source>
      <extracomment>Settings title</extracomment>
      <translation>Schleife</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="55"/>
      <source>Loop around</source>
      <extracomment>When reaching the end of the images in the folder whether to loop back around to the beginning or not</extracomment>
      <translation>Schleife</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="195"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="67"/>
      <source>Sort images</source>
      <extracomment>Settings title</extracomment>
      <translation>Bilder sortieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="73"/>
      <source>Images in a folder can be sorted in different ways depending on your preferences. These criteria here are the ones that can be used in a very quick way. Once a folder is loaded it is possible to further sort a folder in several advanced ways using the menu option for sorting.</source>
      <translation>Bilder in einem Ordner können je nach Vorliebe unterschiedlich sortiert werden. Diese Kriterien hier können sehr effizient angewendet werden. Nach dem Laden eines Ordners ist es möglich, jenen über die Menüoption zur erweiterten Sortierung nach weiteren Kriterien zu sortieren.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="82"/>
      <source>Sort by:</source>
      <translation>Sortieren nach:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="87"/>
      <source>natural name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>natürlicher Name</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="89"/>
      <source>name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Name</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="91"/>
      <source>time</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Zeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="93"/>
      <source>size</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Größe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="95"/>
      <source>type</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Typ</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="106"/>
      <source>ascending order</source>
      <extracomment>Sort images in ascending order</extracomment>
      <translation>aufsteigend</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="113"/>
      <source>descending order</source>
      <extracomment>Sort images in descending order</extracomment>
      <translation>absteigend</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="183"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="43"/>
      <source>Zoom</source>
      <extracomment>Settings title</extracomment>
      <translation>Zoom</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="49"/>
      <source>PhotoQt allows for a great deal of flexibility in viewing images at the perfect size. Additionally it allows for control of how fast the zoom happens, and if there is a minimum/maximum zoom level at which it should always stop no matter what. Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</source>
      <translation>PhotoQt erlaubt für viel Flexibilität beim Betrachten von Bildern in der perfekten Größe. Zusätzlich ermöglicht es die Kontrolle der Geschwindigkeit des Zooms, und ob es eine minimale/maximale Zoomstufe geben soll. Beachte, dass die maximale Zoomstufe die absolute Zoomstufe ist, die minimale Zoomstufe jedoch relativ zur Standard-Zoomstufe (die Zoomstufe beim ersten Laden des Bildes) berechnet wird.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="56"/>
      <source>zoom speed:</source>
      <translation>Zoomgeschwindigkeit:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="85"/>
      <source>minimum zoom:</source>
      <translation>minimaler Zoom:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="125"/>
      <source>maximum zoom:</source>
      <translation>maximaler Zoom:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="196"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="125"/>
      <source>Animation</source>
      <extracomment>Settings title</extracomment>
      <translation>Animation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="47"/>
      <source>When loading an image PhotoQt loads all images in the folder as thumbnails for easy navigation. When PhotoQt reaches the end of the list of files, it can either stop right there or loop back to the other end of the list and keep going.</source>
      <translation>Beim Laden eines Bildes lädt PhotoQt alle Bilder im Ordner als Vorschaubilder zur einfacheren Navigation. Wenn PhotoQt das Ende der Liste erreicht, kann entweder dort angehalten werden oder am anderen Ende der Liste weiter gemacht werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="131"/>
      <source>When switching between images PhotoQt can add an animation to smoothes such a transition. There are a whole bunch of transitions to choose from, and also an option for PhotoQt to choose one at random each time. Additionally, the speed of the chosen animation can be chosen from very slow to very fast.</source>
      <translation>Beim Wechseln zwischen den Bildern kann PhotoQt eine Animation hinzufügen, um einen solchen Übergang weicher zu gestalten. Es kann von einer ganzen Reihe von Übergängen ausgewählt werden, undes ist auch möglich PhotoQt zufällig eine wählen zu lassen. Zusätzlich kann die Geschwindigkeit der gewählten Animation von sehr langsam bis sehr schnell gewählt werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="138"/>
      <source>animate switching between images</source>
      <translation>Wechsel zwischen Bildern animieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="149"/>
      <source>Animation:</source>
      <translation>Animation:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="154"/>
      <source>opacity</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>Deckkraft</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="156"/>
      <source>along x-axis</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>entlang der x-Achse</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="158"/>
      <source>along y-axis</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>entlang der y-Achse</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="160"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="103"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="238"/>
      <source>rotation</source>
      <extracomment>This is referring to an in/out animation of images
----------
Please keep short! This is the rotation of the current image</extracomment>
      <translation>Drehung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="162"/>
      <source>explosion</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>Explosion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="164"/>
      <source>implosion</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>Implosion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="166"/>
      <source>choose one at random</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>zufällig auswählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="177"/>
      <source>very slow</source>
      <extracomment>used here for the animation speed</extracomment>
      <translation>sehr langsam</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="188"/>
      <source>very fast</source>
      <extracomment>used here for the animation speed</extracomment>
      <translation>sehr schnell</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="184"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="164"/>
      <source>Floating navigation</source>
      <extracomment>Settings title</extracomment>
      <translation>Schwebende Navigation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="170"/>
      <source>Switching between images can be done in various ways. It is possible to do so through the shortcuts, through the main menu, or through floating navigation buttons. These floating buttons were added especially with touch screens in mind, as it allows easier navigation without having to use neither the keyboard nor the mouse. In addition to buttons for navigation it also includes a button to hide and show the main menu.</source>
      <translation>Der Wechsel zwischen den Bildern kann auf verschiedene Art und Weise erfolgen. Dies ist über die Tastenkombinationen, über das Hauptmenü oder über schwebende Navigationstasten möglich. Diese schwebenden Tasten wurden speziell für bessere Handhabung mit Touchscreens hinzugefügt. da es eine einfachere Navigation ermöglicht, ohne dass man weder die Tastatur noch die Maus benutzen muss. Zusätzlich zu den Schaltflächen für die Navigation enthält es auch eine Schaltfläche zum Verstecken und Anzeigen des Hauptmenüs.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="177"/>
      <source>show floating navigation buttons</source>
      <translation>schwebende Knöpfe zur Navigation anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="172"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="244"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="226"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="55"/>
      <source>Cache</source>
      <extracomment>Settings title</extracomment>
      <translation>Zwischenspeicher</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="232"/>
      <source>Whenever an image is loaded in full, PhotoQt caches such images in order to greatly improve performance if that same image is shown again soon after. This is done up to a certain memory limit after which the first images in the cache will be removed again to free up the required memory. Depending on the amount of memory available on the system, a higher value can lead to an improved user experience.</source>
      <translation>Wann immer ein Bild vollständig geladen wird, speichert PhotoQt solche Bilder in einem Zwischenspeicher, um diese Bilder das nächste Mal schneller anzuzeigen. Dies geschieht bis zu einer bestimmten Speichergrenze, nach der die ersten Bilder im Zwischenspeicher wieder entfernt werden. Je nach verfügbarem Arbeitsspeicher auf dem System kann ein höherer Wert zu einem verbesserten Benutzererlebnis führen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="171"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="177"/>
      <source>Interpolation</source>
      <extracomment>Settings title</extracomment>
      <translation>Interpolation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="62"/>
      <source>none</source>
      <extracomment>used as in: no margin around image</extracomment>
      <translation>0</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="107"/>
      <source>large images:</source>
      <translation>große Bilder:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="111"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="135"/>
      <source>fit to view</source>
      <translation>in Ansicht anpassen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="115"/>
      <source>load at full scale</source>
      <translation>in voller Größe laden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="131"/>
      <source>small images:</source>
      <translation>kleine Bilder:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="141"/>
      <source>load as-is</source>
      <translation>unverändert laden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="165"/>
      <source>show checkerboard pattern</source>
      <translation>Schachbrettmuster zeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="183"/>
      <source>PhotoQt makes use of interpolation algorithms to show smooth lines and avoid potential artefacts to be shown. However, for small images this can lead to blurry images when no interpolation is necessary. Thus, for small images under the specified threshold PhotoQt can skip the use of interpolation algorithms. Note that both the width and height of an image need to be smaller than the threshold for it to be applied.</source>
      <translation>PhotoQt verwendet Interpolationsalgorithmen, um glatte Linien zu zeigen und mögliche Artefakte zu vermeiden. Für kleine Bilder kann dies jedoch zu ungewollter Unschärfe führen, wenn keine Interpolation erforderlich ist. Daher kann PhotoQt für kleine Bilder unter dem angegebenen Schwellenwert die Verwendung von Interpolationsalgorithmen überspringen. Beachte, dass sowohl die Breite als auch die Höhe eines Bildes kleiner sein müssen als der Schwellenwert um keine Interpolation durchzuführen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="190"/>
      <source>disable interpolation for small images</source>
      <translation>deaktiviere Interpolation für kleine Bilder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="168"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="43"/>
      <source>Margin</source>
      <extracomment>Settings title</extracomment>
      <translation>Rand</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="49"/>
      <source>PhotoQt shows the main image fully stretched across its application window. For an improved visual experience, it can add a small margin of some pixels around the image to not have it stretch completely from edge to edge. Note that once an image is zoomed in the margin might be filled, it only applies to the default zoom level of an image.</source>
      <translation>PhotoQt zeigt das Hauptbild im gesamten Fenster an, von Fensterkante bis Fensterkante. Aus ästhetischen Gründen kann ein kleiner Rand von einigen Pixeln um das Bild herum hinzugefügt werden. Beachte, dass, sobald ein Bild gezoomt wird der Ramen nicht mehr respektiert wird, er wird nur für die Standard-Zoomstufe eines Bildes angewendet.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="169"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="88"/>
      <source>Image size</source>
      <extracomment>Settings title</extracomment>
      <translation>Bildgröße</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="94"/>
      <source>PhotoQt ensures that an image is fully visible when first loaded. To achieve this, large images are zoomed out to fit into the view, but images smaller than the view are left as-is. Alternatively, large images can be loaded at full scale, and small images can be zoomed in to also fit into view. The latter option might result in small images appearing pixelated.</source>
      <translation>PhotoQt stellt sicher, dass ein Bild beim Laden vollständig sichtbar ist. Um dies zu erreichen, werden große Bilder verkleinert, damit diese in die Ansicht zu passen, aber kleinere Bilder bleiben unverändert. Alternativ können große Bilder in voller Größe geladen werden, und kleine Bilder können vergrößert werden um die gesamte Ansicht zu füllen. Letzteres kann dazu führen, dass kleine Bilder etwas unscharf erscheinen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="170"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="152"/>
      <source>Transparency marker</source>
      <extracomment>Settings title</extracomment>
      <translation>Transparenzmarker</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="158"/>
      <source>When an image contains transparent areas, then that area can be left transparent resulting in the background of PhotoQt to show. Alternatively, it is possible to show a checkerboard pattern behind the image, exposing the transparent areas of an image much clearer.</source>
      <translation>Wenn ein Bild transparente Bereiche enthält, kann dieser Bereich transparent gelassen und der Hintergrund von PhotoQt in diesem Bereich angezeigt werden. Alternativ ist es auch möglich, hinter dem Bild ein Schachbrettmuster anzuzeigen, damit die transparenten Bereiche eines Bildes deutlicher werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="46"/>
      <source>It is possible to share an image from PhotoQt directly to imgur.com. This can either be done anonymously or to an imgur.com account. For the former, no setup is required, after a successful upload you are presented with the URL to access and the URL to delete the image. For the latter, PhotoQt first needs to be authenticated to an imgur.com user account.</source>
      <translation>Es ist möglich, ein Bild von PhotoQt direkt auf imgur.com zu teilen, entweder anonym oder zu einem Benutzerkonto dort. Für das anonyme Hochladen ist keine weitere Aktion erforderlich, ansonsten muss PhotoQt zuerst mit einem Konto auf imgur.com verbunden werden. Nach einem erfolgreichen Upload erhalten Sie die URL für den Zugriff auf das Bild, und die URL für das Löschen des Bildes.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="53"/>
      <source>Note that any change here is saved immediately!</source>
      <translation>Jede Änderung hier wird automatisch gespeichert!</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="62"/>
      <source>Authenticated with user account:</source>
      <translation>Mit Benutzerkonto authentifiziert:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="75"/>
      <source>Authenticate</source>
      <extracomment>Written on button, used as in: Authenticate with user account</extracomment>
      <translation>Authentifizieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="77"/>
      <source>Forget account</source>
      <extracomment>Written on button, used as in: Forget user account</extracomment>
      <translation>Konto vergessen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="116"/>
      <source>Switch to your browser and log into your imgur.com account. Then paste the displayed PIN in the field below. Click on the button above again to reopen the website.</source>
      <translation>Zum Browser wechseln und bei imgur.com-Konto anmelden. Die daraufhin angezeigte PIN muss in das untenstehende Feld kopiert werden. Durch erneutes Klicken auf den obigen Knopf kann die Webseite erneut geöffnet werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="159"/>
      <source>An error occured:</source>
      <translation>Ein Fehler ist aufgetreten:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="62"/>
      <source>The background is the area in the back (no surprise there) behind any image that is currently being viewed. By default, PhotoQt is partially transparent with a dark overlay. This is only possible, though, whenever a compositor is available. On some platforms, PhotoQt can fake a transparent background with screenshots taken at startup. Another option is to show a background image (also with a dark overlay) in the background.</source>
      <translation>Der Hintergrund ist der Bereich hinter dem Bild, welches gerade angezeigt wird. Standardmäßig ist der Hintergrund von PhotoQt teilweise transparent mit einer halb-transparenten Farbdeckung. Auf einigen Plattformen kann PhotoQt einen transparenten Hintergrund mittels Bildschirmfotos vortäuschen, die beim Start erstellt werden. Eine weitere Option ist, ein eigenes Hintergrundbild (ebenfalls mit halbßtransparenter Farbdeckung) im Hintergrund anzuzeigen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="75"/>
      <source>real transparency</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>echte Transparenz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="84"/>
      <source>fake transparency</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>falsche Transparenz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="92"/>
      <source>solid background color</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>einfarbiger Hintergrund</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="100"/>
      <source>custom background image</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>benutzerdefiniertes Hintergrundbild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="128"/>
      <source>background image</source>
      <translation>Hintergrundbild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="144"/>
      <source>Click to select an image</source>
      <extracomment>Tooltip for a mouse area, a click on which opens a file dialog for selecting an image</extracomment>
      <translation>Klicken, um ein Bild auszuwählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="175"/>
      <source>scale to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>Skaliert</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="182"/>
      <source>scale and crop to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>Skaliert und beschnitten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="189"/>
      <source>stretch to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>An Fenster anpassen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="196"/>
      <source>center image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>Zentriert</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="203"/>
      <source>tile image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>Gekachelt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="67"/>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="219"/>
      <source>Click on empty background</source>
      <translation>Auf leeren Hintergrund klicken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="225"/>
      <source>The empty background area is the part of the background that is not covered by any image. A click on that area can trigger certain actions, some depending on where exactly the click occured</source>
      <translation>Der leere Hintergrundbereich ist der Teil des Hintergrunds, der von keinem Bild abgedeckt ist. Ein Klick auf diesen Bereich kann bestimmte Aktionen auslösen. Einige hängen davon ab, wo genau der Klick aufgetreten ist</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="238"/>
      <source>no action</source>
      <extracomment>what to do when the empty background is clicked</extracomment>
      <translation>keine Aktion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="246"/>
      <source>close window</source>
      <extracomment>what to do when the empty background is clicked</extracomment>
      <translation>Fenster schließen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="254"/>
      <source>navigate between images</source>
      <extracomment>what to do when the empty background is clicked</extracomment>
      <translation>zwischen Bildern navigieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="262"/>
      <source>toggle window decoration</source>
      <extracomment>what to do when the empty background is clicked</extracomment>
      <translation>Fensterdekoration umschalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="68"/>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="276"/>
      <source>Blurring elements behind other elements</source>
      <extracomment>A settings title</extracomment>
      <translation>Elemente hinter anderen Elementen verwischen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="283"/>
      <source>Whenever an element (e.g., histogram, main menu, etc.) is open, anything behind it can be blurred slightly. This reduces the contrast in the background which improves readability. Note that this requires a slightly higher amount of computations. It also does not work with anything behind PhotoQt that is not part of the window itself.</source>
      <translation>Wenn ein Element (z. B. Histogramm, Hauptmenü) geöffnet ist, kann alles dahinter etwas verschwommen werden. Dies reduziert den Kontrast im Hintergrund, was die Lesbarkeit verbessern kann. Beachte, dass dies etwas mehr von der CPU verlangt. Es ist nicht möglich, die Sachen hinter PhotoQt yu verwischen, die nicht selbst Teil von PhotoQt sind.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="291"/>
      <source>Blur elements in the back</source>
      <translation>Elemente im Hintergrund verwischen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="65"/>
      <source>The context menu contains actions that can be performed related to the currently viewed image. By default is it shown when doing a right click on the background, although it is possible to change that in the shortcuts category. In addition to pre-defined image functions it is also possible to add custom entries to that menu.</source>
      <translation>Das Kontextmenü enthält Aktionen, die im Zusammenhang mit dem aktuell angezeigten Bild ausgeführt werden können. Standardmäßig wird es angezeigt, wenn mit der rechten Maustaste auf den Hintergrund geklickt wird. Es ist jedoch möglich, dies in den Einstellungen für Kurzbefehle zu ändern. Zusätzlich zu den vordefinierten Bildfunktionen ist es auch möglich, dem Menü eigene Einträge hinzuzufügen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="77"/>
      <source>No custom entries exists yet</source>
      <extracomment>The custom entries here are the custom entries in the context menu</extracomment>
      <translation>Noch keine benutzerdefinierten Einträge vorhanden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="108"/>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="157"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="303"/>
      <source>Select</source>
      <extracomment>written on button for selecting a file from the file dialog
----------
written on button in file picker to select an existing executable file</extracomment>
      <translation>Auswählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="121"/>
      <source>entry name</source>
      <extracomment>The entry here refers to the text that is shown in the context menu for a custom entry</extracomment>
      <translation>Name des Eintrages</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="138"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="287"/>
      <source>executable</source>
      <translation>ausführbare Datei</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="153"/>
      <source>Select executable</source>
      <translation>Ausführbare Datei auswählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="183"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="337"/>
      <source>additional flags</source>
      <extracomment>The flags here are additional parameters that can be passed on to an executable
----------
the flags here are parameters specified on the command line</extracomment>
      <translation>zusätzliche Flags</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="199"/>
      <source>quit</source>
      <extracomment>Quit PhotoQt after executing custom context menu entry. Please keep as short as possible!!</extracomment>
      <translation>beenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="230"/>
      <source>Delete entry</source>
      <extracomment>The entry here is a custom entry in the context menu</extracomment>
      <translation>Eintrag löschen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="246"/>
      <source>Add new entry</source>
      <extracomment>The entry here is a custom entry in the context menu</extracomment>
      <translation>Neuen Eintrag hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="256"/>
      <source>Add system applications</source>
      <extracomment>The system applications here refers to any image related applications that can be found automatically on your system</extracomment>
      <translation>Systemanwendungen hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="125"/>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="287"/>
      <source>Duplicate entries in main menu</source>
      <extracomment>The entries here are the custom entries in the context menu</extracomment>
      <translation>Einträge auch im Hauptmenü anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="293"/>
      <source>The custom context menu entries can also be duplicated in the main menu. If enabled, the entries set above will be accesible in both places.</source>
      <translation>Die benutzerdefinierten Einträge im Kontextmenü können im Hauptmenü dupliziert werden. Wenn aktiviert, sind die obigen Einträge an beiden Stellen zugänglich.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="301"/>
      <source>Duplicate in main menu</source>
      <extracomment>Refers to duplicating the custom context menu entries in the main menu</extracomment>
      <translation>Auch im Hauptmenü anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="43"/>
      <source>No action</source>
      <extracomment>Used as descriptor for a screen edge action</extracomment>
      <translation>Keine Aktion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="47"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="56"/>
      <source>Main menu</source>
      <extracomment>Used as descriptor for a screen edge action
----------
Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Hauptmenü</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="74"/>
      <source>Moving the mouse cursor to the edges of the application window can trigger the visibility of some things, like the main menu, thumbnails, or metadata. Here you can choose what is triggered by which window edge. Note that the main menu is fixed to the right window edge and cannot be moved or disabled.</source>
      <translation>Das Verschieben des Mauszeigers an die Kanten des Anwendungsfensters kann die Sichtbarkeit einiger Dinge wie des Hauptmenüs, der Miniaturbilder, oder der Metadaten umschalten. Hier kann ausgewählt werden, was durch welche Fensterkante ausgelöst wird. Beachte, dass das Hauptmenü an der rechten Fensterkante fixiert ist und nicht bewegt oder deaktiviert werden kann.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="176"/>
      <source>The right edge action cannot be changed</source>
      <extracomment>The action here is a screen edge action</extracomment>
      <translation>Die Aktion für den rechten Rand kann nicht geändert werden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="113"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="146"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="207"/>
      <source>Click to change action</source>
      <extracomment>The action here is a screen edge action</extracomment>
      <translation>Zum Ändern der Aktion klicken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="115"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="228"/>
      <source>Sensitivity</source>
      <extracomment>Settings title</extracomment>
      <translation>Sensitivität</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="234"/>
      <source>The edge actions defined above are triggered whenever the mouse cursor gets close to the screen edge. The sensitivity determines how close to the edge the mouse cursor needs to be for this to happen. A value that is too sensitive might cause the edge action to sometimes be triggered accidentally.</source>
      <translation>Die oben definierten Kantenaktionen werden ausgelöst, wenn sich der Mauszeiger in der Nähe einer Bildschirmkante befindet. Die Empfindlichkeit legt fest, wie nahe der Mauszeiger der Kante sein muss, damit diese ausgelöst werden. Ein zu sensibler Wert kann dazu führen, dass die Kantenaktionen unabsichtlich ausgelöst werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="97"/>
      <source>PhotoQt has been translated into a number of different languages. Not all of the languages have a complete translation yet, and new translators are always needed. If you are willing and able to help, that would be greatly appreciated.</source>
      <translation>PhotoQt wurde in verschiedene Sprachen übersetzt. Noch nicht alle Sprachen haben eine vollständige Übersetzung, und neue Übersetzer werden immer gesucht. Wenn du bereit und in der Lage bist, zu helfen, dann wäre ich sehr dankbar.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="159"/>
      <source>Thank you to all who volunteered their time to help translate PhotoQt into other languages!</source>
      <translation>Vielen Dank an alle, die einen Teil ihrer freien Zeit aufgegeben haben, um bei der Übersetzung von PhotoQt zu helfen!</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="169"/>
      <source>If you want to help with the translations, either by translating or by reviewing existing translations, head over to the translation page on Crowdin:</source>
      <translation>Wenn du bei den Übersetzungen helfen möchtest, entweder durch Übersetzen oder durch Überprüfung bestehender Übersetzungen, dann besuche am besten die Übersetzungsseite auf Crowdin:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="188"/>
      <source>Open in browser</source>
      <translation>Im Browser öffnen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="197"/>
      <source>Copy to clipboard</source>
      <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="50"/>
      <source>File dialog</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Dateidialog</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="52"/>
      <source>Map explorer</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Kartenansicht</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="54"/>
      <source>Settings manager</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Einstellungen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="60"/>
      <source>Histogram</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Histogramm</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="62"/>
      <source>Map (current image)</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Karte (aktuelles Bild)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="64"/>
      <source>Scale</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Skalieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="66"/>
      <source>Slideshow setup</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Einstellungen für die Diaschau</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="68"/>
      <source>Slideshow controls</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Diaschau - Steuerelemente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="70"/>
      <source>Rename file</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Datei umbenennen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="72"/>
      <source>Delete file</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Datei löschen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="74"/>
      <source>Export file</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Datei exportieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="76"/>
      <source>About</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Über</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="78"/>
      <source>Imgur</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Imgur</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="80"/>
      <source>Wallpaper</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Hintergrundbild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQMainCategory.qml" line="175"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="82"/>
      <source>Filter</source>
      <extracomment>Noun, not a verb. Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Filter</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="84"/>
      <source>Advanced image sort</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Erweiterte Bildsortierung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="86"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Streaming (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="116"/>
      <source>Almost all of the elements for displaying information or performing actions can either be shown integrated into the main window or shown popped out in their own window. Most of them can also be popped out/in through a small button at the top left corner of each elements.</source>
      <translation>Nahezu alle Elemente zur Anzeige von Informationen oder Aktionen können entweder in das Hauptfenster integriert oder in einem eigenen Fenster angezeigt werden. Die meisten von ihnen können auch über einen kleinen Knopf in der oberen linken Ecke in ein eigenes Fenster verschoben werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="200"/>
      <source>Two of the elements can be kept open after they performed their action. These two elements are the file dialog and the map explorer (if available). Both of them can be kept open after a file is selected and loaded in the main view allowing for quick and convenient browsing of images.</source>
      <translation>Zwei der Elemente können nach Ausführung ihrer Aktion geöffnet bleiben. Diese beiden Elemente sind der Dateidialog und der Karten-Explorer (falls verfügbar). Beide können nach Auswahl und Laden einer Datei in der Hauptansicht geöffnet bleiben, um eine schnelle und bequeme Durchsuchung von Bildern zu ermöglichen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="87"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="194"/>
      <source>Keep popouts open</source>
      <extracomment>Settings title</extracomment>
      <translation>Popouts offen halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="227"/>
      <source>keep file dialog open</source>
      <translation>Dateidialog offen halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="268"/>
      <source>keep map explorer open</source>
      <translation>Kartenexplorer offen halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="88"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="299"/>
      <source>Pop out when window is small</source>
      <extracomment>Settings title</extracomment>
      <translation>Pop-out bei kleinem Anwendungsfenster</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="305"/>
      <source>Some elements might not be as usable or function well when the window is too small. Thus it is possible to force such elements to be popped out automatically whenever the application window is too small.</source>
      <translation>Einige Elemente sind möglicherweise schwieriger zu benutzen, wenn das Fenster zu klein ist. Daher ist es möglich, solche Elemente automatisch in ein eigenes Fenster zu verschieben, wenn das Anwendungsfenster zu klein ist.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="312"/>
      <source>pop out when application window is small</source>
      <translation>Pop-out, wenn das Anwendungsfenster klein ist</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="129"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="130"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="46"/>
      <source>Status info</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Statusinfo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="52"/>
      <source>The status information refers to the set of information shown in the top left corner of the screen. This typically includes the filename of the currently viewed image and information like the zoom level, rotation angle, etc. The exact set of information and their order can be adjusted as desired.</source>
      <translation>Die Statusinformationen beziehen sich auf die Informationen, die in der oberen linken Ecke desFensters angezeigt werden. Dies umfasst in der Regel den Dateinamen des aktuell angezeigten Bildes sowie Informationen wie die Zoomstufe, den Drehwinkel, usw. Die genaue Auswahl der Informationen und ihre Reihenfolge kann nach Belieben angepasst werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="59"/>
      <source>show status information</source>
      <translation>Statusinformationen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="93"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="228"/>
      <source>counter</source>
      <extracomment>Please keep short! The counter shows where we are in the folder.</extracomment>
      <translation>Zähler</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="95"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="230"/>
      <source>filename</source>
      <extracomment>Please keep short!</extracomment>
      <translation>Dateiname</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="97"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="232"/>
      <source>filepath</source>
      <extracomment>Please keep short!</extracomment>
      <translation>Dateipfad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="99"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="234"/>
      <source>resolution</source>
      <extracomment>Please keep short! This is the image resolution.</extracomment>
      <translation>Auflösung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="101"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="236"/>
      <source>zoom</source>
      <extracomment>Please keep short! This is the current zoom level.</extracomment>
      <translation>Zoom</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="105"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="240"/>
      <source>filesize</source>
      <extracomment>Please keep short! This is the filesize of the current image.</extracomment>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="254"/>
      <source>add</source>
      <extracomment>This is written on a button that is used to add a selected block to the status info section.</extracomment>
      <translation>hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="131"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="269"/>
      <source>Font size</source>
      <extracomment>Settings title, the font sized of the status information</extracomment>
      <translation>Schriftgröße</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="275"/>
      <source>The size of the status info is determined by the font size of the text.</source>
      <translation>Die Größe der Statusinformation wird durch die Schriftgröße des Textes bestimmt.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="132"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="147"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="311"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="180"/>
      <source>Hide automatically</source>
      <extracomment>Settings title</extracomment>
      <translation>Automatisch ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="317"/>
      <source>The status info can either be shown at all times, or it can be hidden automatically based on different criteria. It can either be hidden unless the mouse cursor is near the top edge of the screen or until the mouse cursor is moved anywhere. After a specified timeout it will then hide again. In addition to these criteria, it can also be shown shortly whenever the image changes.</source>
      <translation>Die Statusinformationen können entweder jederzeit angezeigt werden oder sie können automatisch nach unterschiedlichen Kriterien ausgeblendet werden. Sie können entweder versteckt bleiben, es sei denn, der Mauszeiger befindet sich am oberen Rand des Bildschirms, oder es sei denn der Mauszeiger wird irgendwo bewegt. Nach einem bestimmten Timeout werden sie dann wieder ausgeblendet. Zusätzlich zu diesen Kriterien können sie auch bei jeder Änderung des Bildes kurz angezeigt werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="328"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="197"/>
      <source>keep always visible</source>
      <extracomment>visibility status of the status information
----------
visibility status of the window buttons</extracomment>
      <translation>immer sichtbar halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="336"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="205"/>
      <source>only show with any cursor move</source>
      <extracomment>visibility status of the status information
----------
visibility status of the window buttons</extracomment>
      <translation>nur bei Bewegung des Mauszeigers anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="344"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="213"/>
      <source>only show when cursor near top edge</source>
      <extracomment>visibility status of the status information
----------
visibility status of the window buttons</extracomment>
      <translation>nur zeigen, wenn Mauszeiger sich dem oberen Rand nähert</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="355"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="224"/>
      <source>hide again after timeout:</source>
      <extracomment>the status information can be hidden automatically after a set timeout
----------
the window buttons can be hidden automatically after a set timeout</extracomment>
      <translation>nach Timeout wieder ausblenden:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="392"/>
      <source>also show when image changes</source>
      <extracomment>Refers to the status information&apos;s auto-hide feature, this is an additional case it can be shown</extracomment>
      <translation>auch anzeigen, wenn sich das Bild ändert</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="133"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="404"/>
      <source>Window management</source>
      <extracomment>Settings title</extracomment>
      <translation>Fensterverwaltung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="410"/>
      <source>By default it is possible to drag the status info around as desired. However, it is also possible to use the status info for managing the window itself. When enabled, dragging the status info will drag the window around, and double clicking the status info will toggle the maximized status of the window.</source>
      <translation>Standardmäßig ist es möglich, die Statusinformationen nach Belieben zu verschieben. Es ist aber auch möglich, die Statusinformationen zur Verwaltung des Fensters selbst zu verwenden. Wenn aktiviert, wird das Ziehen der Statusinformationen das Fenster bewegen, und ein Doppelklick auf die Statusinformationen schaltet den maximierten Status des Fensters um.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="417"/>
      <source>manage window through status info</source>
      <translation>verwalte Fenster mittels Statusinfo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="145"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="49"/>
      <source>Fullscreen or window mode</source>
      <extracomment>Settings title</extracomment>
      <translation>Vollbild- oder Fenstermodus</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="55"/>
      <source>There are two main states that the applicaiton window can be in. It can either be in fullscreen mode or in window mode. In fullscreen mode, PhotoQt will act more like a floating layer that allows you to quickly look at images. In window mode, PhotoQt can be used in combination with other applications. When in window mode, it can also be set to always be above any other windows, and to remember the window geometry in between sessions.</source>
      <translation>Es gibt zwei Hauptzustände, in denen sich das Anwendungsfenster befinden kann. Es kann sich entweder im Vollbildmodus oder im Fenstermodus befinden. Im Vollbildmodus wird PhotoQt eher wie eine schwebende Ebene agieren. Im Fenstermodus kann PhotoQt in Kombination mit anderen Anwendungen verwendet werden. Im Fenstermodus kann es auch so eingestellt werden, dass es sich immer über jedem anderen Fenster befindet, und dass die Fenstergeometrie zwischen verschiedenen Sitzungen gemerkt wird.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="63"/>
      <source>fullscreen mode</source>
      <translation>Vollbildmodus</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="70"/>
      <source>window mode</source>
      <translation>Fenstermodus</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="81"/>
      <source>keep above other windows</source>
      <translation>über anderen Fenstern halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="89"/>
      <source>remember its geometry</source>
      <extracomment>remember the geometry of PhotoQts window between sessions</extracomment>
      <translation>an Geometrie erinnern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="99"/>
      <source>enable window decoration</source>
      <translation>Fensterdekoration aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="146"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="111"/>
      <source>Window buttons</source>
      <extracomment>Settings title</extracomment>
      <translation>Fensterknöpfe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="117"/>
      <source>PhotoQt can show some integrated window buttons for basic window managements both when shown in fullscreen and when in window mode. In window mode with window decoration enabled it can either hide or show buttons from its integrated set that are duplicates of buttons in the window decoration. For help with navigating through a folder, small left/right arrows for navigation and a menu button can also be added next to the window buttons.</source>
      <translation>PhotoQt kann einige integrierte Fenstertasten für eine einfache Fensterverwaltung sowohl im Vollbildmodus als auch im Fenstermodus anzeigen. Im Fenstermodus mit aktivierter Fensterdekoration können duplizierte Schaltflächen aus dem integrierten Set entweder ausgeblendet oder angezeigt werden. Um beim Navigieren durch einen Ordner zu helfen, können zwei kleine Pfeile für die Navigation und eine Menü-Taste neben den Fenstertasten hinzugefügt werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="124"/>
      <source>show integrated window buttons</source>
      <translation>integrierte Fenstertasten anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="133"/>
      <source>duplicate buttons from window decoration</source>
      <translation>Fenstertasten duplizieren, die sich in der Fensterdekoration befinden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="142"/>
      <source>add navigation buttons</source>
      <translation>Knöpfe für die Navigation hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="186"/>
      <source>The window buttons can either be shown at all times, or they can be hidden automatically based on different criteria. They can either be hidden unless the mouse cursor is near the top edge of the screen or until the mouse cursor is moved anywhere. After a specified timeout they will then hide again.</source>
      <translation>Die Fenstertasten können entweder jederzeit angezeigt werden oder sie können automatisch nach unterschiedlichen Kriterien ausgeblendet werden. Sie können entweder versteckt bleiben, es sei denn, der Mauszeiger befindet sich am oberen Rand des Bildschirms, oder es sei denn der Mauszeiger wird irgendwo bewegt. Nach einem bestimmten Timeout werden sie dann wieder ausgeblendet.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="286"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="41"/>
      <source>Auto Rotation</source>
      <extracomment>Settings title</extracomment>
      <translation>Automatische Drehung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="47"/>
      <source>When an image is taken with the camera turned on its side, some cameras store that rotation in the metadata. PhotoQt can use that information to display an image the way it was meant to be viewed. Disabling this will load all photos without any rotation applied by default.</source>
      <translation>Wenn ein Bild mit der Kamera im Querformat aufgenommen wird, speichern einige Kameras diese Drehung in den Metadaten. PhotoQt kann diese Informationen verwenden, um ein Bild so anzuzeigen, wie es ursprünglich gedacht war. Das Deaktivieren dieser Funktion lädt alle Fotos standardmäßig ohne jegliche Rotation.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="54"/>
      <source>Apply default rotation automatically</source>
      <translation>Standardrotation automatisch anwenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="287"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="66"/>
      <source>GPS map</source>
      <extracomment>Settings title</extracomment>
      <translation>GPS-Karte</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="72"/>
      <source>Some cameras store the location of where the image was taken in the metadata of its images. PhotoQt can use that information in multiple ways. It can show a floating embedded map with a pin on that location, and it can show the GPS coordinates in the metadata element. In the latter case, a click on the GPS coordinates will open the location in an online map service, the choice of which can be set here.</source>
      <translation>Einige Kameras speichern den Standort, an dem das Bild aufgenommen wurde, in den Metadaten ihrer Bilder. PhotoQt kann diese Informationen auf verschiedene Arten nutzen. Es kann eine schwebende integrierte Karte mit einer Markierung an diesem Ort anzeigen, und es kann die GPS-Koordinaten im Metadatenelement anzeigen. In letzterem Fall öffnet ein Klick auf die GPS-Koordinaten den Ort in einem Online-Kartendienst. Hier kann dieser Kartendienst ausgewählt werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="288"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="108"/>
      <source>Floating element</source>
      <extracomment>Settings title</extracomment>
      <translation>Schwebendes Element</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="114"/>
      <source>The metadata element can be show in two different ways. It can either be shown hidden behind one of the screen edges and shown when the cursor is close to said edge. Or it can be shown as floating element that can be triggered by shortcut and stays visible until manually hidden.</source>
      <translation>Das Metadatenelement kann auf zwei verschiedene Arten angezeigt werden. Es kann entweder versteckt hinter einer der Bildschirmkanten sein und angezeigt werden, wenn der Mauszeiger sich diesen Rand nähert. Oder es kann als schwebendes Element angezeigt werden, das durch eine Tastenkombination angezeigt werden kann und solange sichtbar bleibt, bis es manuell wieder ausgeblendet wird.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="125"/>
      <source>hide behind screen edge</source>
      <translation>hinter Bildschirmrand verstecken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="132"/>
      <source>use floating element</source>
      <translation>schwebendes Element verwenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="295"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="45"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="58"/>
      <source>Show face tags</source>
      <extracomment>Settings title</extracomment>
      <translation>Gesichter anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="51"/>
      <source>PhotoQt can read face tags stored in its metadata. It offers a great deal of flexibility in how and when the face tags are shown. It is also possible to remove and add face tags using the face tagger interface (accessible through the context menu or by shortcut).</source>
      <translation>PhotoQt kann Gesichter lesen, die in den Metadaten gespeichert sind. Es bietet eine hohe Flexibilität hinsichtlich der Bedingungen zur Anzeige der Gesichter. Es ist auch möglich, Gesichter zu entfernen und hinzuzufügen (über das Kontextmenü oder durch einen Kurzbefehl).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="76"/>
      <source>It is possible to adjust the border shown around tagged faces and the font size used for the displayed name. For the border, not only the width but also the color can be specified.</source>
      <translation>Es ist möglich, den um markierte Gesichter angezeigten Rahmen und die Schriftgröße für den angezeigten Namen anzupassen. In Bezug auf den Rahmen kann nicht nur die Breite, sondern auch die Farbe festgelegt werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="83"/>
      <source>font size:</source>
      <translation>Schriftgröße:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="110"/>
      <source>Show border around face tags</source>
      <translation>Rahmen um Gesichter anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="154"/>
      <source>red</source>
      <translation>rot</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="155"/>
      <source>green</source>
      <translation>green</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="156"/>
      <source>blue</source>
      <translation>blau</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="157"/>
      <source>alpha</source>
      <translation>alpha</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="235"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="297"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="186"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="207"/>
      <source>Visibility</source>
      <extracomment>Settings title</extracomment>
      <translation>Sichtbarkeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="192"/>
      <source>The face tags can be shown under different conditions. It is possible to always show all face tags, to show all face tags when the mouse cursor is moving across the image, or to show an individual face tag only when the mouse cursor is above a face tags.</source>
      <translation>Die Gesichter können unter verschiedenen Bedingungen angezeigt werden. Es ist möglich, immer alle Gesichter anzuzeigen, oder alle Gesichter anzuzeigen, wenn der Mauszeiger bewegt wird, oder ein individuelles Gesicht nur dann anzuzeigen, wenn der Mauszeiger sich über dem Gesicht befindet.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="204"/>
      <source>always show all</source>
      <extracomment>used as in: always show all face tags</extracomment>
      <translation>immer alle anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="212"/>
      <source>show one on hover</source>
      <extracomment>used as in: show one face tag on hover</extracomment>
      <translation>zeige eines beim Bewegen des Mauszeigers</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="220"/>
      <source>show all on hover</source>
      <extracomment>used as in: show one face tag on hover</extracomment>
      <translation>zeige alle beim Bewegen des Mauszeigers</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="48"/>
      <source>file name</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Dateiname</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="50"/>
      <source>file type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Dateityp</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="52"/>
      <source>file size</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="54"/>
      <source>image #/#</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Bild #/#</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="56"/>
      <source>dimensions</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Bildgröße</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="58"/>
      <source>copyright</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Copyright</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="60"/>
      <source>exposure time</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Belichtungszeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="62"/>
      <source>flash</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Blitz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="64"/>
      <source>focal length</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Brennweite</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="66"/>
      <source>f-number</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Blendenzahl</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="68"/>
      <source>GPS position</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>GPS-Position</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="71"/>
      <source>keywords</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Schlüsselwörter</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="73"/>
      <source>light source</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Lichtquelle</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="75"/>
      <source>location</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Standort</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="77"/>
      <source>make</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Hersteller</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="79"/>
      <source>model</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Modell</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="81"/>
      <source>scene type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Szenenart</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="83"/>
      <source>software</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Software</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="85"/>
      <source>time photo was taken</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Aufnahmezeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="110"/>
      <source>Whenever an image is loaded PhotoQt tries to find as much metadata about the image as it can. The found information is then displayed in the metadata element that can be accesses either through one of the screen edges or as floating element. Since not all information might be wanted by everyone, individual information labels can be disabled.</source>
      <translation>Immer wenn ein Bild geladen wird, versucht PhotoQt, so viele Metadaten wie möglich über das Bild zu finden. Die gefundenen Informationen werden dann im Metadatenelement angezeigt, das entweder über eine der Fensterkanten oder als schwebendes Element aufgerufen werden kann. Da nicht jeder alle Informationen benötigt, können individuelle Informationslabels deaktiviert werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="189"/>
      <source>Check all</source>
      <extracomment>used as in: check all checkboxes</extracomment>
      <translation>Alles auswählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="200"/>
      <source>Check none</source>
      <extracomment>used as in: check none of the checkboxes</extracomment>
      <translation>Nichts auswählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="313"/>
      <location filename="../qml/settingsmanager/settings/session/PQInstance.qml" line="37"/>
      <source>Single instance</source>
      <extracomment>Settings title</extracomment>
      <translation>Einzelne Instanz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQInstance.qml" line="43"/>
      <source>PhotoQt can either run in single-instance mode or allow multiple instances to run at the same time. The former has the advantage that it is possible to interact with a running instance of PhotoQt through the command line (in fact, this is a requirement for that to work). The latter allows, for example, for the comparison of multiple images side by side.</source>
      <translation>PhotoQt kann entweder im Einzelinstanz-Modus ausgeführt werden oder es können mehrere Instanzen gleichzeitig laufen. Ersteres ermöglicht die Interaktion mit einer laufenden Instanz von PhotoQt über die Befehlszeile (dies ist eine Voraussetzung, damit dies möglich ist). Letzteres ermöglicht beispielsweise den Vergleich von mehreren Bildern nebeneinander.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQInstance.qml" line="54"/>
      <source>run a single instance only</source>
      <translation>nur eine einzige Instanz ausführen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQInstance.qml" line="60"/>
      <source>allow multiple instances</source>
      <translation>mehrere Instanzen erlauben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="318"/>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="38"/>
      <source>Reopen last image</source>
      <extracomment>Settings title</extracomment>
      <translation>Letztes Bild wieder öffnen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="44"/>
      <source>When PhotoQt is started normally, by default an empty window is shown with the prompt to open an image from the file dialog. Alternatively it is also possible to reopen the image that was last loaded in the previous session.</source>
      <translation>Wenn PhotoQt normal gestartet wird, wird standardmäßig ein leeres Fenster angezeigt mit der Aufforderung, ein Bild über den Dateidialog zu öffnen. Alternativ ist es auch möglich, das zuletzt in der vorherigen Sitzung geladene Bild automatisch erneut zu öffnen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="55"/>
      <source>start with blank session</source>
      <translation>mit leerer Sitzung beginnen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="61"/>
      <source>reopen last used image</source>
      <translation>letztes Bild wieder öffnen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="319"/>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="75"/>
      <source>Remember changes</source>
      <extracomment>Settings title</extracomment>
      <translation>Änderungen merken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="81"/>
      <source>Once an image has been loaded it can be manipulated freely by zooming, rotating, or mirroring the image. Once another image is loaded any such changes are forgotten. If preferred, it is possible for PhotoQt to remember any such manipulations per session. Note that once PhotoQt is closed these changes will be forgotten in any case.</source>
      <translation>Sobald ein Bild geladen wurde, kann es beliebig durch Zoomen, Drehen oder Spiegeln manipuliert werden. Wenn ein anderes Bild geladen wird, werden solche Änderungen wieder vergessen. Wenn gewünscht, kann PhotoQt diese Manipulationen pro Sitzung speichern. Beachten Sie jedoch, dass diese Änderungen trotz allem vergessen werden, sobald PhotoQt geschlossen wird.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="92"/>
      <source>forget changes when other image loaded</source>
      <translation>Änderungen vergessen, wenn ein anderes Bild geladen wird</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="98"/>
      <source>remember changes per session</source>
      <translation>Änderungen pro Sitzung speichern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="325"/>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="41"/>
      <source>Tray Icon</source>
      <extracomment>Settings title</extracomment>
      <translation>Symbol im Infobereich der Systemleiste</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="47"/>
      <source>PhotoQt can show a small icon in the system tray. The tray icon provides additional ways to control and interact with the application. It is also possible to hide PhotoQt to the system tray instead of closing. By default a colored version of the tray icon is used, but it is also possible to use a monochrome version.</source>
      <translation>PhotoQt kann ein kleines Symbol im Infobereich der Systemleiste anzeigen. Das Symbol in der Systemleiste bietet zusätzliche Möglichkeiten zur Steuerung und Interaktion mit der Anwendung. Es ist auch möglich, PhotoQt in die Systemleiste zu minimieren, anstatt es zu schließen. Standardmäßig wird eine farbige Version des Symbols in der Systemleiste verwendet, es ist jedoch auch möglich, eine monochrome Version zu verwenden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="58"/>
      <source>Show tray icon</source>
      <translation>Zeige Symbol im Infobereich der Systemleiste</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="66"/>
      <source>monochrome icon</source>
      <translation>monochromes Symbol</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="74"/>
      <source>hide to tray icon instead of closing</source>
      <translation>verstecken statt schließen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="326"/>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="88"/>
      <source>Reset when hiding</source>
      <extracomment>Settings title</extracomment>
      <translation>Beim Verstecken zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="94"/>
      <source>When hiding PhotoQt in the system tray, it is possible to reset PhotoQt to its initial state, thus freeing most of the memory tied up by caching. Note that this will also unload any loaded folder and image.</source>
      <translation>Mit dem Verstecken von PhotoQt in die Systemleiste kann PhotoQt gleichzeitig auch auf den Anfangs-Zustand zurückzusetzen und dadurch den Großteil des benutzten Speichers wieder freizugeben. Beachte jedoch, dass dadurch auch geladene Ordner und Bilder entladen werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="102"/>
      <source>reset session when hiding</source>
      <translation>Sitzung beim Ausblenden zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="369"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="43"/>
      <source>Move image with mouse</source>
      <extracomment>Settings title</extracomment>
      <translation>Das Bild mit der Maus bewegen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="49"/>
      <source>PhotoQt can use both the left button of the mouse and the mouse wheel to move the image around. In that case, however, these actions are not available for shortcuts anymore, except when combined with one or more modifier buttons (Alt, Ctrl, etc.).</source>
      <translation>PhotoQt kann sowohl die linke Maustaste als auch das Mausrad verwenden, um das Bild zu bewegen. In diesem Fall stehen diese Aktionen jedoch nicht mehr als Tastenkürzel zur Verfügung, es sei denn, sie werden in Kombination mit einer oder mehreren Umschalttasten (Alt, Strg, etc.) verwendet.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="60"/>
      <source>move image with left button</source>
      <translation>Bild mit linker Maustaste bewegen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="67"/>
      <source>move image with mouse wheel</source>
      <translation>Bild mit Mausrad bewegen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="151"/>
      <source>very sensitive</source>
      <extracomment>used as in: very sensitive mouse wheel</extracomment>
      <translation>sehr empfindlich</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="161"/>
      <source>not sensitive</source>
      <extracomment>used as in: not at all sensitive mouse wheel</extracomment>
      <translation>unempfindlich</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="185"/>
      <source>hide cursor after timeout</source>
      <translation>Mauszeiger nach Timeout ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="370"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="81"/>
      <source>Double click</source>
      <extracomment>Settings title</extracomment>
      <translation>Doppelklick</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="87"/>
      <source>A double click is defined as two clicks in quick succession. This means that PhotoQt will have to wait a certain amount of time to see if there is a second click before acting on a single click. Thus, the threshold (specified in milliseconds) for detecting double clicks should be as small as possible while still allowing for reliable detection of double clicks. Setting this value to zero disables double clicks and treats them as two distinct single clicks.</source>
      <translation>Ein Doppelklick ist als zwei Klicks schnell hintereinander definiert. Das bedeutet, dass PhotoQt eine gewisse Zeitspanne abwarten muss, um zu prüfen, ob ein zweiter Klick erfolgt, bevor es auf einen einfachen Klick reagiert. Daher sollte die Schwelle (in Millisekunden angegeben) zur Erkennung von Doppelklicks so klein wie möglich sein, so lange sie immer noch eine zuverlässige Erkennung von Doppelklicks ermöglicht. Das Setzen dieses Werts auf 0 deaktiviert die Erkennnung von Doppelklicks und behandelt sie als zwei separate einfache Klicks.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="371"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="132"/>
      <source>Mouse wheel</source>
      <extracomment>Settings title</extracomment>
      <translation>Mausrad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="138"/>
      <source>Depending on any particular hardware, the mouse wheel moves either a set amount each time it is moved, or relative to how long/fast it is moved. The sensitivity allows to account for very sensitive hardware to decrease the likelihood of accidental/multiple triggers caused by wheel movement.</source>
      <translation>Abhängig von der jeweiligen Hardware bewegt sich das Mausrad entweder jedes Mal um einen festgelegten Betrag oder in Relation zur Dauer/Geschwindigkeit der Drehung. Die Empfindlichkeit kontrolliert dieses Verhalten, und kann damit die Wahrscheinlichkeit von unbeabsichtigten/mehrfachen Auslösen von Kurzbefehlen durch Mausradbewegungen zu verringern.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="372"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="172"/>
      <source>Hide mouse cursor</source>
      <extracomment>Settings title</extracomment>
      <translation>Mauszeiger ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="178"/>
      <source>Whenever an image is viewed and mouse cursor rests on the image it is possible to hide the mouse cursor after a set timeout. This way the cursor does not get in the way of actually viewing an image.</source>
      <translation>Wenn ein Bild angezeigt wird und der Mauszeiger auf dem Bild nicht bewegt wird, dann ist es möglich, den Mauszeiger nach Ablauf einer festgelegten Zeit auszublenden. Auf diese Weise stört der Mauszeiger nicht beim Betrachten der Bilder.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="68"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="268"/>
      <source>Viewing images</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation>Bildansicht</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="70"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="269"/>
      <source>Current image</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation>Aktuelles Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="72"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="270"/>
      <source>Current folder</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation>Aktueller Ordner</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="76"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="272"/>
      <source>Other</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation>Sonstiges</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="78"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="273"/>
      <source>External</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation>Extern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="82"/>
      <source>These actions affect the behavior of PhotoQt when viewing images. They include actions for navigating between images and manipulating the current image (zoom, flip, rotation) amongst others.</source>
      <translation>Diese Aktionen beeinflussen das Verhalten von PhotoQt beim Betrachten von Bildern. Sie beinhalten unter anderem Aktionen zur Navigation zwischen den Bildern und zur Manipulation des aktuellen Bildes (zoom, spiegeln, drehen).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="83"/>
      <source>These actions are certain things that can be done with the currently viewed image. They typically do not affect any of the other images.</source>
      <translation>Diese Aktionen sind bestimmte Dinge, die mit dem aktuell angezeigten Bild gemacht werden können. Sie betreffen in der Regel keine der anderen Bilder.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="84"/>
      <source>These actions affect the currently loaded folder as a whole and not just single images.</source>
      <translation>Diese Aktionen betreffen den aktuell geladenen Ordner als Ganzes und nicht nur einzelne Bilder.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="85"/>
      <source>These actions affect the status and behavior of various interface elements regardless of the status of any possibly loaded image.</source>
      <translation>Diese Aktionen beeinflussen den Status und das Verhalten verschiedener Elemente der Benutzeroberfläche, unabhängig vom Status eines möglicherweise geladenen Bildes.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="86"/>
      <source>These actions do not fit into any other category.</source>
      <translation>Diese Aktionen passen in keine andere Kategorie.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="87"/>
      <source>Here any external executable can be set as shortcut action. The button with the three dots can be used to select an executable with a file dialog.</source>
      <translation>Hier kann jedes externe Programm als Aktion gesetzt werden. Mit dem Knopf mit den drei Punkten können Sie eine ausführbare Datei mithilfe eines Dateidialogs auswählen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="111"/>
      <source>Shortcut actions</source>
      <translation>Verknüpfungsaktionen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="275"/>
      <source>Select an executable</source>
      <translation>Eine ausführbare Datei auswählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="293"/>
      <source>Click here to select an executable.</source>
      <translation>Hier klicken, um eine ausführbare Datei auszuwählen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="327"/>
      <source>Additional flags and options to pass on:</source>
      <translation>Zusätzliche Flags und Optionen:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="342"/>
      <source>Note that relative file paths are not supported, however, you can use the following placeholders:</source>
      <translation>Bitte beachte, dass relative Dateipfade nicht unterstützt werden. Die folgenden Platzhalter können jedoch verwendet werden:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="343"/>
      <source>filename including path</source>
      <translation>Dateiname inklusive Pfad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="344"/>
      <source>filename without path</source>
      <translation>Dateiname ohne Pfad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="345"/>
      <source>directory containing file</source>
      <translation>Verzeichnis der Datei</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="346"/>
      <source>If you type out a path, make sure to escape spaces accordingly by prepending a backslash:</source>
      <translation>Wenn ein Pfad hinzugefügt wird, dann stelle bitter sicher, dass Leerzeichen entsprechend maskiert werden, indem einen Backslash vorangestellt wird:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="358"/>
      <source>quit after calling executable</source>
      <translation>beenden nach Aufruf der ausführbaren Datei</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="367"/>
      <source>Save external action</source>
      <translation>Externe Aktion speichern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="125"/>
      <source>Add New Shortcut</source>
      <translation>Neuen Kurzbefehl hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="125"/>
      <source>Set new shortcut</source>
      <translation>Neuen Kurzbefehl festlegen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="147"/>
      <source>Perform a mouse gesture here or press any key combo</source>
      <translation>Hier eine Mausgeste ausführen oder eine beliebige Tastenkombination drücken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="179"/>
      <source>The left button is used for moving the main image around.</source>
      <translation>Die linke Taste wird verwendet, um das Hauptbild zu verschieben.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="180"/>
      <source>It can be used as part of a shortcut only when combined with modifier buttons (Alt, Ctrl, etc.).</source>
      <translation>Es kann nur als Teil eines Kurzbefehls verwendet werden, wenn es mit einer oder mehreren Umschalttasten kombiniert wird (Alt, Strg, etc.).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="31"/>
      <source>Next image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Nächstes Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="33"/>
      <source>Previous image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Vorheriges Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="35"/>
      <source>Go to first image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Gehe zum ersten Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="37"/>
      <source>Go to last image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Gehe zum letzten Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="39"/>
      <source>Zoom In</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Hineinzoomen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="41"/>
      <source>Zoom Out</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Herauszoomen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="43"/>
      <source>Zoom to Actual Size</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zoom auf tatsächliche Größe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="45"/>
      <source>Reset Zoom</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zoom zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="47"/>
      <source>Rotate Right</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Nach Rechts drehen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="49"/>
      <source>Rotate Left</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Nach Links drehen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="51"/>
      <source>Reset Rotation</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Drehung zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="53"/>
      <source>Flip Horizontally</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Horizontal spiegeln</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="55"/>
      <source>Flip Vertically</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Vertikal spiegeln</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="57"/>
      <source>Load a random image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ein zufälliges Bild laden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="59"/>
      <source>Hide/Show face tags (stored in metadata)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Markierungen für Gesichter anzeigen/verstecken (in den Metadaten gespeichert)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="61"/>
      <source>Toggle: Fit in window</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Umschalten: An Fenstergröße anpassen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="63"/>
      <source>Toggle: Show always actual size by default</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Umschalten: Standardmäßig immer tatsächliche Größe anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="65"/>
      <source>Stream content to Chromecast device</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Inhalt auf Chromecast-Gerät übertragen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="67"/>
      <source>Move view left</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ansicht nach links verschieben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="69"/>
      <source>Move view right</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ansicht nach rechts verschieben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="71"/>
      <source>Move view up</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ansicht nach oben verschieben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="73"/>
      <source>Move view down</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ansicht nach unten verschieben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="75"/>
      <source>Go to left edge of image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zum linken Bildrand gehen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="77"/>
      <source>Go to right edge of image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zum rechten Bildrand gehen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="79"/>
      <source>Go to top edge of image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zum oberen Bildrand gehen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="81"/>
      <source>Go to bottom edge of image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zum unteren Bildrand gehen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="87"/>
      <source>Show Histogram</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zeige Histogramm</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="89"/>
      <source>Show Image on Map</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Bild auf Karte anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="91"/>
      <source>Enter viewer mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Betrachtermodus aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="93"/>
      <source>Scale Image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Bild skalieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="95"/>
      <source>Rename File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei umbenennen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="97"/>
      <source>Delete File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei löschen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="99"/>
      <source>Delete File (without confirmation)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei löschen (ohne Bestätigung)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="101"/>
      <source>Copy File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei an einen neuen Ort kopieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="103"/>
      <source>Move File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei an einen neuen Ort verschieben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="105"/>
      <source>Copy Image to Clipboard</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Bild in Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="107"/>
      <source>Save image in another format</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Bild in einem anderen Format speichern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="109"/>
      <source>Print current photo</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Aktuelles Foto drucken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="111"/>
      <source>Set as Wallpaper</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Als Hintergrund setzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="113"/>
      <source>Upload to imgur.com (anonymously)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Auf imgur.com hochladen (anonym)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="115"/>
      <source>Upload to imgur.com user account</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zu Benutzerkonto auf imgur.com hochladen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="117"/>
      <source>Play/Pause animation/video</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Animation/Video abspielen/pausieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="119"/>
      <source>Start tagging faces</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Gesichter markieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="125"/>
      <source>Open file (browse images)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei öffnen (Bilder durchsuchen)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="127"/>
      <source>Show map explorer</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Kartenexplorer anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="129"/>
      <source>Filter images in folder</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Bilder im Ordner filtern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="131"/>
      <source>Advanced image sort (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Erweiterte Bildsortierung (Setup)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="133"/>
      <source>Advanced image sort (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Erweiterte Bildsortierung (Schnellstart)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="135"/>
      <source>Start Slideshow (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Diaschau starten (Setup)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="137"/>
      <source>Start Slideshow (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Diaschau starten (Schnellstart)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="143"/>
      <source>Show Context Menu</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Kontextmenü zeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="145"/>
      <source>Hide/Show main menu</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Hauptmenü anzeigen/verstecken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="147"/>
      <source>Hide/Show metadata</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Metadaten verstecken/anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="149"/>
      <source>Hide/Show thumbnails</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Miniaturbilder anzeigen/verstecken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="151"/>
      <source>Show floating navigation buttons</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Schwebende Knöpfe zur Navigation anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="153"/>
      <source>Toggle fullscreen mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Vollbildmodus umschalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="155"/>
      <source>Close window (hides to system tray if enabled)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Fenster schließen (optional in die Systemleiste verstecken)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="157"/>
      <source>Quit PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>PhotoQt beenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="164"/>
      <source>Show Settings</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Einstellungen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="166"/>
      <source>About PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Über PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="168"/>
      <source>Show log/debug messages</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Protokoll- und Debugmeldungen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="170"/>
      <source>Reset current session</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Aktuelle Sitzung zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="172"/>
      <source>Reset current session and hide window</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Aktuelle Sitzung zurücksetzen und Fenster verstecken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="210"/>
      <source>Shortcuts are grouped by key combination. Multiple actions can be set for each group of key combinations, with the option of cycling through them one by one, or executing all of them at the same time. When cycling through them one by one, a timeout can be set after which the cycle will be reset to the beginning. Any group that has no key combinations set will be deleted when saving all changes.</source>
      <translation>Kurzbefehle sind gruppiert nach Tastenkombinationen. Für jede Gruppe von Tastenkombinationen können mehrere Aktionen festgelegt werden, mit der Möglichkeit, diese entweder einzeln nacheinander oder alle gleichzeitig auszuführen. Beim Ausführen nacheinander kann ein Zeitlimit festgelegt werden, nach Ablauf dessen der Zyklus wieder am Anfang beginnt. Jede Gruppe, für die keine Tastenkombination festgelegt sind, wird beim Anwenden aller Änderungen gelöscht.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="215"/>
      <source>Add new shortcuts group</source>
      <translation>Neue Gruppe hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="235"/>
      <source>The shortcuts can be filtered by either the key combinations, shortcut actions, category, or all three. For the string search, PhotoQt will by default check if any action/key combination includes whatever string is entered. Adding a dollar sign ($) at the start or end of the search term forces a match to be either at the start or the end of a key combination or action.</source>
      <translation>Die Tastenkürzel können entweder nach Tastenkombinationen, Aktionen, oder nach Kategorie gefiltert werden. Bei der Suche nach Zeichenketten prüft PhotoQt, ob jede Aktion/Tastenkombination die eingegebene Zeichenkette enthält. Das Hinzufügen eines Dollar-Zeichens ($) am Anfang oder Ende des Suchbegriffs erzwingt eine Übereinstimmung entweder am Anfang oder am Ende einer Tastenkombination oder Aktion.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="247"/>
      <source>Filter key combinations</source>
      <translation>Tastenkombinationen filtern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="257"/>
      <source>Filter shortcut actions</source>
      <translation>Aktionen filtern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="267"/>
      <source>Show all categories</source>
      <translation>Alle Kategorien anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="268"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="269"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="270"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="271"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="272"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="273"/>
      <source>Category:</source>
      <translation>Kategorie:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="371"/>
      <source>no key combination set</source>
      <translation>keine Tastenkombination gesetzt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="457"/>
      <source>Click to change key combination</source>
      <translation>Zum Ändern der Tastenkombination klicken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="468"/>
      <source>Click to delete key combination</source>
      <translation>Zum Löschen der Tastenkombination klicken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="502"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="780"/>
      <source>ADD</source>
      <extracomment>Written on small button, used as in: add new key combination. Please keep short!
----------
Written on small button, used as in: add new shortcut action. Please keep short!</extracomment>
      <translation>NEU</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="514"/>
      <source>Click to add new key combination</source>
      <translation>Klicken, um neue Tastenkombination hinzuzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="570"/>
      <source>The new shortcut was in use by another shortcuts group. It has been reassigned to this group.</source>
      <translation>Der neue Kurzbefehl wurde von einer anderen Gruppe verwendet. Er wurde dieser Gruppe neu zugeordnet.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="581"/>
      <source>Undo reassignment</source>
      <translation>Neuzuordnung rückgängig machen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="656"/>
      <source>no action selected</source>
      <extracomment>The action here is a shortcut action</extracomment>
      <translation>keine Aktion ausgewählt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="704"/>
      <source>unknown:</source>
      <extracomment>The unknown here refers to an unknown internal action that was set as shortcut</extracomment>
      <translation>unbekannt:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="706"/>
      <source>external</source>
      <extracomment>This is an identifier in the shortcuts settings used to identify an external shortcut.</extracomment>
      <translation>extern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="709"/>
      <source>quit after</source>
      <extracomment>This is used for listing external commands for shortcuts, showing if the quit after checkbox has been checked</extracomment>
      <translation>danach beenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="719"/>
      <source>Click to change shortcut action</source>
      <translation>Zum Ändern der Verknüpfungsaktion klicken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="745"/>
      <source>Click to delete shortcut action</source>
      <translation>Zum Löschen der Verknüpfungsaktion klicken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="792"/>
      <source>Click to add new action</source>
      <extracomment>The action here is a shortcut action</extracomment>
      <translation>Zum Hinzufügen einer neuen Aktion klicken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="841"/>
      <source>cycle through actions one by one</source>
      <extracomment>The actions here are shortcut actions</extracomment>
      <translation>Aktionen einzeln nacheinander ausführen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="856"/>
      <source>timeout for resetting cycle:</source>
      <extracomment>The cycle here is the act of cycling through shortcut actions one by one</extracomment>
      <translation>Timeout für das Zurücksetzen des Zyklus:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="896"/>
      <source>run all actions at once</source>
      <extracomment>The actions here are shortcut actions</extracomment>
      <translation>alle Aktionen gleichzeitig ausführen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="232"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="41"/>
      <source>Spacing</source>
      <extracomment>Settings title</extracomment>
      <translation>Abstand</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="47"/>
      <source>PhotoQt preloads thumbnails for all files in the current folder and lines them up side by side. In between each thumbnail image it is possible to add a little bit of blank space to better separate the individual images.</source>
      <translation>PhotoQt lädt Miniaturbilder für alle Dateien im aktuellen Ordner im Voraus und ordnet sie nebeneinander an. Es ist möglich etwas Abstand zwischen den Miniaturbildern hinzuzufügen, und damit die einzelnen Bilder besser voneinander zu trennen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="233"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="85"/>
      <source>Highlight</source>
      <extracomment>Settings title</extracomment>
      <translation>Highlight</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="91"/>
      <source>The thumbnail corresponding to the currently loaded image is highlighted so that it is easy to spot. The same highlight effect is used when hovering over a thumbnail image. The different effects can be combined as desired.</source>
      <translation>Das Miniaturbild, das dem gerade geladenen Bild entspricht, wird hervorgehoben, damit es leicht erkennbar ist. Der gleiche Effekt zum Hervorheben wird verwendet, wenn der Mauszeiger sich über einem Miniaturbild befindet. Die verschiedenen Effekte können nach Belieben kombiniert werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="102"/>
      <source>invert background color</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>Hintergrundfarbe invertieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="110"/>
      <source>invert label color</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>Hintergrundfarbe der Beschriftung invertieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="118"/>
      <source>line below</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>Linie darunter</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="126"/>
      <source>magnify</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>vergrößern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="135"/>
      <source>lift up</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>anheben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="224"/>
      <source>hide when not needed</source>
      <extracomment>used as in: hide thumbnail bar when not needed</extracomment>
      <translation>verstecken wenn nicht benötigt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="234"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="182"/>
      <source>Center on active</source>
      <extracomment>Settings title</extracomment>
      <translation>Auf aktiven zentrieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="188"/>
      <source>When switching between images PhotoQt always makes sure that the thumbnail corresponding to the currently viewed image is visible somewhere along the thumbnail bar. Additionally it is possible to tell PhotoQt to not only keep it visible but also keep it in the center of the edge.</source>
      <translation>Beim Wechseln zwischen Bildern stellt PhotoQt sicher, dass das Miniaturbild, das dem gerade angezeigten Bild entspricht, immer sichtbar ist. Darüber hinaus ist es möglich, das Miniaturbild nicht nur sichtbar, sondern auch in der Mitte zu halten.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="195"/>
      <source>keep active thumbnail in center</source>
      <translation>aktives Miniaturbild in der Mitte halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="213"/>
      <source>The visibility of the thumbnail bar can be set depending on personal choice. The bar can either always be kept visible, it can be hidden unless the mouse cursor is close to the respective screen edge, or it can be kept visible unless the main image has been zoomed in.</source>
      <translation>Die Sichtbarkeit der Leiste mit den Miniaturbildern kann je nach persönlicher Vorliebe eingestellt werden. Die Leiste kann entweder immer sichtbar gehalten werden, sie kann ausgeblendet werden, es sei denn, der Mauszeiger ist in der Nähe des jeweiligen Bildschirmrandes, oder sie kann sichtbar gehalten werden, es sei denn, das Hauptbild wurde vergrößert.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="232"/>
      <source>always keep visible</source>
      <extracomment>used as in: always keep thumbnail bar visible</extracomment>
      <translation>immer sichtbar halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="240"/>
      <source>hide when zoomed in</source>
      <extracomment>used as in: hide thumbnail bar when zoomed in</extracomment>
      <translation>verstecken wenn gezoomt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="216"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="44"/>
      <source>Size</source>
      <extracomment>Settings title</extracomment>
      <translation>Größe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="50"/>
      <source>The thumbnails are typically hidden behind one of the screen edges. Which screen edge can be specified in the interface settings. The size of the thumbnails refers to the maximum size of each individual thumbnail image.</source>
      <translation>Die Miniaturbilder sind in der Regel hinter einem der Fensterkanten versteckt, welche, kann in den Einstellungen zur Kante festgelegt werden. Die Größe der Miniaturbilder bezieht sich auf die maximale Größe jedes einzelnen Miniaturbilds.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="217"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="88"/>
      <source>Scale and crop</source>
      <extracomment>Settings title</extracomment>
      <translation>Skalieren und zuschneiden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="94"/>
      <source>The thumbnail for an image can either be scaled to fit fully inside the maximum size specified above, or it can be scaled and cropped such that it takes up all available space. In the latter case some parts of a thumbnail image might be cut off. In addition, thumbnails that are smaller than the size specified above can be kept at their original size.</source>
      <translation>Das Miniaturbild für ein Bild kann entweder so skaliert werden, dass es vollständig in die oben angegebene maximale Größe passt, oder es kann skaliert und beschnitten werden, sodass es den gesamten verfügbaren Platz einnimmt. In letzterem Fall können einige Teile des Miniaturbilds abgeschnitten sein. Darüber hinaus können Miniaturbilder, die kleiner sind als die oben angegebene Größe, in ihrer Originalgröße belassen werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="105"/>
      <source>fit thumbnail</source>
      <translation>Miniaturbild einpassen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="111"/>
      <source>scale and crop thumbnail</source>
      <translation>Miniaturbild skalieren und zuschneiden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="123"/>
      <source>keep small thumbnails small</source>
      <translation>kleine Miniaturbilder klein halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="181"/>
      <source>On top of each thumbnail image PhotoQt can put a small text label with the filename. The font size of the filename is freely adjustable. If a filename is too long for the available space only the beginning and end of the filename will be visible. Additionally, the label of thumbnails that are neither loaded nor hovered can be shown with less opacity.</source>
      <translation>Auf jedes Miniaturbild kann PhotoQt einen kleinen Textlabel mit dem Dateinamen legen. Die Schriftgröße des Dateinamens ist frei einstellbar. Wenn ein Dateiname zu lang für den verfügbaren Platz ist, werden nur der Anfang und das Ende des Dateinamens sichtbar sein. Darüber hinaus kann das Label von Miniaturbildern, die weder aktiv noch hervorgehoben sind, mit geringerer Deckkraft angezeigt werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="225"/>
      <source>decrease opacity for inactive thumbnails</source>
      <translation>verringerte Deckkraft für inaktive Miniaturbilder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="218"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="137"/>
      <source>Icons only</source>
      <extracomment>Settings title</extracomment>
      <translation>Nur Icons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="143"/>
      <source>Instead of loading actual thumbnail images in the background, PhotoQt can instead simply show the respective icon for the filetype. This requires much fewer resources and time but is not as user friendly.</source>
      <translation>Anstelle tatsächliche Miniaturbilder im Hintergrund zu laden, kann PhotoQt stattdessen einfach das entsprechende Symbol für den Dateityp anzeigen. Dies erfordert deutlich weniger Ressourcen und Zeit, ist jedoch nicht so benutzerfreundlich.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="155"/>
      <source>use actual thumbnail images</source>
      <extracomment>The word actual is used with the same meaning as: real</extracomment>
      <translation>echte Miniaturbilder verwenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="161"/>
      <source>use filetype icons</source>
      <translation>Symbole für den Dateityp benutzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="219"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="175"/>
      <source>Label</source>
      <extracomment>Settings title</extracomment>
      <translation>Beschriftung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="188"/>
      <source>show filename label</source>
      <translation>Beschriftung mit Dateinamen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="220"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="236"/>
      <source>Tooltip</source>
      <extracomment>Settings title</extracomment>
      <translation>Tooltip</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="242"/>
      <source>PhotoQt can show additional information about an image in the form of a tooltip that is shown when the mouse cursor hovers above a thumbnail. The displayed information includes the full file name, file size, and file type.</source>
      <translation>PhotoQt kann zusätzliche Informationen über ein Bild in Form eines Tooltips anzeigen, welchen angezeigt wird, wenn der Mauszeiger sich über einem Miniaturbild befindet. Die angezeigten Informationen umfassen den vollständigen Dateinamen, die Dateigröße und den Dateityp.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="249"/>
      <source>show tooltips</source>
      <translation>zeige Tooltips</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="61"/>
      <source>PhotoQt can cache thumbnails so that each subsequent time they can be generated near instantaneously. PhotoQt implements the standard for thumbnails defined by freedesktop.org. On Windows it can also load (but not write) existing thumbnails from the thumbnail cache built into Windows.</source>
      <translation>PhotoQt kann Miniaturbilder zwischenspeichern, so dass sie beim nächsten Mal fast sofort generiert werden können. Unter Windows kann es auch existierende Miniaturbilder aus dem systemweiten Cache lesen (aber nicht schreiben).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="68"/>
      <source>enable cache</source>
      <translation>Zwischenspeicher aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="245"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="80"/>
      <source>Exclude folders</source>
      <extracomment>Settings title</extracomment>
      <translation>Ausgeschlossene Verzeichnisse</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="44"/>
      <source>PhotoQt shows all images in the currently loaded folder as thumbnails along one of the screen edges. If you want to disable thumbnails altogether, you can do so by removing it from all screen edges in the interface settings.</source>
      <translation>PhotoQt zeigt alle Bilder in dem aktuell geladenen Ordner als Miniaturbilder entlang einer der Fensterkanten an. Wenn die Miniaturbilder komplett deaktivieren werden sollen, kann dies durch das Entfernen der Miniaturbilder von allen Fensterkanten in den Einstellungen der Benutzeroberfläche getan werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="86"/>
      <source>When an image is loaded PhotoQt preloads thumbnails for all images found in the current folder. Some cloud providers do not fully sync their files unless accessed. To avoid unnecessarily downloading large amount of files, it is possible to exclude specific directories from any sort of caching and preloading. Note that for files in these folders you will still see thumbnails consisting of filetype icons.</source>
      <translation>Wenn ein Bild geladen wird, lädt PhotoQt Miniaturbilder für alle Bilder im aktuellen Ordner im Voraus. Einige Cloud-Anbieter synchronisieren ihre Dateien nicht vollständig, es sei denn, sie werden aufgerufen. Um das unnötige Herunterladen einer großen Anzahl von Dateien zu vermeiden, ist es möglich, bestimmte Verzeichnisse von jeglicher Art des Cachens und Vorausladens auszuschließen. Beachten Sie jedoch, dass für Dateien in diesen Ordnern weiterhin Miniaturbilder bestehend aus Symbolen für den Dateityp angezeigt werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="97"/>
      <source>Cloud providers to exclude from caching:</source>
      <translation>Cloud-Provider, die vom Cache ausgeschlossen werden sollen:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="141"/>
      <source>Do not cache these folders:</source>
      <translation>Diese Ordner nicht zwischenspeichern:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="149"/>
      <source>One folder per line</source>
      <translation>Ein Ordner pro Zeile</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="161"/>
      <source>Add folder</source>
      <extracomment>Written on a button</extracomment>
      <translation>Ordner hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="246"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="187"/>
      <source>How many threads</source>
      <extracomment>Settings title</extracomment>
      <translation>Wie viele Threads</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="193"/>
      <source>In order to speed up loading all the thumbnails in a folder PhotoQt uses multiple threads simultaneously. On more powerful systems, a larger number of threads can result in much faster loading of all the thumbnails of a folder. Too many threads, however, might make a system feel slow for a short time.</source>
      <translation>Um das Laden aller Miniaturbilder in einem Ordner zu beschleunigen, verwendet PhotoQt mehrere Threads gleichzeitig. Auf leistungsfähigeren Systemen kann eine größere Anzahl von Threads zu deutlich schnellerem Laden aller Miniaturbilder in einem Ordner führen. Zu viele Threads können jedoch dazu führen, dass ein System sich vorübergehend langsam anfühlt.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="222"/>
      <source>current value: %1 thread(s)</source>
      <extracomment>Important: Please do not forget the placeholder!</extracomment>
      <translation>aktueller Wert: %1 Thread(s)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSubCategory.qml" line="20"/>
      <source>subcategory</source>
      <translation>Unterkategorie</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSubCategory.qml" line="177"/>
      <source>Settings:</source>
      <translation>Einstellungen:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQMainCategory.qml" line="17"/>
      <source>main category</source>
      <translation>Hauptkategorie</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="38"/>
      <source>Export/Import configuration</source>
      <extracomment>Settings title</extracomment>
      <translation>Konfiguration ex-/importieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="44"/>
      <source>Here you can create a backup of the configuration for backup or for moving it to another install of PhotoQt. You can import a local backup below. After importing a backup file PhotoQt will automatically close as it will need to be restarted for the changes to take effect.</source>
      <translation>Hier kann eine Sicherung der Konfiguration erstellt werden für ein Backup oder um sie auf eine andere Installation von PhotoQt zu übertragen. Unten kann ebenfalls eine lokales Backup importiert werden. Nach dem Importieren eines Backups wird PhotoQt automatisch geschlossen, da es neu gestartet werden muss, damit die Änderungen wirksam werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="56"/>
      <source>export configuration</source>
      <translation>Konfiguration exportieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="69"/>
      <source>import configuration</source>
      <translation>Konfiguration importieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="72"/>
      <source>Restart required</source>
      <translation>Neustart erforderlich</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="73"/>
      <source>PhotoQt will now quit as it needs to be restarted for the changes to take effect.</source>
      <translation>PhotoQt wird nun beendet, da es neu gestartet werden muss, um die Änderungen wirksam zu machen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="38"/>
      <source>Reset settings and shortcuts</source>
      <extracomment>Settings title</extracomment>
      <translation>Einstellungen und Kurzbefehle zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="44"/>
      <source>Here the various configurations of PhotoQt can be reset to their defaults. Once you select one of the below categories you have a total of 5 seconds to cancel the action. After the 5 seconds are up the respective defaults will be set. This cannot be undone.</source>
      <translation>Hier können die verschiedenen Konfigurationsbereiche von PhotoQt auf ihre Standardwerte zurückgesetzt werden. Sobald du eine der untenstehenden Kategorien auswählst, hast du insgesamt 5 Sekunden Zeit, um die Aktion abzubrechen. Nach Ablauf der 5 Sekunden werden die entsprechenden Standardwerte festgelegt. Dies kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="56"/>
      <source>reset settings</source>
      <translation>Einstellungen zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="73"/>
      <source>reset shortcuts</source>
      <translation>Kurzbefehle zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="90"/>
      <source>reset enabled file formats</source>
      <translation>aktivierte Dateiformate zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="123"/>
      <source>You can still cancel this action. Seconds remaining:</source>
      <translation>Du kannst diese Aktion noch abbrechen. Sekunden verbleibend:</translation>
    </message>
  </context>
  <context>
    <name>slideshow</name>
    <message>
      <location filename="../qml/actions/popout/PQSlideshowSetupPopout.qml" line="32"/>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="38"/>
      <source>Slideshow setup</source>
      <extracomment>Window title</extracomment>
      <translation>Einstellungen für die Diaschau</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="41"/>
      <source>Start slideshow</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Diaschau starten</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="70"/>
      <source>interval</source>
      <extracomment>The interval between images in a slideshow</extracomment>
      <translation>Zeitintervall</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="106"/>
      <source>animation</source>
      <extracomment>This is referring to the in/out animation of images during a slideshow</extracomment>
      <translation>Animation</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="118"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>Deckkraft</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="120"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>entlang der x-Achse</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="122"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>entlang der y-Achse</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="124"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Drehung</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="126"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Explosion</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="128"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Implosion</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="130"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>zufällig auswählen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="149"/>
      <source>animation speed</source>
      <extracomment>The speed of transitioning from one image to another during slideshows</extracomment>
      <translation>Animationsgeschwindigkeit</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="171"/>
      <source>immediately, without animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>sofort, ohne Animation</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="174"/>
      <source>pretty fast animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>ziemlich schnelle Animation</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="177"/>
      <source>not too fast and not too slow</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>nicht zu schnell und nicht zu langsam</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="180"/>
      <source>very slow animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>sehr langsame Animation</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="189"/>
      <source>current speed</source>
      <extracomment>This refers to the currently set speed of transitioning from one image to another during slideshows</extracomment>
      <translation>aktuelle Geschwindigkeit</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="206"/>
      <source>looping</source>
      <translation>Wiederholen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="219"/>
      <source>loop over all files</source>
      <extracomment>Loop over all images during slideshows</extracomment>
      <translation>alle Dateien in Dauerschleife durchlaufen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="235"/>
      <source>shuffle</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>Zufallswiedergabe</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="248"/>
      <source>shuffle all files</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>alle Dateien mischen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="264"/>
      <source>subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>Unterordner</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="277"/>
      <source>include images in subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>Bilder aus den Unterordnern mit einbeziehen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="294"/>
      <source>status info</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation>Statusinfo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="307"/>
      <source>hide status info during slideshow</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation>Statusinformationen während der Diaschau ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="324"/>
      <source>window buttons</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation>Fensterknöpfe</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="337"/>
      <source>hide window buttons during slideshow</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation>Fensterknöpfe während der Slideshow ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="354"/>
      <source>music</source>
      <extracomment>The music that is to be played during slideshows</extracomment>
      <translation>Musik</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="371"/>
      <source>enable music</source>
      <extracomment>Enable music to be played during slideshows</extracomment>
      <translation>Musik aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="381"/>
      <source>no file selected</source>
      <translation>keine Datei ausgewählt</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="383"/>
      <source>Click to select music file</source>
      <translation>Klicken, um Musikdatei auszuwählen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="384"/>
      <source>Click to change music file</source>
      <translation>Klicken, um Musikdatei zu ändern</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQSlideshowControlsPopout.qml" line="33"/>
      <source>Slideshow</source>
      <extracomment>Window title</extracomment>
      <translation>Diaschau</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="145"/>
      <source>Click to go to the previous image</source>
      <translation>Klicken, um zum vorherigen Bild zu gehen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="179"/>
      <source>Click to pause slideshow</source>
      <translation>Klicken, um die Diaschau zu pausieren</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="180"/>
      <source>Click to play slideshow</source>
      <translation>Klicken, um die Diaschau zu starten</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="215"/>
      <source>Click to go to the next image</source>
      <translation>Klicke, um zum nächsten Bild zu gehen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="247"/>
      <source>Click to exit slideshow</source>
      <translation>Klicken, um die Diaschau zu beenden</translation>
    </message>
  </context>
  <context>
    <name>startup</name>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="138"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="140"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="142"/>
      <source>Edit with %1</source>
      <extracomment>Used as in &apos;Edit with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Mit %1 bearbeiten</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="144"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="146"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="148"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="150"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="152"/>
      <source>Open in %1</source>
      <extracomment>Used as in &apos;Open with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Mit %1 öffnen</translation>
    </message>
  </context>
  <context>
    <name>statusinfo</name>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="378"/>
      <source>Click anywhere to open a file</source>
      <translation>Zum Öffnen einer Datei irgendwo klicken</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="146"/>
      <source>Click and drag to move window around</source>
      <translation>Klicken und ziehen um das Fenster zu verschieben</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="147"/>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="191"/>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="317"/>
      <source>Click and drag to move status info around</source>
      <translation>Klicken und ziehen um Statusinformationen zu verschieben</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="218"/>
      <source>Click to remove filter</source>
      <translation>Klicken, um den Filter zu entfernen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="282"/>
      <source>Filter:</source>
      <extracomment>This refers to the currently set filter</extracomment>
      <translation>Filter:</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="369"/>
      <source>Connected to:</source>
      <extracomment>Used in tooltip for the chromecast icon</extracomment>
      <translation>Verbunden mit:</translation>
    </message>
  </context>
  <context>
    <name>streaming</name>
    <message>
      <location filename="../qml/actions/popout/PQChromeCastManagerPopout.qml" line="32"/>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="38"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>Window title</extracomment>
      <translation>Streaming (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="44"/>
      <source>Connect</source>
      <extracomment>Used as in: Connect to chromecast device</extracomment>
      <translation>Verbinden</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="55"/>
      <source>Disconnect</source>
      <extracomment>Used as in: Disconnect from chromecast device</extracomment>
      <translation>Trennen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="123"/>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="146"/>
      <source>No devices found</source>
      <extracomment>The devices here are chromecast devices</extracomment>
      <translation>Keine Geräte gefunden</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="139"/>
      <source>Status:</source>
      <extracomment>The status refers to whether the chromecast manager is currently scanning or idle</extracomment>
      <translation>Status:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="143"/>
      <source>Looking for Chromecast devices</source>
      <translation>Suche nach Chromecast-Geräten</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="145"/>
      <source>Select which device to connect to</source>
      <translation>Gerät zum Verbinden auswählen</translation>
    </message>
  </context>
  <context>
    <name>thumbnails</name>
    <message>
      <location filename="../qml/ongoing/PQThumbnails.qml" line="436"/>
      <source>File size:</source>
      <translation>Dateigröße:</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQThumbnails.qml" line="437"/>
      <source>File type:</source>
      <translation>Dateityp:</translation>
    </message>
  </context>
  <context>
    <name>unavailable</name>
    <message>
      <location filename="../qml/manage/PQLoader.qml" line="53"/>
      <source>The chromecast feature is not available in this build of PhotoQt.</source>
      <translation>Die Chromecast-Funktion ist in dieser Version von PhotoQt nicht verfügbar.</translation>
    </message>
    <message>
      <location filename="../qml/manage/PQLoader.qml" line="56"/>
      <source>The location feature is not available in this build of PhotoQt.</source>
      <translation>Die Standortfunktion ist in dieser Version von PhotoQt nicht verfügbar.</translation>
    </message>
  </context>
  <context>
    <name>wallpaper</name>
    <message>
      <location filename="../qml/actions/popout/PQWallpaperPopout.qml" line="32"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="41"/>
      <source>Wallpaper</source>
      <extracomment>Window title</extracomment>
      <translation>Hintergrundbild</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQWallpaper.qml" line="46"/>
      <source>Set as Wallpaper</source>
      <translation>Als Hintergrund setzen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQWallpaper.qml" line="97"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="113"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="129"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="145"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="161"/>
      <source>Click to choose %1</source>
      <extracomment>%1 is a placeholder for the name of a desktop environment (plasma, xfce, gnome, etc.)</extracomment>
      <translation>Klicken, um %1 auszuwählen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQWallpaper.qml" line="163"/>
      <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="57"/>
      <source>Other</source>
      <extracomment>Used as in: Other Desktop Environment</extracomment>
      <translation>Andere</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="70"/>
      <source>Warning: %1 module not activated</source>
      <translation>Warnung: Modul %1 nicht aktiviert</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="78"/>
      <location filename="../qml/actions/wallpaperparts/PQGnome.qml" line="68"/>
      <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="71"/>
      <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="79"/>
      <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="68"/>
      <source>Warning: %1 not found</source>
      <translation>Warnung: %1 nicht gefunden</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="98"/>
      <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="88"/>
      <source>Set to which screens</source>
      <extracomment>As in: Set wallpaper to which screens</extracomment>
      <translation>Für welche Bildschirme setzen</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="111"/>
      <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="100"/>
      <source>Screen</source>
      <extracomment>Used in wallpaper element</extracomment>
      <translation>Bildschirm</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="134"/>
      <source>Set to which workspaces</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>Für welche Arbeitsflächen setzen</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="148"/>
      <source>Workspace:</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>Arbeitsfläche:</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQGnome.qml" line="80"/>
      <location filename="../qml/actions/wallpaperparts/PQWindows.qml" line="62"/>
      <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="123"/>
      <source>Choose picture option</source>
      <extracomment>picture option refers to how to format a pictrue when setting it as wallpaper</extracomment>
      <translation>Bildoption auswählen</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="97"/>
      <source>Tool:</source>
      <extracomment>Tool refers to a program that can be executed</extracomment>
      <translation>Werkzeug:</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQPlasma.qml" line="65"/>
      <source>The image will be set to all screens at the same time.</source>
      <translation>Das Bild wird auf alle Bildschirme gleichzeitig gesetzt.</translation>
    </message>
  </context>
</TS>
