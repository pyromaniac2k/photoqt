<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es-ES" sourcelanguage="en">
  <context>
    <name>MainMenu</name>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="168"/>
      <source>navigation</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>navegación</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="187"/>
      <source>previous</source>
      <extracomment>as in: PREVIOUS image. Please keep short.</extracomment>
      <translation>anterior</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="199"/>
      <source>next</source>
      <extracomment>as in: NEXT image. Please keep short.</extracomment>
      <translation>siguiente</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="214"/>
      <source>first</source>
      <extracomment>as in: FIRST image. Please keep short.</extracomment>
      <translation>primero</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="223"/>
      <source>last</source>
      <extracomment>as in: LAST image. Please keep short.</extracomment>
      <translation>último</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="233"/>
      <source>Browse images</source>
      <translation>Navegar imágenes</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="242"/>
      <source>Map Explorer</source>
      <translation>Explorador de Mapa</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="264"/>
      <source>current image</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>imagen actual</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="291"/>
      <source>Zoom</source>
      <extracomment>Entry in main menu. Please keep short.</extracomment>
      <translation>Zum</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="326"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="374"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="422"/>
      <source>reset</source>
      <extracomment>Used as in RESET zoom.
----------
Used as in RESET rotation.
----------
Used as in RESET mirroring.</extracomment>
      <translation>restablecer</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="348"/>
      <source>Rotation</source>
      <extracomment>Entry in main menu. Please keep short.</extracomment>
      <translation>Rotación</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="396"/>
      <source>Mirror</source>
      <extracomment>Mirroring (or flipping) an image. Please keep short.</extracomment>
      <translation>Espejo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="434"/>
      <source>Hide histogram</source>
      <translation>Ocultar histograma</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="434"/>
      <source>Show histogram</source>
      <translation>Mostrar histograma</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="440"/>
      <source>Hide current location</source>
      <translation>Ocultar ubicación actual</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="440"/>
      <source>Show current location</source>
      <translation>Mostrar ubicación actual</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="463"/>
      <source>all images</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>todas las imágenes</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="490"/>
      <source>Slideshow</source>
      <extracomment>Entry in main menu. Please keep short.</extracomment>
      <translation>Diapositiva</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="500"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="543"/>
      <source>Start</source>
      <extracomment>Used as in START slideshow. Please keep short
----------
Used as in START advanced sort. Please keep short</extracomment>
      <translation>Iniciar</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="510"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="553"/>
      <source>Setup</source>
      <extracomment>Used as in SETUP slideshow. Please keep short
----------
Used as in SETUP advanced sort. Please keep short</extracomment>
      <translation>Configurar</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="533"/>
      <source>Sort</source>
      <extracomment>Entry in main menu. Please keep short.</extracomment>
      <translation>Ordenar</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="566"/>
      <source>Filter images</source>
      <translation>Filtrar imágenes</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="575"/>
      <source>Streaming (Chromecast)</source>
      <translation>Transmitiendo (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="583"/>
      <source>Open in default file manager</source>
      <translation>Abrir en el gestor de archivos predeterminado</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="606"/>
      <source>general</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>general</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="623"/>
      <source>Settings</source>
      <translation>Preferencias</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="631"/>
      <source>About</source>
      <translation>Acerca de</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="643"/>
      <source>Online help</source>
      <translation>Ayuda en línea</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="651"/>
      <source>Quit</source>
      <translation>Salir</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="677"/>
      <source>custom</source>
      <extracomment>This is a category in the main menu.</extracomment>
      <translation>personalizado</translation>
    </message>
  </context>
  <context>
    <name>PQCImageFormats</name>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="66"/>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="79"/>
      <source>ERROR getting default image formats</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ERROR al obtener los formatos de imagen por omisión</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="67"/>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="80"/>
      <source>Not even a read-only version of the database of default image formats could be opened.</source>
      <translation>No se pudo abrir siquiera una versión de solo lectura de la base de datos de formatos de imagen predeterminados.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="67"/>
      <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="80"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>¡Algo salió terriblemente mal en algún lugar!</translation>
    </message>
  </context>
  <context>
    <name>PQCScriptsMetaData</name>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="64"/>
      <source>yes</source>
      <extracomment>This string identifies that flash was fired, stored in image metadata</extracomment>
      <translation>sí</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="66"/>
      <source>no</source>
      <extracomment>This string identifies that flash was not fired, stored in image metadata</extracomment>
      <translation>no</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="68"/>
      <source>No flash function</source>
      <extracomment>This string refers to the absense of a flash, stored in image metadata</extracomment>
      <translation>No hay función de flash</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="70"/>
      <source>strobe return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>El estroboscopio indicó que no se detectó luz</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="72"/>
      <source>strobe return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>El estroboscopio detectó luz</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="74"/>
      <source>compulsory flash mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>modo de flash obligatorio</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="76"/>
      <source>auto mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>modo automático</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="78"/>
      <source>red-eye reduction mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>modo de reducción de ojos rojos</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="80"/>
      <source>return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>luz de retorno detectada</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="82"/>
      <source>return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>luz de retorno no detectada</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="130"/>
      <source>Invalid flash</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Flash no válido</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="140"/>
      <source>Standard</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Estándar</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="143"/>
      <source>Landscape</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Apaisada</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="146"/>
      <source>Portrait</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Retrato</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="149"/>
      <source>Night Scene</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Escena nocturna</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="152"/>
      <source>Invalid Scene Type</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Tipo de escena no válido</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="202"/>
      <source>Unknown</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Desconocido</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="205"/>
      <source>Daylight</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Luz diurna</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="208"/>
      <source>Fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluorescente</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="211"/>
      <source>Tungsten (incandescent light)</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Tungsteno (luz incandescente)</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="214"/>
      <source>Flash</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Flash</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="217"/>
      <source>Fine weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Buen tiempo</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="220"/>
      <source>Cloudy Weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Tiempo nublado</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="223"/>
      <source>Shade</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Sombra</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="226"/>
      <source>Daylight fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluorescente de día</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="229"/>
      <source>Day white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluorescente blanca de día</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="232"/>
      <source>Cool white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluorescente blanca fría</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="235"/>
      <source>White fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluorescente blanca</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="238"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="241"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="244"/>
      <source>Standard light</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Luz estándar</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="257"/>
      <source>Other light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Otra fuente de luz</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="260"/>
      <source>Invalid light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>La fuente de luz no es válida</translation>
    </message>
  </context>
  <context>
    <name>PQCScriptsOther</name>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptsother.cpp" line="83"/>
      <source>Print</source>
      <translation>Imprimir</translation>
    </message>
  </context>
  <context>
    <name>PQCStartup</name>
    <message>
      <location filename="../cplusplus/other/pqc_startup.cpp" line="47"/>
      <source>SQLite error</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>Error de SQLite</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_startup.cpp" line="48"/>
      <source>You seem to be missing the SQLite driver for Qt. This is needed though for a few different things, like reading and writing the settings. Without it, PhotoQt cannot function!</source>
      <translation>Al parecer te hace falta el controlador de SQLite para Qt. Esto es necesario para algunas cuantas cosas, como lo es leer y escribir las preferencias. ¡Sin esto, PhotoQt no podrá funcionar!</translation>
    </message>
  </context>
  <context>
    <name>PQSettings</name>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="72"/>
      <source>ERROR getting database with default settings</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ERROR al obtener la base de datos con las preferencias predeterminadas</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="73"/>
      <source>I tried hard, but I just cannot open even a read-only version of the settings database.</source>
      <translation>Lo intenté mucho, pero simplemente no puedo abrir ni siquiera una versión de solo lectura de la base de datos de preferencias.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="73"/>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="85"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>¡Algo fue terriblemente mal en algún lado!</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="84"/>
      <source>ERROR opening database with default settings</source>
      <translation>ERROR al abrir la base de datos con las preferencias predeterminadas</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="85"/>
      <source>I tried hard, but I just cannot open the database of default settings.</source>
      <translation>Lo intenté mucho, pero simplemente no puedo abrir la base de datos de preferencias predeterminadas.</translation>
    </message>
  </context>
  <context>
    <name>PQShortcuts</name>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="61"/>
      <source>ERROR getting database with default shortcuts</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ERROR al obtener la base de datos con los atajos predeterminados</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="62"/>
      <source>I tried hard, but I just cannot open even a read-only version of the shortcuts database.</source>
      <translation>Lo intenté mucho, pero simplemente no puedo abrir ni siquiera una versión de solo lectura de la base de datos de atajos.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="62"/>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="74"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>¡Algo salió terriblemente mal en algún lugar!</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="73"/>
      <source>ERROR opening database with default settings</source>
      <translation>ERROR al abrir la base de datos con las preferencias predeterminadas</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="74"/>
      <source>I tried hard, but I just cannot open the database of default shortcuts.</source>
      <translation>Lo intenté mucho, pero simplemente no puedo abrir la base de datos de atajos predeterminados.</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="50"/>
      <source>Alt</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Alt</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="52"/>
      <source>Ctrl</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Ctrl</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="54"/>
      <source>Shift</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Mayús</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="56"/>
      <source>Page Up</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Re Pág</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="58"/>
      <source>Page Down</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Av Pág</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="60"/>
      <source>Meta</source>
      <extracomment>Refers to the key that usually has the Windows symbol on it</extracomment>
      <translation>Meta</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="62"/>
      <source>Keypad</source>
      <extracomment>Refers to the key that triggers the number block on keyboards</extracomment>
      <translation>Teclado numérico</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="64"/>
      <source>Escape</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Esc</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="66"/>
      <source>Right</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Derecha</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="68"/>
      <source>Left</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Izquierda</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="70"/>
      <source>Up</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Arriba</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="72"/>
      <source>Down</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Abajo</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="74"/>
      <source>Space</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Espacio</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="76"/>
      <source>Delete</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Supr</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="78"/>
      <source>Backspace</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Retroceso</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="80"/>
      <source>Home</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Inicio</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="82"/>
      <source>End</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Fin</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="84"/>
      <source>Insert</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Ins</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="86"/>
      <source>Tab</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Tab</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="88"/>
      <source>Return</source>
      <extracomment>Return refers to the enter key of the number block - please try to make the translations of Return and Enter (the main button) different if possible!</extracomment>
      <translation>Intro</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="90"/>
      <source>Enter</source>
      <extracomment>Enter refers to the main enter key - please try to make the translations of Return (in the number block) and Enter different if possible!</extracomment>
      <translation>Entrar</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="95"/>
      <source>Left Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón izquierdo</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="97"/>
      <source>Right Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón derecho</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="99"/>
      <source>Middle Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón central</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="101"/>
      <source>Back Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón de retroceder</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="103"/>
      <source>Forward Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón de avanzar</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="105"/>
      <source>Task Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón de tareas</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="107"/>
      <source>Button #7</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón #7</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="109"/>
      <source>Button #8</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón #8</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="111"/>
      <source>Button #9</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón #9</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="113"/>
      <source>Button #10</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botón #10</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="115"/>
      <source>Double Click</source>
      <extracomment>Refers to a mouse event</extracomment>
      <translation>Doble clic</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="117"/>
      <source>Wheel Up</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Ruedecilla arriba</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="119"/>
      <source>Wheel Down</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Ruedecilla abajo</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="121"/>
      <source>Wheel Left</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Ruedecilla a la izquierda</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="123"/>
      <source>Wheel Right</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Ruedecilla a la derecha</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="125"/>
      <source>East</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Este</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="127"/>
      <source>South</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Sur</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="129"/>
      <source>West</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Oeste</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="131"/>
      <source>North</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Norte</translation>
    </message>
  </context>
  <context>
    <name>about</name>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="39"/>
      <source>About</source>
      <translation>Acerca de</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="73"/>
      <source>Show configuration overview</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Mostrar vista general de configuración</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="80"/>
      <source>License:</source>
      <translation>Licencia:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="85"/>
      <source>Open license in browser</source>
      <translation>Abrir licencia en el navegador</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="98"/>
      <source>Website:</source>
      <translation>Sitio Web:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="103"/>
      <source>Open website in browser</source>
      <translation>Abrir sitio web en el navegador</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="111"/>
      <source>Contact:</source>
      <translation>Contacto:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="116"/>
      <source>Send an email</source>
      <translation>Envíar un correo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="158"/>
      <source>Configuration</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Configuración</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAbout.qml" line="173"/>
      <source>Copy to clipboard</source>
      <translation>Copiar al portapapeles</translation>
    </message>
  </context>
  <context>
    <name>actions</name>
    <message>
      <location filename="../qml/actions/popout/PQExportPopout.qml" line="32"/>
      <source>Export image</source>
      <extracomment>Window title</extracomment>
      <translation>Exportar imagen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/popout/PQFileDialogPopout.qml" line="33"/>
      <source>File Dialog</source>
      <extracomment>Window title</extracomment>
      <translation>Cuadro de diálogo de archivos</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQMainMenuPopout.qml" line="33"/>
      <source>Main Menu</source>
      <extracomment>Window title</extracomment>
      <translation>Menú principal</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQMetaDataPopout.qml" line="33"/>
      <source>Metadata</source>
      <extracomment>Window title</extracomment>
      <translation>Metadatos</translation>
    </message>
    <message>
      <location filename="../qml/actions/popout/PQAboutPopout.qml" line="32"/>
      <source>About</source>
      <extracomment>Window title</extracomment>
      <translation>Acerca de</translation>
    </message>
  </context>
  <context>
    <name>advancedsort</name>
    <message>
      <location filename="../qml/actions/popout/PQAdvancedSortPopout.qml" line="32"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="40"/>
      <source>Advanced image sort</source>
      <extracomment>Window title</extracomment>
      <translation>Ordenamiento avanzado de imágenes</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="42"/>
      <source>Sort images</source>
      <translation>Ordenar imágenes</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="78"/>
      <source>Sorting criteria:</source>
      <translation>Criterios de ordenación:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="85"/>
      <source>Resolution</source>
      <extracomment>The image resolution (width/height in pixels)</extracomment>
      <translation>Resolución</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="87"/>
      <source>Dominant color</source>
      <extracomment>The color that is most common in the image</extracomment>
      <translation>Color dominante</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="89"/>
      <source>Average color</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Color promedio</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="91"/>
      <source>Luminosity</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Luminosidad</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="92"/>
      <source>Exif date</source>
      <translation>Fecha Exif</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="149"/>
      <source>Sort by image resolution</source>
      <translation>Ordenar por resolución de imagen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="166"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="233"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="330"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="424"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="519"/>
      <source>in ascending order</source>
      <extracomment>as is: sort in ascending order</extracomment>
      <translation>en orden ascendente</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="172"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="238"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="335"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="429"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="524"/>
      <source>in descending order</source>
      <extracomment>as is: sort in descending order</extracomment>
      <translation>en orden descendente</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="217"/>
      <source>Sort by dominant color</source>
      <translation>Ordenar por color dominante</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="254"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="350"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="444"/>
      <source>low quality (fast)</source>
      <extracomment>quality and speed of advanced sorting of images</extracomment>
      <translation>calidad baja (rápido)</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="256"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="351"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="445"/>
      <source>medium quality</source>
      <extracomment>quality and speed of advanced sorting of images</extracomment>
      <translation>Calidad media</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="258"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="352"/>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="446"/>
      <source>high quality (slow)</source>
      <extracomment>quality and speed of advanced sorting of images</extracomment>
      <translation>calidad alta (lento)</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="314"/>
      <source>Sort by average color</source>
      <translation>Ordenar por color promedio</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="408"/>
      <source>Sort by luminosity</source>
      <translation>Ordenar por luminosidad</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="503"/>
      <source>Sort by date</source>
      <translation>Ordenar por fecha</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="536"/>
      <source>Order of priority:</source>
      <translation>Orden de prioridad:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="549"/>
      <source>Exif tag: Original date/time</source>
      <translation>Etiqueta Exif: Fecha/hora original</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="550"/>
      <source>Exif tag: Digitized date/time</source>
      <translation>Etiqueta Exif: Fecha/hora de la digitalización</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="551"/>
      <source>File creation date</source>
      <translation>Fecha de creación del archivo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="552"/>
      <source>File modification date</source>
      <translation>Fecha de modificación del archivo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQAdvancedSort.qml" line="625"/>
      <source>If a value cannot be found, PhotoQt will proceed to the next item in the list.</source>
      <translation type="unfinished">If a value cannot be found, PhotoQt will proceed to the next item in the list.</translation>
    </message>
  </context>
  <context>
    <name>buttongeneric</name>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="55"/>
      <location filename="../qml/elements/PQButtonElement.qml" line="41"/>
      <source>Ok</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Aceptar</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="57"/>
      <location filename="../qml/elements/PQButtonElement.qml" line="43"/>
      <source>Cancel</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Cancelar</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="59"/>
      <location filename="../qml/elements/PQButtonElement.qml" line="45"/>
      <source>Save</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Guardar</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="61"/>
      <location filename="../qml/elements/PQButtonElement.qml" line="47"/>
      <source>Close</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Cerrar</translation>
    </message>
  </context>
  <context>
    <name>commandlineparser</name>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="36"/>
      <source>Image Viewer</source>
      <translation>Visor de imágenes</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="38"/>
      <source>Image file to open.</source>
      <translation>Archivo de imagen a abrir.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="45"/>
      <source>Make PhotoQt ask for a new file.</source>
      <translation>Hacer que PhotoQt solicite un nuevo archivo.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="47"/>
      <source>Shows PhotoQt from system tray.</source>
      <translation>Muestra PhotoQt desde la bandera del sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="49"/>
      <source>Hides PhotoQt to system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Oculta PhotoQt a la bandera del sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="51"/>
      <source>Quit PhotoQt.</source>
      <translation>Salir de PhotoQt.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="53"/>
      <source>Show/Hide PhotoQt.</source>
      <translation>Mostrar/Ocultar PhotoQt.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="55"/>
      <source>Enable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Habilitar icono de la bandeja del sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="57"/>
      <source>Disable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Deshabilitar icono de la bandeja del sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="59"/>
      <source>Start PhotoQt hidden to the system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Iniciar PhotoQt oculto en la bandeja del sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="61"/>
      <source>Simulate a shortcut sequence</source>
      <extracomment>Command line option</extracomment>
      <translation>Simular secuencia de atajo</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="63"/>
      <source>Change setting to specified value.</source>
      <extracomment>Command line option</extracomment>
      <translation>Cambiar configuración a un valor específico.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="65"/>
      <source>settingname:value</source>
      <extracomment>Command line option</extracomment>
      <translation>nombredeconfiguración:valor</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="67"/>
      <source>Switch on debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Activar los mensajes de depuración.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="69"/>
      <source>Switch off debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Desactivar los mensajes de depuración.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="71"/>
      <source>Export configuration to given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Exportar configuración a un archivo.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="73"/>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="77"/>
      <source>filename</source>
      <extracomment>Command line option</extracomment>
      <translation>nombre de archivo</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="75"/>
      <source>Import configuration from given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Importar configuración a partir de un archivo.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="79"/>
      <source>Check the configuration and correct any detected issues.</source>
      <extracomment>Command line option</extracomment>
      <translation>Revisar la configuración y corregir cualquier problema detectado.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="81"/>
      <source>Reset default configuration.</source>
      <extracomment>Command line option</extracomment>
      <translation>Restablecer a configuración por omisión.</translation>
    </message>
    <message>
      <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="83"/>
      <source>Show configuration overview.</source>
      <extracomment>Command line option</extracomment>
      <translation>Mostrar vista general de configuración.</translation>
    </message>
  </context>
  <context>
    <name>contextmenu</name>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="16"/>
      <source>Rename file</source>
      <translation>Cambiar nombre de archivo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="23"/>
      <source>Copy file</source>
      <translation>Copiar archivo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="30"/>
      <source>Move file</source>
      <translation>Mover archivo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="37"/>
      <source>Delete file</source>
      <translation>Borrar archivo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="46"/>
      <source>Copy to clipboard</source>
      <translation>Copiar al portapapeles</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="53"/>
      <source>Export to different format</source>
      <translation>Exportar a otro formato</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="60"/>
      <source>Scale image</source>
      <translation>Escalar imagen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="67"/>
      <source>Tag faces</source>
      <translation>Etiquetar rostros</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="74"/>
      <source>Set as wallpaper</source>
      <translation>Establecer como fondo de pantalla</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="83"/>
      <source>Show histogram</source>
      <translation>Mostrar histograma</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQContextMenu.qml" line="90"/>
      <source>Show on map</source>
      <translation>Mostrar en mapa</translation>
    </message>
  </context>
  <context>
    <name>export</name>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="22"/>
      <source>Export image</source>
      <extracomment>title of action element</extracomment>
      <translation>Exportar imagen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="25"/>
      <location filename="../qml/actions/PQExport.qml" line="32"/>
      <source>Export</source>
      <extracomment>written on button</extracomment>
      <translation>Exportar</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="67"/>
      <source>Something went wrong during export to the selected format...</source>
      <translation>Se produjo un problema al exportar al formato seleccionado...</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="98"/>
      <source>Favorites:</source>
      <extracomment>These are the favorite image formats for exporting images to</extracomment>
      <translation>Favoritos:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="111"/>
      <source>no favorites set</source>
      <extracomment>the favorites are image formats for exporting images to</extracomment>
      <translation>no se han establecido favoritos</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="194"/>
      <location filename="../qml/actions/PQExport.qml" line="338"/>
      <source>Click to select this image format</source>
      <translation>Clic para seleccionar este formato de imagen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="216"/>
      <location filename="../qml/actions/PQExport.qml" line="360"/>
      <source>Click to remove this image format from your favorites</source>
      <translation>Clic para eliminar este formato de imagen de tus favoritos</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="361"/>
      <source>Click to add this image format to your favorites</source>
      <translation type="unfinished">Click to add this image format to your favorites</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQExport.qml" line="387"/>
      <source>Selected target format:</source>
      <extracomment>The target format is the format the image is about to be exported to</extracomment>
      <translation type="unfinished">Selected target format:</translation>
    </message>
  </context>
  <context>
    <name>facetagging</name>
    <message>
      <location filename="../qml/image/PQFaceTagger.qml" line="49"/>
      <source>Click to exit face tagging mode</source>
      <translation>Pulse para salir del modo de etiquetado de rostros</translation>
    </message>
    <message>
      <location filename="../qml/image/PQFaceTagger.qml" line="205"/>
      <source>Who is this?</source>
      <translation>¿Quién es?</translation>
    </message>
  </context>
  <context>
    <name>filedialog</name>
    <message>
      <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="80"/>
      <source>Show files as icons</source>
      <translation>Mostrar archivos como iconos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="92"/>
      <source>Show files as list</source>
      <translation>Mostrar archivos como lista</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="235"/>
      <source>no subfolders found</source>
      <translation>no se encontró ninguna subcarpeta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="471"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="473"/>
      <source>%1 image</source>
      <translation>%1 imagen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="471"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="475"/>
      <source>%1 images</source>
      <translation>%1 imágenes</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="656"/>
      <source># images</source>
      <translation># imágenes</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="215"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="657"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="692"/>
      <source>Date:</source>
      <translation>Fecha:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="216"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="658"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="693"/>
      <source>Time:</source>
      <translation>Hora:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="217"/>
      <source>Location:</source>
      <translation>Ubicación:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="213"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="690"/>
      <source>File size:</source>
      <translation>Tamaño de archivo:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="214"/>
      <location filename="../qml/filedialog/PQFileView.qml" line="691"/>
      <source>File type:</source>
      <translation>Tipo de archivo:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="104"/>
      <source>no supported files/folders found</source>
      <translation>no se encontró ningún archivo o carpeta admitido</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1112"/>
      <source>Load this folder</source>
      <translation>Cargar esta carpeta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1112"/>
      <source>Load this file</source>
      <translation>Cargar este archivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1124"/>
      <source>Add to Favorites</source>
      <translation>Agregar a favoritos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1133"/>
      <source>Remove file selection</source>
      <translation>Eliminar selección de archivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1133"/>
      <source>Select file</source>
      <translation>Seleccionar archivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1144"/>
      <source>Remove all file selection</source>
      <translation>Eliminar selección de todos los archivos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1144"/>
      <source>Select all files</source>
      <translation>Seleccionar todos los archivos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1153"/>
      <source>Delete selection</source>
      <translation>Borrar selección</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1154"/>
      <source>Delete file</source>
      <translation>Borrar archivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1155"/>
      <source>Delete folder</source>
      <translation>Borrar carpeta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1156"/>
      <source>Delete file/folder</source>
      <translation>Borrar archivo/carpeta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1163"/>
      <source>Cut selection</source>
      <translation>Cortar selección</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1164"/>
      <source>Cut file</source>
      <translation>Cortar archivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1165"/>
      <source>Cut folder</source>
      <translation>Cortar carpeta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1166"/>
      <source>Cut file/folder</source>
      <translation>Cortar archivo/carpeta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1173"/>
      <source>Copy selection</source>
      <translation>Copiar selección</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1174"/>
      <source>Copy file</source>
      <translation>Copiar archivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1175"/>
      <source>Copy folder</source>
      <translation>Copiar carpeta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1176"/>
      <source>Copy file/folder</source>
      <translation>Copiar archivo/carpeta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1182"/>
      <source>Paste files from clipboard</source>
      <translation>Pegar archivos desde el portapapeles</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1202"/>
      <source>Show hidden files</source>
      <translation>Mostrar archivos ocultos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileView.qml" line="1209"/>
      <source>Show tooltip with image details</source>
      <translation>Mostrar descripción emergente con los detalles de la imagen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="67"/>
      <source>bookmarks and devices disabled</source>
      <translation type="unfinished">bookmarks and devices disabled</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="403"/>
      <source>Show entry</source>
      <translation>Mostrar entrada</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="403"/>
      <source>Hide entry</source>
      <translation>Ocultar entrada</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="422"/>
      <source>Remove entry</source>
      <translation>Eliminar entrada</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="441"/>
      <source>Hide hidden entries</source>
      <translation>No mostrar las entradas ocultas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="441"/>
      <source>Show hidden entries</source>
      <translation>Mostrar las entradas ocultas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="469"/>
      <source>Hide bookmarked places</source>
      <translation type="unfinished">Hide bookmarked places</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="469"/>
      <source>Show bookmarked places</source>
      <translation type="unfinished">Show bookmarked places</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="475"/>
      <source>Hide storage devices</source>
      <translation>Ocultar dispositivos de almacenamiento</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="475"/>
      <source>Show storage devices</source>
      <translation>Mostrar dispositivos de almacenamiento</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="498"/>
      <source>Storage Devices</source>
      <translation>Dispositivo de almacenamiento</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPlaces.qml" line="521"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="203"/>
      <source>Bookmarks</source>
      <extracomment>file manager settings popdown: menu title</extracomment>
      <translation>Marcadores</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="42"/>
      <source>Zoom:</source>
      <translation>Zum:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="46"/>
      <source>Adjust size of files and folders</source>
      <translation>Ajustar tamaño de archivos y carpetas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="116"/>
      <source>Cancel and close</source>
      <translation>Cancelar y cerrar</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="135"/>
      <source>Sort by:</source>
      <translation>Ordenar por:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="142"/>
      <source>Name</source>
      <translation>Nombre</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="143"/>
      <source>Natural Name</source>
      <translation>Nombre natural</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="144"/>
      <source>Time modified</source>
      <translation>Modificado</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="145"/>
      <source>File size</source>
      <translation>Tamaño de archivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="146"/>
      <source>File type</source>
      <translation>Tipo de archivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="147"/>
      <source>reverse order</source>
      <translation>invertir orden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="201"/>
      <source>All supported images</source>
      <translation>Todas las imágenes admitidas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="206"/>
      <source>Video files</source>
      <translation>Archivos de vídeo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQTweaks.qml" line="207"/>
      <source>All files</source>
      <translation>Todos los archivos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="18"/>
      <source>View</source>
      <extracomment>file manager settings popdown: menu title</extracomment>
      <translation>Vista</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="21"/>
      <source>layout</source>
      <extracomment>file manager settings popdown: submenu title</extracomment>
      <translation>disposición</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="23"/>
      <source>list view</source>
      <translation>vista de lista</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="31"/>
      <source>icon view</source>
      <translation>vista de iconos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="42"/>
      <source>padding</source>
      <extracomment>file manager settings popdown: submenu title</extracomment>
      <translation>relleno</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="60"/>
      <source>drag and drop</source>
      <extracomment>file manager settings popdown: submenu title</extracomment>
      <translation>arrastrar y soltar</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="63"/>
      <source>enable for list view</source>
      <extracomment>file manager settings popdown: the thing to enable here is drag-and-drop</extracomment>
      <translation>habilitar para vista de lista</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="71"/>
      <source>enable for grid view</source>
      <extracomment>file manager settings popdown: the thing to enable here is drag-and-drop</extracomment>
      <translation>habilitar para vista de cuadrícula</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="79"/>
      <source>enable for bookmarks</source>
      <extracomment>file manager settings popdown: the thing to enable here is drag-and-drop</extracomment>
      <translation>habilitar para marcadores</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="89"/>
      <source>select with single click</source>
      <extracomment>file manager settings popdown: how to select files</extracomment>
      <translation>seleccionar con un solo clic</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="96"/>
      <source>hidden files</source>
      <translation>archivos ocultos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="103"/>
      <source>tooltips</source>
      <translation>cuadros emergentes</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="112"/>
      <source>Thumbnails</source>
      <extracomment>file manager settings popdown: menu title</extracomment>
      <translation>Miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="116"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="137"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="226"/>
      <source>show</source>
      <extracomment>file manager settings popdown: show thumbnails
----------
file manager settings popdown: show folder thumbnails
----------
file manager settings popdown: show image previews</extracomment>
      <translation>mostrar</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="124"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="145"/>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="252"/>
      <source>scale and crop</source>
      <extracomment>file manager settings popdown: scale and crop the thumbnails
----------
file manager settings popdown: scale and crop the folder thumbnails
----------
file manager settings popdown: scale and crop image previews</extracomment>
      <translation>escalar y recortar</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="132"/>
      <source>folder thumbnails</source>
      <translation>miniaturas de carpeta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="154"/>
      <source>autoload</source>
      <extracomment>file manager settings popdown: automatically load the folder thumbnails</extracomment>
      <translation>carga automática</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="163"/>
      <source>loop</source>
      <extracomment>file manager settings popdown: loop through the folder thumbnails</extracomment>
      <translation>ciclar</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="172"/>
      <source>timeout</source>
      <extracomment>file manager settings popdown: timeout between switching folder thumbnails</extracomment>
      <translation>tiempo de espera</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="183"/>
      <source>1 second</source>
      <translation>1 segundo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="191"/>
      <source>half a second</source>
      <translation>medio segundo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="205"/>
      <source>show bookmarks</source>
      <translation>mostrar marcadores</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="213"/>
      <source>show devices</source>
      <extracomment>file manager settings popdown: the devices here are the storage devices</extracomment>
      <translation>mostrar dispositivos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="222"/>
      <source>Preview</source>
      <extracomment>file manager settings popdown: menu title</extracomment>
      <translation>Vista previa</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="234"/>
      <source>higher resolution</source>
      <extracomment>file manager settings popdown: use higher resolution for image previews</extracomment>
      <translation>mayor resolución</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="243"/>
      <source>blur</source>
      <extracomment>file manager settings popdown: blur image previews</extracomment>
      <translation>desenfoque</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQSettingsMenu.qml" line="262"/>
      <source>color intensity</source>
      <extracomment>file manager settings popdown: color intensity of image previews</extracomment>
      <translation>intensidad de color</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="35"/>
      <source>%1 files already exist in the target directory.</source>
      <translation>Ya existen %1 archivos en el directorio de destino.</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="40"/>
      <source>Check the files below that you want to paste anyways. Files left unchecked will not be pasted.</source>
      <translation>Compruebe los archivos siguientes que desea pegar de todos modos. Los archivos que se dejen sin marcar no se pegarán.</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="162"/>
      <source>Select all</source>
      <translation>Seleccionar todo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="169"/>
      <source>Select none</source>
      <translation>No seleccionar ninguno</translation>
    </message>
  </context>
  <context>
    <name>filemanagement</name>
    <message>
      <location filename="../qml/actions/popout/PQDeletePopout.qml" line="32"/>
      <location filename="../qml/actions/PQDelete.qml" line="42"/>
      <source>Delete file?</source>
      <extracomment>Window title</extracomment>
      <translation>¿Eliminar el archivo?</translation>
    </message>
    <message>
      <location filename="../qml/actions/popout/PQRenamePopout.qml" line="32"/>
      <location filename="../qml/actions/PQRename.qml" line="41"/>
      <location filename="../qml/actions/PQRename.qml" line="44"/>
      <source>Rename file</source>
      <extracomment>Window title</extracomment>
      <translation>Renombrar archivo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQDelete.qml" line="44"/>
      <location filename="../qml/actions/PQDelete.qml" line="112"/>
      <source>Move to trash</source>
      <translation>Mover a la basura</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQDelete.qml" line="48"/>
      <location filename="../qml/actions/PQDelete.qml" line="114"/>
      <source>Delete permanently</source>
      <translation>Eliminar permanentemente</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQDelete.qml" line="78"/>
      <source>Are you sure you want to delete this file?</source>
      <translation>¿Está seguro que desea borrar este archivo?</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQDelete.qml" line="99"/>
      <source>An error occured, file could not be deleted!</source>
      <translation>¡Ocurrió un error, el archivo no pudo ser eliminado!</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQRename.qml" line="62"/>
      <source>old filename:</source>
      <translation>nombre de archivo anterior:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQRename.qml" line="87"/>
      <source>new filename:</source>
      <translation>nombre de archivo nuevo:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQRename.qml" line="140"/>
      <source>An error occured, file could not be renamed</source>
      <translation>Ocurrió un error, el archivo no pudo ser renombrado</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQRename.qml" line="152"/>
      <source>A file with this filename already exists</source>
      <translation>Ya existe un archivo con este nombre</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQCopy.qml" line="55"/>
      <source>Copy here</source>
      <translation>Copiar aquí</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQCopy.qml" line="91"/>
      <location filename="../qml/actions/PQMove.qml" line="94"/>
      <source>An error occured</source>
      <translation>Ha ocurrido un error</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQCopy.qml" line="96"/>
      <source>File could not be copied</source>
      <translation>No se pudo copiar el archivo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQMove.qml" line="56"/>
      <source>Move here</source>
      <translation>Mover aquí</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQMove.qml" line="99"/>
      <source>File could not be moved</source>
      <translation>No se pudo mover el archivo</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="555"/>
      <source>File successfully deleted</source>
      <translation>Se eliminó el archivo correctamente</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="558"/>
      <source>Could not delete file</source>
      <translation>No se pudo eliminar el archivo</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="565"/>
      <source>File successfully moved to trash</source>
      <translation>Se movió el archivo a la papelera correctamente</translation>
    </message>
    <message>
      <location filename="../qml/other/PQShortcuts.qml" line="568"/>
      <source>Could not move file to trash</source>
      <translation>No se pudo mover el archivo a la papelera</translation>
    </message>
  </context>
  <context>
    <name>filter</name>
    <message>
      <location filename="../qml/actions/popout/PQFilterPopout.qml" line="32"/>
      <location filename="../qml/actions/PQFilter.qml" line="38"/>
      <source>Filter images in current directory</source>
      <extracomment>Window title</extracomment>
      <translation>Filtrar imágenes en el directorio actual</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="41"/>
      <source>Filter</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Filtrar</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="48"/>
      <source>Remove filter</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Eliminar filtro</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="79"/>
      <source>To filter by file extension, start the term with a dot. Setting the width or height of the resolution to 0 ignores that dimension.</source>
      <translation>Para filtrar por extensión de archivo, inicie el término con un punto. Establecer el ancho o la altura de la resolución hace que se ignore esa dimensión.</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="108"/>
      <source>File name/extension:</source>
      <translation>Nombre/extensión de archivo:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="128"/>
      <source>Enter terms</source>
      <translation>Introducir términos</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="142"/>
      <source>Image Resolution</source>
      <translation>Resolución de imagen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="160"/>
      <location filename="../qml/actions/PQFilter.qml" line="225"/>
      <source>greater than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution GREATER THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size GREATER THAN 123 KB/MB&apos;</extracomment>
      <translation>mayor que</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="162"/>
      <location filename="../qml/actions/PQFilter.qml" line="227"/>
      <source>less than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution LESS THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size LESS THAN 123 KB/MB&apos;</extracomment>
      <translation>menor que</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="207"/>
      <source>File size</source>
      <translation>Tamaño de archivo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQFilter.qml" line="276"/>
      <source>Please note that filtering by image resolution can take a little while, depending on the number of images in the folder.</source>
      <translation>Nótese que filtrar por resolución de imagen puede tomar algo de tiempo, dependiendo del número de imágenes en la carpeta.</translation>
    </message>
  </context>
  <context>
    <name>histogram</name>
    <message>
      <location filename="../qml/ongoing/popout/PQHistogramPopout.qml" line="33"/>
      <location filename="../qml/ongoing/PQHistogram.qml" line="193"/>
      <source>Histogram</source>
      <extracomment>Window title</extracomment>
      <translation>Histograma</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQHistogram.qml" line="57"/>
      <source>Click-and-drag to move.</source>
      <translation>Clic y arrastrar para mover.</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQHistogram.qml" line="57"/>
      <source>Right click to switch version.</source>
      <translation>Clic derecho para cambiar versión.</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQHistogram.qml" line="165"/>
      <source>Loading...</source>
      <translation>Cargando...</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQHistogram.qml" line="179"/>
      <source>Error loading histogram</source>
      <translation>Error al cargar el histograma</translation>
    </message>
  </context>
  <context>
    <name>image</name>
    <message>
      <location filename="../qml/image/PQImage.qml" line="757"/>
      <source>Click here to enter viewer mode</source>
      <translation>Pulse aquí para entrar en el modo de visualización</translation>
    </message>
    <message>
      <location filename="../qml/image/PQImage.qml" line="776"/>
      <source>Hide central button for entering viewer mode</source>
      <translation>Ocultar botón central para entrar en modo de visualización</translation>
    </message>
  </context>
  <context>
    <name>imageprovider</name>
    <message>
      <location filename="../cplusplus/images/provider/pqc_providerfull.cpp" line="47"/>
      <location filename="../cplusplus/images/provider/pqc_providerthumb.cpp" line="195"/>
      <source>File failed to load, it does not exist!</source>
      <translation>¡Error al cargar, no existe el archivo!</translation>
    </message>
  </context>
  <context>
    <name>imgur</name>
    <message>
      <location filename="../qml/actions/popout/PQImgurPopout.qml" line="32"/>
      <location filename="../qml/actions/PQImgur.qml" line="40"/>
      <location filename="../qml/actions/PQImgur.qml" line="41"/>
      <source>Upload to imgur.com</source>
      <extracomment>Window title</extracomment>
      <translation>Subir a imgur.com</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="48"/>
      <source>Show past uploads</source>
      <translation>Mostrar cargas anteriores</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="99"/>
      <source>This seems to take a long time...</source>
      <translation>Parece que esto tomará mucho tiempo...</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="100"/>
      <source>There might be a problem with your internet connection or the imgur.com servers.</source>
      <translation>Puede que haya un problema con su conexión a internet o con los servidores de imgur.com.</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="116"/>
      <source>An Error occurred while uploading image!</source>
      <translation>¡Ha ocurrido un error mientras se subía la imagen!</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="117"/>
      <source>Error code:</source>
      <translation>Código de error:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="137"/>
      <source>You do not seem to be connected to the internet...</source>
      <translation>No parece haber conexión con internet...</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="138"/>
      <source>Unable to upload!</source>
      <translation>¡No se puede subir el archivo!</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="191"/>
      <source>Access Image</source>
      <translation>Acceder a imagen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="206"/>
      <location filename="../qml/actions/PQImgur.qml" line="243"/>
      <location filename="../qml/actions/PQImgur.qml" line="408"/>
      <location filename="../qml/actions/PQImgur.qml" line="440"/>
      <source>Click to open in browser</source>
      <translation>Clic para abrir en el navegador</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="228"/>
      <source>Delete Image</source>
      <translation>Eliminar imagen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="305"/>
      <source>No past uploads found</source>
      <extracomment>The uploads are uploads to imgur.com</extracomment>
      <translation>No se encontró ninguna carga anterior</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="399"/>
      <source>Access:</source>
      <extracomment>Used as in: access this image</extracomment>
      <translation>Acceso:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="431"/>
      <source>Delete:</source>
      <extracomment>Used as in: delete this image</extracomment>
      <translation>Eliminar:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQImgur.qml" line="479"/>
      <source>Clear all</source>
      <extracomment>Written on button, please keep short. Used as in: clear all entries</extracomment>
      <translation>Vaciar todo</translation>
    </message>
  </context>
  <context>
    <name>logging</name>
    <message>
      <location filename="../qml/ongoing/popout/PQLoggingPopout.qml" line="108"/>
      <source>enable</source>
      <extracomment>Used as in: enable debug message</extracomment>
      <translation>activar</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQLoggingPopout.qml" line="140"/>
      <source>copy to clipboard</source>
      <extracomment>the thing being copied here are the debug messages</extracomment>
      <translation>copiar al portapapeles</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQLoggingPopout.qml" line="146"/>
      <source>save to file</source>
      <extracomment>the thing saved to files here are the debug messages</extracomment>
      <translation>guardar en archivo</translation>
    </message>
  </context>
  <context>
    <name>mapcurrent</name>
    <message>
      <location filename="../qml/ongoing/popout/PQMapCurrentPopout.qml" line="33"/>
      <location filename="../qml/ongoing/PQMapCurrent.qml" line="186"/>
      <source>Current location</source>
      <extracomment>Window title</extracomment>
      <translation>Ubicación actual</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMapCurrent.qml" line="62"/>
      <source>Click-and-drag to move.</source>
      <translation>Pulse y arrastre para mover.</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMapCurrent.qml" line="166"/>
      <source>No location data</source>
      <translation>Sin datos de ubicación</translation>
    </message>
  </context>
  <context>
    <name>mapexplorer</name>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="257"/>
      <source>no images in currently visible area</source>
      <extracomment>the currently visible area refers to the latitude/longitude selection in the map explorer</extracomment>
      <translation>no hay imágenes en el área visible actual</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="269"/>
      <source>no images with location data in current folder</source>
      <translation>no hay imágenes con datos de ubicación en la carpeta actual</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImagesTweaks.qml" line="46"/>
      <source>Image zoom:</source>
      <translation>Zum de imagen:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImagesTweaks.qml" line="69"/>
      <source>scale and crop thumbnails</source>
      <translation>escalar y recortar miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerMapTweaks.qml" line="47"/>
      <source>Map zoom:</source>
      <translation>Zum de mapa:</translation>
    </message>
    <message>
      <location filename="../qml/actions/mapexplorerparts/PQMapExplorerMapTweaks.qml" line="73"/>
      <source>Reset view</source>
      <extracomment>The view here is the map layout in the map explorer</extracomment>
      <translation>Restablecer vista</translation>
    </message>
  </context>
  <context>
    <name>metadata</name>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="172"/>
      <source>No file loaded</source>
      <translation>No se cargó ningún archivo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="194"/>
      <source>Metadata</source>
      <extracomment>The title of the floating element</extracomment>
      <translation>Metadatos</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="236"/>
      <source>File name</source>
      <translation>Nombre del archivo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="242"/>
      <source>Dimensions</source>
      <translation>Dimensiones</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="248"/>
      <source>Image</source>
      <translation>Imagen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="254"/>
      <source>File size</source>
      <translation>Tamaño del archivo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="260"/>
      <source>File type</source>
      <translation>Tipo de archivo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="271"/>
      <source>Make</source>
      <translation>Fabricante</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="277"/>
      <source>Model</source>
      <translation>Modelo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="283"/>
      <source>Software</source>
      <translation>Programa</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="294"/>
      <source>Time Photo was Taken</source>
      <translation>Hora en la que se tomó la foto</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="300"/>
      <source>Exposure Time</source>
      <translation>Tiempo de exposición</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="306"/>
      <source>Flash</source>
      <translation>Flash</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="318"/>
      <source>Scene Type</source>
      <translation>Tipo de escena</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="324"/>
      <source>Focal Length</source>
      <translation>Distancia focal</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="330"/>
      <source>F Number</source>
      <translation>Número f</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="336"/>
      <source>Light Source</source>
      <translation>Fuente de luz</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="347"/>
      <source>Keywords</source>
      <translation>Palabras Claves</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="353"/>
      <source>Location</source>
      <translation>Ubicación</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="359"/>
      <source>Copyright</source>
      <translation>Derechos de autor</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQMetaData.qml" line="370"/>
      <source>GPS Position</source>
      <translation>Posición del GPS</translation>
    </message>
  </context>
  <context>
    <name>navigate</name>
    <message>
      <location filename="../qml/ongoing/PQNavigation.qml" line="79"/>
      <source>Click and drag to move</source>
      <translation>Clic y arrastrar para mover</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQNavigation.qml" line="117"/>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="101"/>
      <source>Navigate to previous image in folder</source>
      <translation>Navegar a la imagen anterior en la carpeta</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQNavigation.qml" line="151"/>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="123"/>
      <source>Navigate to next image in folder</source>
      <translation>Navegar a la siguiente imagen en la carpeta</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQNavigation.qml" line="182"/>
      <source>Show main menu</source>
      <translation>Mostrar menú principal</translation>
    </message>
  </context>
  <context>
    <name>other</name>
    <message>
      <location filename="../qml/other/PQBackgroundMessage.qml" line="87"/>
      <source>Click anywhere to open a file</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation>Clic donde sea para abrir un archivo</translation>
    </message>
    <message>
      <location filename="../qml/other/PQBackgroundMessage.qml" line="99"/>
      <source>Move your cursor to the indicated window edges for various actions</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation type="unfinished">Move your cursor to the indicated window edges for various actions</translation>
    </message>
  </context>
  <context>
    <name>popinpopout</name>
    <message>
      <location filename="../qml/actions/PQMapExplorer.qml" line="132"/>
      <location filename="../qml/elements/PQTemplateFloating.qml" line="133"/>
      <location filename="../qml/elements/PQTemplateFullscreen.qml" line="227"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="205"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="816"/>
      <location filename="../qml/ongoing/PQMetaData.qml" line="481"/>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="340"/>
      <source>Merge into main interface</source>
      <extracomment>Tooltip of small button to merge a popped out element (i.e., one in its own window) into the main interface</extracomment>
      <translation>Combinar dentro de la interfaz principal</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQMapExplorer.qml" line="134"/>
      <location filename="../qml/elements/PQTemplateFloating.qml" line="135"/>
      <location filename="../qml/elements/PQTemplateFullscreen.qml" line="229"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="207"/>
      <location filename="../qml/ongoing/PQMainMenu.qml" line="818"/>
      <location filename="../qml/ongoing/PQMetaData.qml" line="483"/>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="342"/>
      <source>Move to its own window</source>
      <extracomment>Tooltip of small button to show an element in its own window (i.e., not merged into main interface)</extracomment>
      <translation>Mover a su propia ventana</translation>
    </message>
  </context>
  <context>
    <name>quickinfo</name>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="152"/>
      <source>Click here to show main menu</source>
      <translation>Haz clic aquí para mostrar el menú principal</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="176"/>
      <source>Click here to not keep window in foreground</source>
      <translation>Clic aquí para no mantener la ventana en primer plano</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="176"/>
      <source>Click here to keep window in foreground</source>
      <translation>Clic aquí para mantener la ventana en primer plano</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="206"/>
      <source>Click here to minimize window</source>
      <translation>Clic aquí para minimizar ventana</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="239"/>
      <source>Click here to restore window</source>
      <translation>Clic aquí para restaurar ventana</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="240"/>
      <source>Click here to maximize window</source>
      <translation>Clic aquí para maximizar ventana</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="272"/>
      <source>Click here to enter fullscreen mode</source>
      <translation>Clic aquí para entrar en modo de pantalla completa</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="273"/>
      <source>Click here to exit fullscreen mode</source>
      <translation>Clic aquí para salir de modo de pantalla completa</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQWindowButtons.qml" line="305"/>
      <source>Click here to close PhotoQt</source>
      <translation>Haga clic aquí para cerrar PhotoQt</translation>
    </message>
  </context>
  <context>
    <name>scale</name>
    <message>
      <location filename="../qml/actions/popout/PQScalePopout.qml" line="32"/>
      <location filename="../qml/actions/PQScale.qml" line="42"/>
      <source>Scale image</source>
      <extracomment>Window title</extracomment>
      <translation>Escalar imagen</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="44"/>
      <location filename="../qml/actions/PQScale.qml" line="394"/>
      <source>Scale</source>
      <translation>Escala</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="71"/>
      <source>An error occured, file could not be scaled</source>
      <translation>Se produjo un error; no se pudo escalar el archivo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="81"/>
      <source>Scaling of this file format is not yet supported</source>
      <translation>Aún no se admite el escalado de este formato de archivo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="100"/>
      <source>Width:</source>
      <extracomment>The number of horizontal pixels of the image</extracomment>
      <translation>Ancho:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="106"/>
      <source>Height:</source>
      <extracomment>The number of vertical pixels of the image</extracomment>
      <translation>Alto:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="206"/>
      <source>New size:</source>
      <translation>Tamaño nuevo:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="209"/>
      <source>pixels</source>
      <extracomment>This is used as in: 100x100 pixels</extracomment>
      <translation>píxeles</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQScale.qml" line="292"/>
      <source>Quality:</source>
      <translation>Calidad:</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager</name>
    <message>
      <location filename="../qml/settingsmanager/popout/PQSettingsManagerPopout.qml" line="33"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="16"/>
      <source>Settings Manager</source>
      <extracomment>Window title</extracomment>
      <translation>Gestor de ajustes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="21"/>
      <source>Apply changes</source>
      <translation>Aplicar cambios</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="25"/>
      <source>Revert changes</source>
      <translation>Revertir cambios</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="55"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="74"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="271"/>
      <source>Interface</source>
      <extracomment>A settings category
----------
This is a shortcut category</extracomment>
      <translation>Interfaz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="58"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="60"/>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="91"/>
      <source>Language</source>
      <extracomment>A settings subcategory and the qml filename
----------
A settings title</extracomment>
      <translation>Idioma</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="65"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="66"/>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="56"/>
      <source>Background</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Fondo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="85"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="86"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="110"/>
      <source>Popout</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation type="unfinished">Popout</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="123"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="124"/>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="59"/>
      <source>Context menu</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Menú contextual</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="144"/>
      <source>Window</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Ventana</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="167"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="215"/>
      <source>Image</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="182"/>
      <source>Interaction</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Interacción</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="193"/>
      <source>Folder</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Carpeta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="204"/>
      <source>Share online</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Compartir en línea</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>All thumbnails</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Todas las miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="243"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="380"/>
      <source>Manage</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Gestionar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="294"/>
      <source>Face tags</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Etiquetas de rostros</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="312"/>
      <source>Instance</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Proceso</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="324"/>
      <source>Tray icon</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Icono en área de notificaciones</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="164"/>
      <source>Image view</source>
      <extracomment>A settings category</extracomment>
      <translation>Vista de imágenes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="296"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="70"/>
      <source>Look</source>
      <extracomment>Settings title</extracomment>
      <translation>Aspecto</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="285"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="343"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="368"/>
      <source>Behavior</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Comportamiento</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="113"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="114"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="68"/>
      <source>Edges</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Bordes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="212"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="45"/>
      <source>Thumbnails</source>
      <extracomment>A settings category
----------
Used as descriptor for a screen edge action</extracomment>
      <translation>Miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="258"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="49"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="58"/>
      <source>Metadata</source>
      <extracomment>A settings category
----------
Used as descriptor for a screen edge action
----------
Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Metadatos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="261"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="262"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="104"/>
      <source>Labels</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Etiquetas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="309"/>
      <source>Session</source>
      <extracomment>A settings category</extracomment>
      <translation>Sesión</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="317"/>
      <source>Remember</source>
      <extracomment>A settings subcategory</extracomment>
      <translation>Recordar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="335"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="338"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="339"/>
      <source>File types</source>
      <extracomment>A settings category
----------
A settings subcategory</extracomment>
      <translation>Tipos de archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="360"/>
      <source>Keyboard &amp; Mouse</source>
      <extracomment>A settings category</extracomment>
      <translation>Teclado y ratón</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="363"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="364"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="204"/>
      <source>Shortcuts</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Atajos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="382"/>
      <source>Reset</source>
      <translation>Restablecer</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="383"/>
      <source>Reset settings</source>
      <translation>Restablecer configuración</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="384"/>
      <source>Reset shortcuts</source>
      <translation>Restablecer atajos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="387"/>
      <source>Export/Import</source>
      <translation>Exportar/importar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="388"/>
      <source>Export settings</source>
      <translation type="unfinished">Export settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="389"/>
      <source>Import settings</source>
      <translation type="unfinished">Import settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="460"/>
      <source>Ctrl+S = Apply changes, Ctrl+R = Revert changes, Esc = Close</source>
      <translation type="unfinished">Ctrl+S = Apply changes, Ctrl+R = Revert changes, Esc = Close</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="493"/>
      <source>Unsaved changes</source>
      <translation>Cambios no guardados</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="501"/>
      <source>The settings on this page have changed. Do you want to apply or discard them?</source>
      <translation>La configuración en esta página ha cambiado. ¿Quiere aplicarla o descartarla?</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="513"/>
      <source>Apply</source>
      <extracomment>written on button, used as in: apply changes</extracomment>
      <translation>Aplicar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="532"/>
      <source>Discard</source>
      <extracomment>written on button, used as in: discard changes</extracomment>
      <translation>Descartar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="344"/>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="45"/>
      <source>PDF</source>
      <extracomment>Settings title</extracomment>
      <translation>PDF</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="51"/>
      <source>PhotoQt can show PDF and Postscript documents alongside your images, you can even enter a multi-page document and browse its pages as if they were images in a folder. The quality setting here - specified in dots per pixel (dpi) - affects the resolution and speed of loading such pages.</source>
      <translation>PhotoQt puede mostrar documentos PDF y PostScript junto a las imágenes, e incluso puede introducir un documento de varias páginas y explorar las páginas como si fuesen imágenes en una carpeta. La configuración de calidad definida aquí (especificada en puntos por píxel, ppp) afecta la resolución y velocidad de carga de las páginas.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="80"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="78"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="216"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="254"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="72"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="113"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="153"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="257"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="301"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="384"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="170"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="253"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="122"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="208"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="75"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="169"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="78"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="219"/>
      <source>current value:</source>
      <extracomment>The current value of the slider specifying the PDF quality
----------
The current value of the slider specifying the font size for the status information
----------
The current value of the slider specifying the size of the window buttons</extracomment>
      <translation>valor actual:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="345"/>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="90"/>
      <source>Archive</source>
      <extracomment>Settings title</extracomment>
      <translation>Archivador</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="96"/>
      <source>PhotoQt allows the browsing of all images contained in an archive file (ZIP, RAR, etc.) as if they all are located in a folder. By default, PhotoQt uses Libarchive for this purpose, but for RAR archives in particular PhotoQt can call the external tool unrar to load and display the archive and its contents. Note that this requires unrar to be installed and located in your path.</source>
      <translation type="unfinished">PhotoQt allows the browsing of all images contained in an archive file (ZIP, RAR, etc.) as if they all are located in a folder. By default, PhotoQt uses Libarchive for this purpose, but for RAR archives in particular PhotoQt can call the external tool unrar to load and display the archive and its contents. Note that this requires unrar to be installed and located in your path.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="103"/>
      <source>use external tool: unrar</source>
      <translation>utilizar herramienta externa: unrar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="346"/>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="115"/>
      <source>Video</source>
      <extracomment>Settings title</extracomment>
      <translation>Vídeo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="121"/>
      <source>PhotoQt can treat video files the same as image files, as long as the respective video formats are enabled. There are a few settings available for managing how videos behave in PhotoQt: Whether they should autoplay when loaded, whether they should loop from the beginning when the end is reached, whether to prefer libmpv (if available) or Qt for video playback, and which video thumbnail generator to use.</source>
      <translation type="unfinished">PhotoQt can treat video files the same as image files, as long as the respective video formats are enabled. There are a few settings available for managing how videos behave in PhotoQt: Whether they should autoplay when loaded, whether they should loop from the beginning when the end is reached, whether to prefer libmpv (if available) or Qt for video playback, and which video thumbnail generator to use.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="129"/>
      <source>Autoplay</source>
      <translation>Reproducción automática</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="135"/>
      <source>Loop</source>
      <translation type="unfinished">Loop</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="145"/>
      <source>prefer Qt Multimedia</source>
      <translation>preferir Qt Multimedia</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="150"/>
      <source>prefer Libmpv</source>
      <translation>preferir Libmpv</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="161"/>
      <source>Video thumbnail generator:</source>
      <translation>Generador de miniaturas de vídeo:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="347"/>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="179"/>
      <source>Viewer mode</source>
      <extracomment>Settings title</extracomment>
      <translation>Modo de visualización</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="185"/>
      <source>When a document or achive is loaded in PhotoQt, it is possible to enter such a file. This means that PhotoQt will act as if the content of the file is located in some folder and loads the content as thumbnails allowing for the usual interaction and navigation to browse around. This viewer mode can be entered either by a small button that will show up below the status info, or it is possible to also show a big central button to activate this mode.</source>
      <translation type="unfinished">When a document or achive is loaded in PhotoQt, it is possible to enter such a file. This means that PhotoQt will act as if the content of the file is located in some folder and loads the content as thumbnails allowing for the usual interaction and navigation to browse around. This viewer mode can be entered either by a small button that will show up below the status info, or it is possible to also show a big central button to activate this mode.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="192"/>
      <source>show big button to enter viewer mode</source>
      <translation type="unfinished">show big button to enter viewer mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="47"/>
      <source>images</source>
      <extracomment>This is a category of files PhotoQt can recognize: any image format</extracomment>
      <translation>imágenes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="49"/>
      <source>compressed files</source>
      <extracomment>This is a category of files PhotoQt can recognize: compressed files like zip, tar, cbr, 7z, etc.</extracomment>
      <translation>archivos comprimidos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="51"/>
      <source>documents</source>
      <extracomment>This is a category of files PhotoQt can recognize: documents like pdf, txt, etc.</extracomment>
      <translation>documentos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="53"/>
      <source>videos</source>
      <extracomment>This is a type of category of files PhotoQt can recognize: videos like mp4, avi, etc.</extracomment>
      <translation>vídeos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="59"/>
      <source>Enable</source>
      <extracomment>As in: &quot;Enable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Activar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="65"/>
      <source>Disable</source>
      <extracomment>As in: &quot;Disable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Desactivar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="90"/>
      <source>Enable everything</source>
      <extracomment>As in &quot;Enable every single file format PhotoQt can open in any category&quot;</extracomment>
      <translation>Activar todo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="102"/>
      <source>Currently there are %1 file formats enabled</source>
      <extracomment>The %1 will be replaced with the number of file formats, please don&apos;t forget to add it.</extracomment>
      <translation>Actualmente hay %1 formatos de archivo activados</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="131"/>
      <source>Search by description or file ending</source>
      <translation>Buscar por descripción o por extensión de archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="140"/>
      <source>Search by image library or category</source>
      <translation>Buscar por librería de imágenes o por categoría</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="239"/>
      <source>File endings:</source>
      <translation>Extensiones de archivo:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="194"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="41"/>
      <source>Looping</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Looping</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="55"/>
      <source>Loop around</source>
      <extracomment>When reaching the end of the images in the folder whether to loop back around to the beginning or not</extracomment>
      <translation type="unfinished">Loop around</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="195"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="67"/>
      <source>Sort images</source>
      <extracomment>Settings title</extracomment>
      <translation>Ordenar imágenes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="73"/>
      <source>Images in a folder can be sorted in different ways depending on your preferences. These criteria here are the ones that can be used in a very quick way. Once a folder is loaded it is possible to further sort a folder in several advanced ways using the menu option for sorting.</source>
      <translation type="unfinished">Images in a folder can be sorted in different ways depending on your preferences. These criteria here are the ones that can be used in a very quick way. Once a folder is loaded it is possible to further sort a folder in several advanced ways using the menu option for sorting.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="82"/>
      <source>Sort by:</source>
      <translation>Ordenar por:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="87"/>
      <source>natural name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>nombre natural</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="89"/>
      <source>name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>nombre</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="91"/>
      <source>time</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>hora</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="93"/>
      <source>size</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>tamaño</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="95"/>
      <source>type</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>tipo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="106"/>
      <source>ascending order</source>
      <extracomment>Sort images in ascending order</extracomment>
      <translation>orden ascendente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="113"/>
      <source>descending order</source>
      <extracomment>Sort images in descending order</extracomment>
      <translation>orden descendente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="183"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="43"/>
      <source>Zoom</source>
      <extracomment>Settings title</extracomment>
      <translation>Zum</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="49"/>
      <source>PhotoQt allows for a great deal of flexibility in viewing images at the perfect size. Additionally it allows for control of how fast the zoom happens, and if there is a minimum/maximum zoom level at which it should always stop no matter what. Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</source>
      <translation type="unfinished">PhotoQt allows for a great deal of flexibility in viewing images at the perfect size. Additionally it allows for control of how fast the zoom happens, and if there is a minimum/maximum zoom level at which it should always stop no matter what. Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="56"/>
      <source>zoom speed:</source>
      <translation>velocidad de zum:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="85"/>
      <source>minimum zoom:</source>
      <translation>zum mínimo:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="125"/>
      <source>maximum zoom:</source>
      <translation>zum máximo:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="196"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="125"/>
      <source>Animation</source>
      <extracomment>Settings title</extracomment>
      <translation>Animación</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="47"/>
      <source>When loading an image PhotoQt loads all images in the folder as thumbnails for easy navigation. When PhotoQt reaches the end of the list of files, it can either stop right there or loop back to the other end of the list and keep going.</source>
      <translation>Al cargar una imagen, PhotoQt carga todas las imágenes de la carpeta como miniaturas para una fácil navegación. Cuando PhotoQt llegue al final de la lista de archivos, puede detenerse allí mismo o volver al otro extremo de la lista y seguir.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="131"/>
      <source>When switching between images PhotoQt can add an animation to smoothes such a transition. There are a whole bunch of transitions to choose from, and also an option for PhotoQt to choose one at random each time. Additionally, the speed of the chosen animation can be chosen from very slow to very fast.</source>
      <translation type="unfinished">When switching between images PhotoQt can add an animation to smoothes such a transition. There are a whole bunch of transitions to choose from, and also an option for PhotoQt to choose one at random each time. Additionally, the speed of the chosen animation can be chosen from very slow to very fast.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="138"/>
      <source>animate switching between images</source>
      <translation>animar cambio de imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="149"/>
      <source>Animation:</source>
      <translation>Animación:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="154"/>
      <source>opacity</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>opacidad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="156"/>
      <source>along x-axis</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>a lo largo del eje X</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="158"/>
      <source>along y-axis</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>a lo largo del eje Y</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="160"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="103"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="238"/>
      <source>rotation</source>
      <extracomment>This is referring to an in/out animation of images
----------
Please keep short! This is the rotation of the current image</extracomment>
      <translation>giro</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="162"/>
      <source>explosion</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>explosión</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="164"/>
      <source>implosion</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>implosión</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="166"/>
      <source>choose one at random</source>
      <extracomment>This is referring to an in/out animation of images</extracomment>
      <translation>elegir una al azar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="177"/>
      <source>very slow</source>
      <extracomment>used here for the animation speed</extracomment>
      <translation>muy lenta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="188"/>
      <source>very fast</source>
      <extracomment>used here for the animation speed</extracomment>
      <translation>muy rápida</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="184"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="164"/>
      <source>Floating navigation</source>
      <extracomment>Settings title</extracomment>
      <translation>Navegación flotante</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="170"/>
      <source>Switching between images can be done in various ways. It is possible to do so through the shortcuts, through the main menu, or through floating navigation buttons. These floating buttons were added especially with touch screens in mind, as it allows easier navigation without having to use neither the keyboard nor the mouse. In addition to buttons for navigation it also includes a button to hide and show the main menu.</source>
      <translation type="unfinished">Switching between images can be done in various ways. It is possible to do so through the shortcuts, through the main menu, or through floating navigation buttons. These floating buttons were added especially with touch screens in mind, as it allows easier navigation without having to use neither the keyboard nor the mouse. In addition to buttons for navigation it also includes a button to hide and show the main menu.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="177"/>
      <source>show floating navigation buttons</source>
      <translation type="unfinished">show floating navigation buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="172"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="244"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="226"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="55"/>
      <source>Cache</source>
      <extracomment>Settings title</extracomment>
      <translation>Antememoria</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="232"/>
      <source>Whenever an image is loaded in full, PhotoQt caches such images in order to greatly improve performance if that same image is shown again soon after. This is done up to a certain memory limit after which the first images in the cache will be removed again to free up the required memory. Depending on the amount of memory available on the system, a higher value can lead to an improved user experience.</source>
      <translation type="unfinished">Whenever an image is loaded in full, PhotoQt caches such images in order to greatly improve performance if that same image is shown again soon after. This is done up to a certain memory limit after which the first images in the cache will be removed again to free up the required memory. Depending on the amount of memory available on the system, a higher value can lead to an improved user experience.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="171"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="177"/>
      <source>Interpolation</source>
      <extracomment>Settings title</extracomment>
      <translation>Interpolación</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="62"/>
      <source>none</source>
      <extracomment>used as in: no margin around image</extracomment>
      <translation>ninguno</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="107"/>
      <source>large images:</source>
      <translation>imágenes grandes:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="111"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="135"/>
      <source>fit to view</source>
      <translation>encajar en la vista</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="115"/>
      <source>load at full scale</source>
      <translation>cargar a escala completa</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="131"/>
      <source>small images:</source>
      <translation>imágenes pequeñas:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="141"/>
      <source>load as-is</source>
      <translation>cargar tal como son</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="165"/>
      <source>show checkerboard pattern</source>
      <translation>mostrar motivo de damero</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="183"/>
      <source>PhotoQt makes use of interpolation algorithms to show smooth lines and avoid potential artefacts to be shown. However, for small images this can lead to blurry images when no interpolation is necessary. Thus, for small images under the specified threshold PhotoQt can skip the use of interpolation algorithms. Note that both the width and height of an image need to be smaller than the threshold for it to be applied.</source>
      <translation type="unfinished">PhotoQt makes use of interpolation algorithms to show smooth lines and avoid potential artefacts to be shown. However, for small images this can lead to blurry images when no interpolation is necessary. Thus, for small images under the specified threshold PhotoQt can skip the use of interpolation algorithms. Note that both the width and height of an image need to be smaller than the threshold for it to be applied.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="190"/>
      <source>disable interpolation for small images</source>
      <translation>desactivar interpolación en imágenes pequeñas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="168"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="43"/>
      <source>Margin</source>
      <extracomment>Settings title</extracomment>
      <translation>Margen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="49"/>
      <source>PhotoQt shows the main image fully stretched across its application window. For an improved visual experience, it can add a small margin of some pixels around the image to not have it stretch completely from edge to edge. Note that once an image is zoomed in the margin might be filled, it only applies to the default zoom level of an image.</source>
      <translation type="unfinished">PhotoQt shows the main image fully stretched across its application window. For an improved visual experience, it can add a small margin of some pixels around the image to not have it stretch completely from edge to edge. Note that once an image is zoomed in the margin might be filled, it only applies to the default zoom level of an image.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="169"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="88"/>
      <source>Image size</source>
      <extracomment>Settings title</extracomment>
      <translation>Tamaño de imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="94"/>
      <source>PhotoQt ensures that an image is fully visible when first loaded. To achieve this, large images are zoomed out to fit into the view, but images smaller than the view are left as-is. Alternatively, large images can be loaded at full scale, and small images can be zoomed in to also fit into view. The latter option might result in small images appearing pixelated.</source>
      <translation type="unfinished">PhotoQt ensures that an image is fully visible when first loaded. To achieve this, large images are zoomed out to fit into the view, but images smaller than the view are left as-is. Alternatively, large images can be loaded at full scale, and small images can be zoomed in to also fit into view. The latter option might result in small images appearing pixelated.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="170"/>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="152"/>
      <source>Transparency marker</source>
      <extracomment>Settings title</extracomment>
      <translation>Marcador de transparencia</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQImage.qml" line="158"/>
      <source>When an image contains transparent areas, then that area can be left transparent resulting in the background of PhotoQt to show. Alternatively, it is possible to show a checkerboard pattern behind the image, exposing the transparent areas of an image much clearer.</source>
      <translation type="unfinished">When an image contains transparent areas, then that area can be left transparent resulting in the background of PhotoQt to show. Alternatively, it is possible to show a checkerboard pattern behind the image, exposing the transparent areas of an image much clearer.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="46"/>
      <source>It is possible to share an image from PhotoQt directly to imgur.com. This can either be done anonymously or to an imgur.com account. For the former, no setup is required, after a successful upload you are presented with the URL to access and the URL to delete the image. For the latter, PhotoQt first needs to be authenticated to an imgur.com user account.</source>
      <translation type="unfinished">It is possible to share an image from PhotoQt directly to imgur.com. This can either be done anonymously or to an imgur.com account. For the former, no setup is required, after a successful upload you are presented with the URL to access and the URL to delete the image. For the latter, PhotoQt first needs to be authenticated to an imgur.com user account.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="53"/>
      <source>Note that any change here is saved immediately!</source>
      <translation type="unfinished">Note that any change here is saved immediately!</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="62"/>
      <source>Authenticated with user account:</source>
      <translation type="unfinished">Authenticated with user account:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="75"/>
      <source>Authenticate</source>
      <extracomment>Written on button, used as in: Authenticate with user account</extracomment>
      <translation>Autenticar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="77"/>
      <source>Forget account</source>
      <extracomment>Written on button, used as in: Forget user account</extracomment>
      <translation>Olvidar cuenta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="116"/>
      <source>Switch to your browser and log into your imgur.com account. Then paste the displayed PIN in the field below. Click on the button above again to reopen the website.</source>
      <translation type="unfinished">Switch to your browser and log into your imgur.com account. Then paste the displayed PIN in the field below. Click on the button above again to reopen the website.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="159"/>
      <source>An error occured:</source>
      <translation type="unfinished">An error occured:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="62"/>
      <source>The background is the area in the back (no surprise there) behind any image that is currently being viewed. By default, PhotoQt is partially transparent with a dark overlay. This is only possible, though, whenever a compositor is available. On some platforms, PhotoQt can fake a transparent background with screenshots taken at startup. Another option is to show a background image (also with a dark overlay) in the background.</source>
      <translation type="unfinished">The background is the area in the back (no surprise there) behind any image that is currently being viewed. By default, PhotoQt is partially transparent with a dark overlay. This is only possible, though, whenever a compositor is available. On some platforms, PhotoQt can fake a transparent background with screenshots taken at startup. Another option is to show a background image (also with a dark overlay) in the background.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="75"/>
      <source>real transparency</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>transparencia real</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="84"/>
      <source>fake transparency</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>transparencia falsa</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="92"/>
      <source>solid background color</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>color de fondo sólido</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="100"/>
      <source>custom background image</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>imagen de fondo personalizada</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="128"/>
      <source>background image</source>
      <translation>imagen de fondo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="144"/>
      <source>Click to select an image</source>
      <extracomment>Tooltip for a mouse area, a click on which opens a file dialog for selecting an image</extracomment>
      <translation>Pulse para seleccionar una imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="175"/>
      <source>scale to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>escalar para encajar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="182"/>
      <source>scale and crop to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>escalar y cortar para encajar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="189"/>
      <source>stretch to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>estirar para encajar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="196"/>
      <source>center image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>centrar imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="203"/>
      <source>tile image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
      <translation>repetir imagen en mosaico</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="67"/>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="219"/>
      <source>Click on empty background</source>
      <translation type="unfinished">Click on empty background</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="225"/>
      <source>The empty background area is the part of the background that is not covered by any image. A click on that area can trigger certain actions, some depending on where exactly the click occured</source>
      <translation type="unfinished">The empty background area is the part of the background that is not covered by any image. A click on that area can trigger certain actions, some depending on where exactly the click occured</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="238"/>
      <source>no action</source>
      <extracomment>what to do when the empty background is clicked</extracomment>
      <translation>ninguna acción</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="246"/>
      <source>close window</source>
      <extracomment>what to do when the empty background is clicked</extracomment>
      <translation>cerrar ventana</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="254"/>
      <source>navigate between images</source>
      <extracomment>what to do when the empty background is clicked</extracomment>
      <translation>navegar por las imágenes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="262"/>
      <source>toggle window decoration</source>
      <extracomment>what to do when the empty background is clicked</extracomment>
      <translation>alternar decoración de ventana</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="68"/>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="276"/>
      <source>Blurring elements behind other elements</source>
      <extracomment>A settings title</extracomment>
      <translation type="unfinished">Blurring elements behind other elements</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="283"/>
      <source>Whenever an element (e.g., histogram, main menu, etc.) is open, anything behind it can be blurred slightly. This reduces the contrast in the background which improves readability. Note that this requires a slightly higher amount of computations. It also does not work with anything behind PhotoQt that is not part of the window itself.</source>
      <translation type="unfinished">Whenever an element (e.g., histogram, main menu, etc.) is open, anything behind it can be blurred slightly. This reduces the contrast in the background which improves readability. Note that this requires a slightly higher amount of computations. It also does not work with anything behind PhotoQt that is not part of the window itself.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="291"/>
      <source>Blur elements in the back</source>
      <translation type="unfinished">Blur elements in the back</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="65"/>
      <source>The context menu contains actions that can be performed related to the currently viewed image. By default is it shown when doing a right click on the background, although it is possible to change that in the shortcuts category. In addition to pre-defined image functions it is also possible to add custom entries to that menu.</source>
      <translation type="unfinished">The context menu contains actions that can be performed related to the currently viewed image. By default is it shown when doing a right click on the background, although it is possible to change that in the shortcuts category. In addition to pre-defined image functions it is also possible to add custom entries to that menu.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="77"/>
      <source>No custom entries exists yet</source>
      <extracomment>The custom entries here are the custom entries in the context menu</extracomment>
      <translation type="unfinished">No custom entries exists yet</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="108"/>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="157"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="303"/>
      <source>Select</source>
      <extracomment>written on button for selecting a file from the file dialog
----------
written on button in file picker to select an existing executable file</extracomment>
      <translation>Seleccionar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="121"/>
      <source>entry name</source>
      <extracomment>The entry here refers to the text that is shown in the context menu for a custom entry</extracomment>
      <translation>nombre de entrada</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="138"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="287"/>
      <source>executable</source>
      <translation>ejecutable</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="153"/>
      <source>Select executable</source>
      <translation>Seleccionar ejecutable</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="183"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="337"/>
      <source>additional flags</source>
      <extracomment>The flags here are additional parameters that can be passed on to an executable
----------
the flags here are parameters specified on the command line</extracomment>
      <translation>parámetros adicionales</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="199"/>
      <source>quit</source>
      <extracomment>Quit PhotoQt after executing custom context menu entry. Please keep as short as possible!!</extracomment>
      <translation>salir</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="230"/>
      <source>Delete entry</source>
      <extracomment>The entry here is a custom entry in the context menu</extracomment>
      <translation>Eliminar entrada</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="246"/>
      <source>Add new entry</source>
      <extracomment>The entry here is a custom entry in the context menu</extracomment>
      <translation>Añadir entrada nueva</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="256"/>
      <source>Add system applications</source>
      <extracomment>The system applications here refers to any image related applications that can be found automatically on your system</extracomment>
      <translation>Añadir aplicaciones del sistema</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="125"/>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="287"/>
      <source>Duplicate entries in main menu</source>
      <extracomment>The entries here are the custom entries in the context menu</extracomment>
      <translation>Entradas duplicadas en menú principal</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="293"/>
      <source>The custom context menu entries can also be duplicated in the main menu. If enabled, the entries set above will be accesible in both places.</source>
      <translation>Las entradas personalizadas del menú contextual también se pueden duplicar en el menú principal. Si se activa, las entradas establecidas anteriormente serán accesibles en ambos sitios.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQContextMenu.qml" line="301"/>
      <source>Duplicate in main menu</source>
      <extracomment>Refers to duplicating the custom context menu entries in the main menu</extracomment>
      <translation>Duplicar en menú principal</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="43"/>
      <source>No action</source>
      <extracomment>Used as descriptor for a screen edge action</extracomment>
      <translation>Ninguna acción</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="47"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="56"/>
      <source>Main menu</source>
      <extracomment>Used as descriptor for a screen edge action
----------
Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Menú principal</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="74"/>
      <source>Moving the mouse cursor to the edges of the application window can trigger the visibility of some things, like the main menu, thumbnails, or metadata. Here you can choose what is triggered by which window edge. Note that the main menu is fixed to the right window edge and cannot be moved or disabled.</source>
      <translation type="unfinished">Moving the mouse cursor to the edges of the application window can trigger the visibility of some things, like the main menu, thumbnails, or metadata. Here you can choose what is triggered by which window edge. Note that the main menu is fixed to the right window edge and cannot be moved or disabled.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="176"/>
      <source>The right edge action cannot be changed</source>
      <extracomment>The action here is a screen edge action</extracomment>
      <translation type="unfinished">The right edge action cannot be changed</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="113"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="146"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="207"/>
      <source>Click to change action</source>
      <extracomment>The action here is a screen edge action</extracomment>
      <translation type="unfinished">Click to change action</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="115"/>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="228"/>
      <source>Sensitivity</source>
      <extracomment>Settings title</extracomment>
      <translation>Sensibilidad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="234"/>
      <source>The edge actions defined above are triggered whenever the mouse cursor gets close to the screen edge. The sensitivity determines how close to the edge the mouse cursor needs to be for this to happen. A value that is too sensitive might cause the edge action to sometimes be triggered accidentally.</source>
      <translation type="unfinished">The edge actions defined above are triggered whenever the mouse cursor gets close to the screen edge. The sensitivity determines how close to the edge the mouse cursor needs to be for this to happen. A value that is too sensitive might cause the edge action to sometimes be triggered accidentally.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="97"/>
      <source>PhotoQt has been translated into a number of different languages. Not all of the languages have a complete translation yet, and new translators are always needed. If you are willing and able to help, that would be greatly appreciated.</source>
      <translation type="unfinished">PhotoQt has been translated into a number of different languages. Not all of the languages have a complete translation yet, and new translators are always needed. If you are willing and able to help, that would be greatly appreciated.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="159"/>
      <source>Thank you to all who volunteered their time to help translate PhotoQt into other languages!</source>
      <translation type="unfinished">Thank you to all who volunteered their time to help translate PhotoQt into other languages!</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="169"/>
      <source>If you want to help with the translations, either by translating or by reviewing existing translations, head over to the translation page on Crowdin:</source>
      <translation type="unfinished">If you want to help with the translations, either by translating or by reviewing existing translations, head over to the translation page on Crowdin:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="188"/>
      <source>Open in browser</source>
      <translation>Abrir en navegador</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQLanguage.qml" line="197"/>
      <source>Copy to clipboard</source>
      <translation>Copiar en portapapeles</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="50"/>
      <source>File dialog</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Cuadro de diálogo de archivos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="52"/>
      <source>Map explorer</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Map explorer</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="54"/>
      <source>Settings manager</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Gestor de configuración</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="60"/>
      <source>Histogram</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Histograma</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="62"/>
      <source>Map (current image)</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Map (current image)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="64"/>
      <source>Scale</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Escala</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="66"/>
      <source>Slideshow setup</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Configuración de pase de diapositivas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="68"/>
      <source>Slideshow controls</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Controles de pase de diapositivas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="70"/>
      <source>Rename file</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Cambiar nombre de archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="72"/>
      <source>Delete file</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Eliminar archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="74"/>
      <source>Export file</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Exportar archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="76"/>
      <source>About</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Acerca de</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="78"/>
      <source>Imgur</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Imgur</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="80"/>
      <source>Wallpaper</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Fondo de escritorio</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQMainCategory.qml" line="175"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="82"/>
      <source>Filter</source>
      <extracomment>Noun, not a verb. Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Filtro</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="84"/>
      <source>Advanced image sort</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Ordenamiento avanzado de imágenes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="86"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Transmisión (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="116"/>
      <source>Almost all of the elements for displaying information or performing actions can either be shown integrated into the main window or shown popped out in their own window. Most of them can also be popped out/in through a small button at the top left corner of each elements.</source>
      <translation type="unfinished">Almost all of the elements for displaying information or performing actions can either be shown integrated into the main window or shown popped out in their own window. Most of them can also be popped out/in through a small button at the top left corner of each elements.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="200"/>
      <source>Two of the elements can be kept open after they performed their action. These two elements are the file dialog and the map explorer (if available). Both of them can be kept open after a file is selected and loaded in the main view allowing for quick and convenient browsing of images.</source>
      <translation type="unfinished">Two of the elements can be kept open after they performed their action. These two elements are the file dialog and the map explorer (if available). Both of them can be kept open after a file is selected and loaded in the main view allowing for quick and convenient browsing of images.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="87"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="194"/>
      <source>Keep popouts open</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Keep popouts open</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="227"/>
      <source>keep file dialog open</source>
      <translation type="unfinished">keep file dialog open</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="268"/>
      <source>keep map explorer open</source>
      <translation type="unfinished">keep map explorer open</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="88"/>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="299"/>
      <source>Pop out when window is small</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Pop out when window is small</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="305"/>
      <source>Some elements might not be as usable or function well when the window is too small. Thus it is possible to force such elements to be popped out automatically whenever the application window is too small.</source>
      <translation type="unfinished">Some elements might not be as usable or function well when the window is too small. Thus it is possible to force such elements to be popped out automatically whenever the application window is too small.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="312"/>
      <source>pop out when application window is small</source>
      <translation type="unfinished">pop out when application window is small</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="129"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="130"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="46"/>
      <source>Status info</source>
      <extracomment>A settings subcategory
----------
Settings title</extracomment>
      <translation>Información de estado</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="52"/>
      <source>The status information refers to the set of information shown in the top left corner of the screen. This typically includes the filename of the currently viewed image and information like the zoom level, rotation angle, etc. The exact set of information and their order can be adjusted as desired.</source>
      <translation>La información de estado se refiere al conjunto de datos que se muestra en la esquina superior izquierda de la pantalla. Esto típicamente incluye el nombre del archivo de la imagen actualmente vista e información como el nivel de zum, ángulo de giro, etc. El conjunto exacto de información y su orden pueden ajustarse como desee.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="59"/>
      <source>show status information</source>
      <translation>mostrar información de estado</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="93"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="228"/>
      <source>counter</source>
      <extracomment>Please keep short! The counter shows where we are in the folder.</extracomment>
      <translation>contador</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="95"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="230"/>
      <source>filename</source>
      <extracomment>Please keep short!</extracomment>
      <translation>nombre de archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="97"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="232"/>
      <source>filepath</source>
      <extracomment>Please keep short!</extracomment>
      <translation>ruta de archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="99"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="234"/>
      <source>resolution</source>
      <extracomment>Please keep short! This is the image resolution.</extracomment>
      <translation>resolución</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="101"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="236"/>
      <source>zoom</source>
      <extracomment>Please keep short! This is the current zoom level.</extracomment>
      <translation>zum</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="105"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="240"/>
      <source>filesize</source>
      <extracomment>Please keep short! This is the filesize of the current image.</extracomment>
      <translation>tamaño de archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="254"/>
      <source>add</source>
      <extracomment>This is written on a button that is used to add a selected block to the status info section.</extracomment>
      <translation>añadir</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="131"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="269"/>
      <source>Font size</source>
      <extracomment>Settings title, the font sized of the status information</extracomment>
      <translation>Tamaño de letra</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="275"/>
      <source>The size of the status info is determined by the font size of the text.</source>
      <translation>El tamaño de la información de estado lo determina el tamaño de letra del texto.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="132"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="147"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="311"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="180"/>
      <source>Hide automatically</source>
      <extracomment>Settings title</extracomment>
      <translation>Ocultar automáticamente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="317"/>
      <source>The status info can either be shown at all times, or it can be hidden automatically based on different criteria. It can either be hidden unless the mouse cursor is near the top edge of the screen or until the mouse cursor is moved anywhere. After a specified timeout it will then hide again. In addition to these criteria, it can also be shown shortly whenever the image changes.</source>
      <translation>La información de estado puede mostrarse incondicionalmente u ocultarse automáticamente en función de varios criterios. Puede ocultarse ya sea cuando el cursor se aleje del borde superior de la pantalla o hasta que el puntero se mueva a cualquier sitio. Luego de un tiempo de espera especificado, se ocultará nuevamente. Además de estos criterios, también se puede definir mostrarla brevemente cuando cambie la imagen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="328"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="197"/>
      <source>keep always visible</source>
      <extracomment>visibility status of the status information
----------
visibility status of the window buttons</extracomment>
      <translation>mantener siempre visible</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="336"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="205"/>
      <source>only show with any cursor move</source>
      <extracomment>visibility status of the status information
----------
visibility status of the window buttons</extracomment>
      <translation>mostrar solo al mover el puntero</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="344"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="213"/>
      <source>only show when cursor near top edge</source>
      <extracomment>visibility status of the status information
----------
visibility status of the window buttons</extracomment>
      <translation type="unfinished">only show when cursor near top edge</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="355"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="224"/>
      <source>hide again after timeout:</source>
      <extracomment>the status information can be hidden automatically after a set timeout
----------
the window buttons can be hidden automatically after a set timeout</extracomment>
      <translation type="unfinished">hide again after timeout:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="392"/>
      <source>also show when image changes</source>
      <extracomment>Refers to the status information&apos;s auto-hide feature, this is an additional case it can be shown</extracomment>
      <translation type="unfinished">also show when image changes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="133"/>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="404"/>
      <source>Window management</source>
      <extracomment>Settings title</extracomment>
      <translation>Gestión de ventanas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="410"/>
      <source>By default it is possible to drag the status info around as desired. However, it is also possible to use the status info for managing the window itself. When enabled, dragging the status info will drag the window around, and double clicking the status info will toggle the maximized status of the window.</source>
      <translation type="unfinished">By default it is possible to drag the status info around as desired. However, it is also possible to use the status info for managing the window itself. When enabled, dragging the status info will drag the window around, and double clicking the status info will toggle the maximized status of the window.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQStatusInfo.qml" line="417"/>
      <source>manage window through status info</source>
      <translation type="unfinished">manage window through status info</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="145"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="49"/>
      <source>Fullscreen or window mode</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Fullscreen or window mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="55"/>
      <source>There are two main states that the applicaiton window can be in. It can either be in fullscreen mode or in window mode. In fullscreen mode, PhotoQt will act more like a floating layer that allows you to quickly look at images. In window mode, PhotoQt can be used in combination with other applications. When in window mode, it can also be set to always be above any other windows, and to remember the window geometry in between sessions.</source>
      <translation type="unfinished">There are two main states that the applicaiton window can be in. It can either be in fullscreen mode or in window mode. In fullscreen mode, PhotoQt will act more like a floating layer that allows you to quickly look at images. In window mode, PhotoQt can be used in combination with other applications. When in window mode, it can also be set to always be above any other windows, and to remember the window geometry in between sessions.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="63"/>
      <source>fullscreen mode</source>
      <translation>modo a pantalla completa</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="70"/>
      <source>window mode</source>
      <translation>modo de ventana</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="81"/>
      <source>keep above other windows</source>
      <translation type="unfinished">keep above other windows</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="89"/>
      <source>remember its geometry</source>
      <extracomment>remember the geometry of PhotoQts window between sessions</extracomment>
      <translation type="unfinished">remember its geometry</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="99"/>
      <source>enable window decoration</source>
      <translation type="unfinished">enable window decoration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="146"/>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="111"/>
      <source>Window buttons</source>
      <extracomment>Settings title</extracomment>
      <translation>Botones de ventana</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="117"/>
      <source>PhotoQt can show some integrated window buttons for basic window managements both when shown in fullscreen and when in window mode. In window mode with window decoration enabled it can either hide or show buttons from its integrated set that are duplicates of buttons in the window decoration. For help with navigating through a folder, small left/right arrows for navigation and a menu button can also be added next to the window buttons.</source>
      <translation type="unfinished">PhotoQt can show some integrated window buttons for basic window managements both when shown in fullscreen and when in window mode. In window mode with window decoration enabled it can either hide or show buttons from its integrated set that are duplicates of buttons in the window decoration. For help with navigating through a folder, small left/right arrows for navigation and a menu button can also be added next to the window buttons.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="124"/>
      <source>show integrated window buttons</source>
      <translation type="unfinished">show integrated window buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="133"/>
      <source>duplicate buttons from window decoration</source>
      <translation type="unfinished">duplicate buttons from window decoration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="142"/>
      <source>add navigation buttons</source>
      <translation type="unfinished">add navigation buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/interface/PQWindow.qml" line="186"/>
      <source>The window buttons can either be shown at all times, or they can be hidden automatically based on different criteria. They can either be hidden unless the mouse cursor is near the top edge of the screen or until the mouse cursor is moved anywhere. After a specified timeout they will then hide again.</source>
      <translation type="unfinished">The window buttons can either be shown at all times, or they can be hidden automatically based on different criteria. They can either be hidden unless the mouse cursor is near the top edge of the screen or until the mouse cursor is moved anywhere. After a specified timeout they will then hide again.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="286"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="41"/>
      <source>Auto Rotation</source>
      <extracomment>Settings title</extracomment>
      <translation>Giro automático</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="47"/>
      <source>When an image is taken with the camera turned on its side, some cameras store that rotation in the metadata. PhotoQt can use that information to display an image the way it was meant to be viewed. Disabling this will load all photos without any rotation applied by default.</source>
      <translation type="unfinished">When an image is taken with the camera turned on its side, some cameras store that rotation in the metadata. PhotoQt can use that information to display an image the way it was meant to be viewed. Disabling this will load all photos without any rotation applied by default.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="54"/>
      <source>Apply default rotation automatically</source>
      <translation type="unfinished">Apply default rotation automatically</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="287"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="66"/>
      <source>GPS map</source>
      <extracomment>Settings title</extracomment>
      <translation>Mapa GPS</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="72"/>
      <source>Some cameras store the location of where the image was taken in the metadata of its images. PhotoQt can use that information in multiple ways. It can show a floating embedded map with a pin on that location, and it can show the GPS coordinates in the metadata element. In the latter case, a click on the GPS coordinates will open the location in an online map service, the choice of which can be set here.</source>
      <translation type="unfinished">Some cameras store the location of where the image was taken in the metadata of its images. PhotoQt can use that information in multiple ways. It can show a floating embedded map with a pin on that location, and it can show the GPS coordinates in the metadata element. In the latter case, a click on the GPS coordinates will open the location in an online map service, the choice of which can be set here.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="288"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="108"/>
      <source>Floating element</source>
      <extracomment>Settings title</extracomment>
      <translation>Elemento flotante</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="114"/>
      <source>The metadata element can be show in two different ways. It can either be shown hidden behind one of the screen edges and shown when the cursor is close to said edge. Or it can be shown as floating element that can be triggered by shortcut and stays visible until manually hidden.</source>
      <translation type="unfinished">The metadata element can be show in two different ways. It can either be shown hidden behind one of the screen edges and shown when the cursor is close to said edge. Or it can be shown as floating element that can be triggered by shortcut and stays visible until manually hidden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="125"/>
      <source>hide behind screen edge</source>
      <translation type="unfinished">hide behind screen edge</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQBehavior.qml" line="132"/>
      <source>use floating element</source>
      <translation type="unfinished">use floating element</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="295"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="45"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="58"/>
      <source>Show face tags</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Show face tags</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="51"/>
      <source>PhotoQt can read face tags stored in its metadata. It offers a great deal of flexibility in how and when the face tags are shown. It is also possible to remove and add face tags using the face tagger interface (accessible through the context menu or by shortcut).</source>
      <translation>PhotoQt puede leer etiquetas de rostros almacenadas en sus metadatos. Ofrece mucha flexibilidad en cómo y cuándo se muestran las etiquetas de rostro. También es posible eliminar y añadir etiquetas de rostro usando la interfaz de etiquetas (accesible a través del menú contextual o por atajo).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="76"/>
      <source>It is possible to adjust the border shown around tagged faces and the font size used for the displayed name. For the border, not only the width but also the color can be specified.</source>
      <translation type="unfinished">It is possible to adjust the border shown around tagged faces and the font size used for the displayed name. For the border, not only the width but also the color can be specified.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="83"/>
      <source>font size:</source>
      <translation>tamaño de fuente:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="110"/>
      <source>Show border around face tags</source>
      <translation type="unfinished">Show border around face tags</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="154"/>
      <source>red</source>
      <translation>rojo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="155"/>
      <source>green</source>
      <translation>verde</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="156"/>
      <source>blue</source>
      <translation>azul</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="157"/>
      <source>alpha</source>
      <translation>alfa</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="235"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="297"/>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="186"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="207"/>
      <source>Visibility</source>
      <extracomment>Settings title</extracomment>
      <translation>Visibilidad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="192"/>
      <source>The face tags can be shown under different conditions. It is possible to always show all face tags, to show all face tags when the mouse cursor is moving across the image, or to show an individual face tag only when the mouse cursor is above a face tags.</source>
      <translation type="unfinished">The face tags can be shown under different conditions. It is possible to always show all face tags, to show all face tags when the mouse cursor is moving across the image, or to show an individual face tag only when the mouse cursor is above a face tags.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="204"/>
      <source>always show all</source>
      <extracomment>used as in: always show all face tags</extracomment>
      <translation>siempre mostrar todas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="212"/>
      <source>show one on hover</source>
      <extracomment>used as in: show one face tag on hover</extracomment>
      <translation>mostrar una al pasar el ratón</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQFaceTags.qml" line="220"/>
      <source>show all on hover</source>
      <extracomment>used as in: show one face tag on hover</extracomment>
      <translation>mostrar todas al pasar el ratón</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="48"/>
      <source>file name</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>nombre del archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="50"/>
      <source>file type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>tipo del archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="52"/>
      <source>file size</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>tamaño de archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="54"/>
      <source>image #/#</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>imagen #/#</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="56"/>
      <source>dimensions</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>dimensiones</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="58"/>
      <source>copyright</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>copyright</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="60"/>
      <source>exposure time</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>tiempo de exposición</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="62"/>
      <source>flash</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>flash</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="64"/>
      <source>focal length</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>distancia focal</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="66"/>
      <source>f-number</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>número f</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="68"/>
      <source>GPS position</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>posición del GPS</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="71"/>
      <source>keywords</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>palabras clave</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="73"/>
      <source>light source</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>fuente de luz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="75"/>
      <source>location</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>ubicación</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="77"/>
      <source>make</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">make</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="79"/>
      <source>model</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>modelo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="81"/>
      <source>scene type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>tipo de escena</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="83"/>
      <source>software</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>software</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="85"/>
      <source>time photo was taken</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>hora en que fue tomada la foto</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="110"/>
      <source>Whenever an image is loaded PhotoQt tries to find as much metadata about the image as it can. The found information is then displayed in the metadata element that can be accesses either through one of the screen edges or as floating element. Since not all information might be wanted by everyone, individual information labels can be disabled.</source>
      <translation type="unfinished">Whenever an image is loaded PhotoQt tries to find as much metadata about the image as it can. The found information is then displayed in the metadata element that can be accesses either through one of the screen edges or as floating element. Since not all information might be wanted by everyone, individual information labels can be disabled.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="189"/>
      <source>Check all</source>
      <extracomment>used as in: check all checkboxes</extracomment>
      <translation>Marcar todos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/metadata/PQLabels.qml" line="200"/>
      <source>Check none</source>
      <extracomment>used as in: check none of the checkboxes</extracomment>
      <translation>Desmarcar todos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="313"/>
      <location filename="../qml/settingsmanager/settings/session/PQInstance.qml" line="37"/>
      <source>Single instance</source>
      <extracomment>Settings title</extracomment>
      <translation>Instancia única</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQInstance.qml" line="43"/>
      <source>PhotoQt can either run in single-instance mode or allow multiple instances to run at the same time. The former has the advantage that it is possible to interact with a running instance of PhotoQt through the command line (in fact, this is a requirement for that to work). The latter allows, for example, for the comparison of multiple images side by side.</source>
      <translation>PhotoQt puede ejecutarse en modo de uno solo proceso o permitir que múltiples procesos se ejecuten al mismo tiempo. El primero tiene la ventaja de que es posible interactuar con una instancia en ejecución de PhotoQt a través de la línea de órdenes (de hecho, es un requisito para que funcione). Esto último permite, por ejemplo, la comparación de varias imágenes lado a lado.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQInstance.qml" line="54"/>
      <source>run a single instance only</source>
      <translation type="unfinished">run a single instance only</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQInstance.qml" line="60"/>
      <source>allow multiple instances</source>
      <translation type="unfinished">allow multiple instances</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="318"/>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="38"/>
      <source>Reopen last image</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Reopen last image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="44"/>
      <source>When PhotoQt is started normally, by default an empty window is shown with the prompt to open an image from the file dialog. Alternatively it is also possible to reopen the image that was last loaded in the previous session.</source>
      <translation type="unfinished">When PhotoQt is started normally, by default an empty window is shown with the prompt to open an image from the file dialog. Alternatively it is also possible to reopen the image that was last loaded in the previous session.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="55"/>
      <source>start with blank session</source>
      <translation type="unfinished">start with blank session</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="61"/>
      <source>reopen last used image</source>
      <translation type="unfinished">reopen last used image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="319"/>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="75"/>
      <source>Remember changes</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Remember changes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="81"/>
      <source>Once an image has been loaded it can be manipulated freely by zooming, rotating, or mirroring the image. Once another image is loaded any such changes are forgotten. If preferred, it is possible for PhotoQt to remember any such manipulations per session. Note that once PhotoQt is closed these changes will be forgotten in any case.</source>
      <translation type="unfinished">Once an image has been loaded it can be manipulated freely by zooming, rotating, or mirroring the image. Once another image is loaded any such changes are forgotten. If preferred, it is possible for PhotoQt to remember any such manipulations per session. Note that once PhotoQt is closed these changes will be forgotten in any case.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="92"/>
      <source>forget changes when other image loaded</source>
      <translation type="unfinished">forget changes when other image loaded</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQRemember.qml" line="98"/>
      <source>remember changes per session</source>
      <translation type="unfinished">remember changes per session</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="325"/>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="41"/>
      <source>Tray Icon</source>
      <extracomment>Settings title</extracomment>
      <translation>Icono en área de notificaciones</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="47"/>
      <source>PhotoQt can show a small icon in the system tray. The tray icon provides additional ways to control and interact with the application. It is also possible to hide PhotoQt to the system tray instead of closing. By default a colored version of the tray icon is used, but it is also possible to use a monochrome version.</source>
      <translation type="unfinished">PhotoQt can show a small icon in the system tray. The tray icon provides additional ways to control and interact with the application. It is also possible to hide PhotoQt to the system tray instead of closing. By default a colored version of the tray icon is used, but it is also possible to use a monochrome version.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="58"/>
      <source>Show tray icon</source>
      <translation>Mostrar icono en área de notificaciones</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="66"/>
      <source>monochrome icon</source>
      <translation>icono monocromático</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="74"/>
      <source>hide to tray icon instead of closing</source>
      <translation type="unfinished">hide to tray icon instead of closing</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="326"/>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="88"/>
      <source>Reset when hiding</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Reset when hiding</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="94"/>
      <source>When hiding PhotoQt in the system tray, it is possible to reset PhotoQt to its initial state, thus freeing most of the memory tied up by caching. Note that this will also unload any loaded folder and image.</source>
      <translation type="unfinished">When hiding PhotoQt in the system tray, it is possible to reset PhotoQt to its initial state, thus freeing most of the memory tied up by caching. Note that this will also unload any loaded folder and image.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/session/PQTrayIcon.qml" line="102"/>
      <source>reset session when hiding</source>
      <translation type="unfinished">reset session when hiding</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="369"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="43"/>
      <source>Move image with mouse</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Move image with mouse</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="49"/>
      <source>PhotoQt can use both the left button of the mouse and the mouse wheel to move the image around. In that case, however, these actions are not available for shortcuts anymore, except when combined with one or more modifier buttons (Alt, Ctrl, etc.).</source>
      <translation type="unfinished">PhotoQt can use both the left button of the mouse and the mouse wheel to move the image around. In that case, however, these actions are not available for shortcuts anymore, except when combined with one or more modifier buttons (Alt, Ctrl, etc.).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="60"/>
      <source>move image with left button</source>
      <translation type="unfinished">move image with left button</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="67"/>
      <source>move image with mouse wheel</source>
      <translation type="unfinished">move image with mouse wheel</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="151"/>
      <source>very sensitive</source>
      <extracomment>used as in: very sensitive mouse wheel</extracomment>
      <translation>muy sensible</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="161"/>
      <source>not sensitive</source>
      <extracomment>used as in: not at all sensitive mouse wheel</extracomment>
      <translation>no sensible</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="185"/>
      <source>hide cursor after timeout</source>
      <translation>ocultar cursor tras un tiempo de espera</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="370"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="81"/>
      <source>Double click</source>
      <extracomment>Settings title</extracomment>
      <translation>Doble clic</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="87"/>
      <source>A double click is defined as two clicks in quick succession. This means that PhotoQt will have to wait a certain amount of time to see if there is a second click before acting on a single click. Thus, the threshold (specified in milliseconds) for detecting double clicks should be as small as possible while still allowing for reliable detection of double clicks. Setting this value to zero disables double clicks and treats them as two distinct single clicks.</source>
      <translation type="unfinished">A double click is defined as two clicks in quick succession. This means that PhotoQt will have to wait a certain amount of time to see if there is a second click before acting on a single click. Thus, the threshold (specified in milliseconds) for detecting double clicks should be as small as possible while still allowing for reliable detection of double clicks. Setting this value to zero disables double clicks and treats them as two distinct single clicks.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="371"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="132"/>
      <source>Mouse wheel</source>
      <extracomment>Settings title</extracomment>
      <translation>Rueda del ratón</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="138"/>
      <source>Depending on any particular hardware, the mouse wheel moves either a set amount each time it is moved, or relative to how long/fast it is moved. The sensitivity allows to account for very sensitive hardware to decrease the likelihood of accidental/multiple triggers caused by wheel movement.</source>
      <translation type="unfinished">Depending on any particular hardware, the mouse wheel moves either a set amount each time it is moved, or relative to how long/fast it is moved. The sensitivity allows to account for very sensitive hardware to decrease the likelihood of accidental/multiple triggers caused by wheel movement.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="372"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="172"/>
      <source>Hide mouse cursor</source>
      <extracomment>Settings title</extracomment>
      <translation>Ocultar cursor del ratón</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="178"/>
      <source>Whenever an image is viewed and mouse cursor rests on the image it is possible to hide the mouse cursor after a set timeout. This way the cursor does not get in the way of actually viewing an image.</source>
      <translation type="unfinished">Whenever an image is viewed and mouse cursor rests on the image it is possible to hide the mouse cursor after a set timeout. This way the cursor does not get in the way of actually viewing an image.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="68"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="268"/>
      <source>Viewing images</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation type="unfinished">Viewing images</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="70"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="269"/>
      <source>Current image</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation type="unfinished">Current image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="72"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="270"/>
      <source>Current folder</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation type="unfinished">Current folder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="76"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="272"/>
      <source>Other</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation type="unfinished">Other</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="78"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="273"/>
      <source>External</source>
      <extracomment>This is a shortcut category</extracomment>
      <translation type="unfinished">External</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="82"/>
      <source>These actions affect the behavior of PhotoQt when viewing images. They include actions for navigating between images and manipulating the current image (zoom, flip, rotation) amongst others.</source>
      <translation type="unfinished">These actions affect the behavior of PhotoQt when viewing images. They include actions for navigating between images and manipulating the current image (zoom, flip, rotation) amongst others.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="83"/>
      <source>These actions are certain things that can be done with the currently viewed image. They typically do not affect any of the other images.</source>
      <translation type="unfinished">These actions are certain things that can be done with the currently viewed image. They typically do not affect any of the other images.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="84"/>
      <source>These actions affect the currently loaded folder as a whole and not just single images.</source>
      <translation type="unfinished">These actions affect the currently loaded folder as a whole and not just single images.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="85"/>
      <source>These actions affect the status and behavior of various interface elements regardless of the status of any possibly loaded image.</source>
      <translation type="unfinished">These actions affect the status and behavior of various interface elements regardless of the status of any possibly loaded image.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="86"/>
      <source>These actions do not fit into any other category.</source>
      <translation type="unfinished">These actions do not fit into any other category.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="87"/>
      <source>Here any external executable can be set as shortcut action. The button with the three dots can be used to select an executable with a file dialog.</source>
      <translation type="unfinished">Here any external executable can be set as shortcut action. The button with the three dots can be used to select an executable with a file dialog.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="111"/>
      <source>Shortcut actions</source>
      <translation type="unfinished">Shortcut actions</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="275"/>
      <source>Select an executable</source>
      <translation type="unfinished">Select an executable</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="293"/>
      <source>Click here to select an executable.</source>
      <translation type="unfinished">Click here to select an executable.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="327"/>
      <source>Additional flags and options to pass on:</source>
      <translation type="unfinished">Additional flags and options to pass on:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="342"/>
      <source>Note that relative file paths are not supported, however, you can use the following placeholders:</source>
      <translation type="unfinished">Note that relative file paths are not supported, however, you can use the following placeholders:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="343"/>
      <source>filename including path</source>
      <translation>nombre de archivo incluyendo ruta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="344"/>
      <source>filename without path</source>
      <translation>nombre de archivo sin la ruta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="345"/>
      <source>directory containing file</source>
      <translation>directorio que contiene el archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="346"/>
      <source>If you type out a path, make sure to escape spaces accordingly by prepending a backslash:</source>
      <translation>Si escribes una ruta, asegúrate de escapar los espacios de forma correcta anteponiendo una diagonal invertida:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="358"/>
      <source>quit after calling executable</source>
      <translation>salir después de llamar al ejecutable</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="367"/>
      <source>Save external action</source>
      <translation>Guardar acción externa</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="125"/>
      <source>Add New Shortcut</source>
      <translation>Añadir atajo nuevo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="125"/>
      <source>Set new shortcut</source>
      <translation>Establecer atajo nuevo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="147"/>
      <source>Perform a mouse gesture here or press any key combo</source>
      <translation>Realiza un gesto del ratón aquí o presiona cualquier combinación de teclas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="179"/>
      <source>The left button is used for moving the main image around.</source>
      <translation>El botón izquierdo se usa para mover la imagen principal.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="180"/>
      <source>It can be used as part of a shortcut only when combined with modifier buttons (Alt, Ctrl, etc.).</source>
      <translation>Puede usarse como parte de un atajo solo cuando se combina con teclas modificadoras (Alt, Ctrl, etc.).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="31"/>
      <source>Next image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Imagen siguiente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="33"/>
      <source>Previous image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Imagen anterior</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="35"/>
      <source>Go to first image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ir a la primera imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="37"/>
      <source>Go to last image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ir a la última imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="39"/>
      <source>Zoom In</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Acercar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="41"/>
      <source>Zoom Out</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Alejar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="43"/>
      <source>Zoom to Actual Size</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ajustar al tamaño real</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="45"/>
      <source>Reset Zoom</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Restablecer acercamiento</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="47"/>
      <source>Rotate Right</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Girar a la derecha</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="49"/>
      <source>Rotate Left</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Girar a la izquierda</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="51"/>
      <source>Reset Rotation</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Restablecer rotación</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="53"/>
      <source>Flip Horizontally</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Voltear horizontalmente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="55"/>
      <source>Flip Vertically</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Voltear verticalmente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="57"/>
      <source>Load a random image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Cargar una imagen al azar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="59"/>
      <source>Hide/Show face tags (stored in metadata)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ocultar/mostrar etiquetas de rostros (almacenadas en metadatos)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="61"/>
      <source>Toggle: Fit in window</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Alternar: Ajustar en ventana</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="63"/>
      <source>Toggle: Show always actual size by default</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Conmutar: mostrar siempre el tamaño real por omisión</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="65"/>
      <source>Stream content to Chromecast device</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Transmitir contenido a dispositivo Chromecast</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="67"/>
      <source>Move view left</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mover vista a la izquierda</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="69"/>
      <source>Move view right</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mover vista a la derecha</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="71"/>
      <source>Move view up</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mover vista arriba</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="73"/>
      <source>Move view down</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mover vista abajo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="75"/>
      <source>Go to left edge of image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ir al borde izquierdo de la imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="77"/>
      <source>Go to right edge of image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ir al borde derecho de la imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="79"/>
      <source>Go to top edge of image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ir al borde superior de la imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="81"/>
      <source>Go to bottom edge of image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ir al borde inferior de la imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="87"/>
      <source>Show Histogram</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mostrar histograma</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="89"/>
      <source>Show Image on Map</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mostrar imagen en mapa</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="91"/>
      <source>Enter viewer mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Entrar en modo de visualización</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="93"/>
      <source>Scale Image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Escalar imagen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="95"/>
      <source>Rename File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Renombrar archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="97"/>
      <source>Delete File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Eliminar archivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="99"/>
      <source>Delete File (without confirmation)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Eliminar archivo (sin confirmación)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="101"/>
      <source>Copy File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Copy File to a New Location</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="103"/>
      <source>Move File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Move File to a New Location</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="105"/>
      <source>Copy Image to Clipboard</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Copiar imagen al portapapeles</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="107"/>
      <source>Save image in another format</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Guardar imagen en otro formato</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="109"/>
      <source>Print current photo</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Imprimir foto actual</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="111"/>
      <source>Set as Wallpaper</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Establecer como fondo de escritorio</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="113"/>
      <source>Upload to imgur.com (anonymously)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Upload to imgur.com (anonymously)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="115"/>
      <source>Upload to imgur.com user account</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Upload to imgur.com user account</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="117"/>
      <source>Play/Pause animation/video</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Play/Pause animation/video</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="119"/>
      <source>Start tagging faces</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Start tagging faces</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="125"/>
      <source>Open file (browse images)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Open file (browse images)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="127"/>
      <source>Show map explorer</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show map explorer</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="129"/>
      <source>Filter images in folder</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Filter images in folder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="131"/>
      <source>Advanced image sort (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Advanced image sort (Setup)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="133"/>
      <source>Advanced image sort (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Advanced image sort (Quickstart)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="135"/>
      <source>Start Slideshow (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Iniciar presentación (Configurar)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="137"/>
      <source>Start Slideshow (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Start Slideshow (Quickstart)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="143"/>
      <source>Show Context Menu</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show Context Menu</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="145"/>
      <source>Hide/Show main menu</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Hide/Show main menu</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="147"/>
      <source>Hide/Show metadata</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Hide/Show metadata</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="149"/>
      <source>Hide/Show thumbnails</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Hide/Show thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="151"/>
      <source>Show floating navigation buttons</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show floating navigation buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="153"/>
      <source>Toggle fullscreen mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Toggle fullscreen mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="155"/>
      <source>Close window (hides to system tray if enabled)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Close window (hides to system tray if enabled)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="157"/>
      <source>Quit PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Salir de PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="164"/>
      <source>Show Settings</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mostrar ajustes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="166"/>
      <source>About PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Acerca de PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="168"/>
      <source>Show log/debug messages</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show log/debug messages</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="170"/>
      <source>Reset current session</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Restablecer sesión actual</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="172"/>
      <source>Reset current session and hide window</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Restablecer sesión actual y ocultar ventana</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="210"/>
      <source>Shortcuts are grouped by key combination. Multiple actions can be set for each group of key combinations, with the option of cycling through them one by one, or executing all of them at the same time. When cycling through them one by one, a timeout can be set after which the cycle will be reset to the beginning. Any group that has no key combinations set will be deleted when saving all changes.</source>
      <translation type="unfinished">Shortcuts are grouped by key combination. Multiple actions can be set for each group of key combinations, with the option of cycling through them one by one, or executing all of them at the same time. When cycling through them one by one, a timeout can be set after which the cycle will be reset to the beginning. Any group that has no key combinations set will be deleted when saving all changes.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="215"/>
      <source>Add new shortcuts group</source>
      <translation type="unfinished">Add new shortcuts group</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="235"/>
      <source>The shortcuts can be filtered by either the key combinations, shortcut actions, category, or all three. For the string search, PhotoQt will by default check if any action/key combination includes whatever string is entered. Adding a dollar sign ($) at the start or end of the search term forces a match to be either at the start or the end of a key combination or action.</source>
      <translation type="unfinished">The shortcuts can be filtered by either the key combinations, shortcut actions, category, or all three. For the string search, PhotoQt will by default check if any action/key combination includes whatever string is entered. Adding a dollar sign ($) at the start or end of the search term forces a match to be either at the start or the end of a key combination or action.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="247"/>
      <source>Filter key combinations</source>
      <translation type="unfinished">Filter key combinations</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="257"/>
      <source>Filter shortcut actions</source>
      <translation type="unfinished">Filter shortcut actions</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="267"/>
      <source>Show all categories</source>
      <translation>Mostrar todas las categorías</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="268"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="269"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="270"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="271"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="272"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="273"/>
      <source>Category:</source>
      <translation type="unfinished">Category:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="371"/>
      <source>no key combination set</source>
      <translation type="unfinished">no key combination set</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="457"/>
      <source>Click to change key combination</source>
      <translation type="unfinished">Click to change key combination</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="468"/>
      <source>Click to delete key combination</source>
      <translation type="unfinished">Click to delete key combination</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="502"/>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="780"/>
      <source>ADD</source>
      <extracomment>Written on small button, used as in: add new key combination. Please keep short!
----------
Written on small button, used as in: add new shortcut action. Please keep short!</extracomment>
      <translation type="unfinished">ADD</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="514"/>
      <source>Click to add new key combination</source>
      <translation type="unfinished">Click to add new key combination</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="570"/>
      <source>The new shortcut was in use by another shortcuts group. It has been reassigned to this group.</source>
      <translation type="unfinished">The new shortcut was in use by another shortcuts group. It has been reassigned to this group.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="581"/>
      <source>Undo reassignment</source>
      <translation type="unfinished">Undo reassignment</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="656"/>
      <source>no action selected</source>
      <extracomment>The action here is a shortcut action</extracomment>
      <translation type="unfinished">no action selected</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="704"/>
      <source>unknown:</source>
      <extracomment>The unknown here refers to an unknown internal action that was set as shortcut</extracomment>
      <translation>desconocido:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="706"/>
      <source>external</source>
      <extracomment>This is an identifier in the shortcuts settings used to identify an external shortcut.</extracomment>
      <translation type="unfinished">external</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="709"/>
      <source>quit after</source>
      <extracomment>This is used for listing external commands for shortcuts, showing if the quit after checkbox has been checked</extracomment>
      <translation>salir después de</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="719"/>
      <source>Click to change shortcut action</source>
      <translation>Clic para cambiar acción de atajo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="745"/>
      <source>Click to delete shortcut action</source>
      <translation>Clic para borrar acción de atajo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="792"/>
      <source>Click to add new action</source>
      <extracomment>The action here is a shortcut action</extracomment>
      <translation>Clic para agregar una nueva acción</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="841"/>
      <source>cycle through actions one by one</source>
      <extracomment>The actions here are shortcut actions</extracomment>
      <translation>ciclar a través de las acciones una por una</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="856"/>
      <source>timeout for resetting cycle:</source>
      <extracomment>The cycle here is the act of cycling through shortcut actions one by one</extracomment>
      <translation>tiempo de espera para reiniciar el ciclo:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="896"/>
      <source>run all actions at once</source>
      <extracomment>The actions here are shortcut actions</extracomment>
      <translation>ejecutar todas las acciones a la vez</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="232"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="41"/>
      <source>Spacing</source>
      <extracomment>Settings title</extracomment>
      <translation>Espaciado</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="47"/>
      <source>PhotoQt preloads thumbnails for all files in the current folder and lines them up side by side. In between each thumbnail image it is possible to add a little bit of blank space to better separate the individual images.</source>
      <translation>PhotoQt precarga miniaturas para todos los archivos de la carpeta actual y las alinea de lado a lado. Entre cada miniatura se puede añadir espacio para separar cada una de las imágenes.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="233"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="85"/>
      <source>Highlight</source>
      <extracomment>Settings title</extracomment>
      <translation>Resalte</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="91"/>
      <source>The thumbnail corresponding to the currently loaded image is highlighted so that it is easy to spot. The same highlight effect is used when hovering over a thumbnail image. The different effects can be combined as desired.</source>
      <translation type="unfinished">The thumbnail corresponding to the currently loaded image is highlighted so that it is easy to spot. The same highlight effect is used when hovering over a thumbnail image. The different effects can be combined as desired.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="102"/>
      <source>invert background color</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>invertir color de fondo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="110"/>
      <source>invert label color</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>invertir color de etiqueta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="118"/>
      <source>line below</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>línea debajo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="126"/>
      <source>magnify</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>ampliar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="135"/>
      <source>lift up</source>
      <extracomment>effect for highlighting active thumbnail</extracomment>
      <translation>levantar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="224"/>
      <source>hide when not needed</source>
      <extracomment>used as in: hide thumbnail bar when not needed</extracomment>
      <translation>ocultar cuando sea innecesario</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="234"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="182"/>
      <source>Center on active</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Center on active</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="188"/>
      <source>When switching between images PhotoQt always makes sure that the thumbnail corresponding to the currently viewed image is visible somewhere along the thumbnail bar. Additionally it is possible to tell PhotoQt to not only keep it visible but also keep it in the center of the edge.</source>
      <translation type="unfinished">When switching between images PhotoQt always makes sure that the thumbnail corresponding to the currently viewed image is visible somewhere along the thumbnail bar. Additionally it is possible to tell PhotoQt to not only keep it visible but also keep it in the center of the edge.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="195"/>
      <source>keep active thumbnail in center</source>
      <translation type="unfinished">keep active thumbnail in center</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="213"/>
      <source>The visibility of the thumbnail bar can be set depending on personal choice. The bar can either always be kept visible, it can be hidden unless the mouse cursor is close to the respective screen edge, or it can be kept visible unless the main image has been zoomed in.</source>
      <translation type="unfinished">The visibility of the thumbnail bar can be set depending on personal choice. The bar can either always be kept visible, it can be hidden unless the mouse cursor is close to the respective screen edge, or it can be kept visible unless the main image has been zoomed in.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="232"/>
      <source>always keep visible</source>
      <extracomment>used as in: always keep thumbnail bar visible</extracomment>
      <translation>mantener siempre visible</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="240"/>
      <source>hide when zoomed in</source>
      <extracomment>used as in: hide thumbnail bar when zoomed in</extracomment>
      <translation>ocultar cuando hay un acercamiento</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="216"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="44"/>
      <source>Size</source>
      <extracomment>Settings title</extracomment>
      <translation>Tamaño</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="50"/>
      <source>The thumbnails are typically hidden behind one of the screen edges. Which screen edge can be specified in the interface settings. The size of the thumbnails refers to the maximum size of each individual thumbnail image.</source>
      <translation type="unfinished">The thumbnails are typically hidden behind one of the screen edges. Which screen edge can be specified in the interface settings. The size of the thumbnails refers to the maximum size of each individual thumbnail image.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="217"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="88"/>
      <source>Scale and crop</source>
      <extracomment>Settings title</extracomment>
      <translation>Escalar y recortar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="94"/>
      <source>The thumbnail for an image can either be scaled to fit fully inside the maximum size specified above, or it can be scaled and cropped such that it takes up all available space. In the latter case some parts of a thumbnail image might be cut off. In addition, thumbnails that are smaller than the size specified above can be kept at their original size.</source>
      <translation type="unfinished">The thumbnail for an image can either be scaled to fit fully inside the maximum size specified above, or it can be scaled and cropped such that it takes up all available space. In the latter case some parts of a thumbnail image might be cut off. In addition, thumbnails that are smaller than the size specified above can be kept at their original size.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="105"/>
      <source>fit thumbnail</source>
      <translation type="unfinished">fit thumbnail</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="111"/>
      <source>scale and crop thumbnail</source>
      <translation type="unfinished">scale and crop thumbnail</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="123"/>
      <source>keep small thumbnails small</source>
      <translation type="unfinished">keep small thumbnails small</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="181"/>
      <source>On top of each thumbnail image PhotoQt can put a small text label with the filename. The font size of the filename is freely adjustable. If a filename is too long for the available space only the beginning and end of the filename will be visible. Additionally, the label of thumbnails that are neither loaded nor hovered can be shown with less opacity.</source>
      <translation type="unfinished">On top of each thumbnail image PhotoQt can put a small text label with the filename. The font size of the filename is freely adjustable. If a filename is too long for the available space only the beginning and end of the filename will be visible. Additionally, the label of thumbnails that are neither loaded nor hovered can be shown with less opacity.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="225"/>
      <source>decrease opacity for inactive thumbnails</source>
      <translation>disminuir opacidad de miniaturas inactivas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="218"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="137"/>
      <source>Icons only</source>
      <extracomment>Settings title</extracomment>
      <translation>Solo iconos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="143"/>
      <source>Instead of loading actual thumbnail images in the background, PhotoQt can instead simply show the respective icon for the filetype. This requires much fewer resources and time but is not as user friendly.</source>
      <translation type="unfinished">Instead of loading actual thumbnail images in the background, PhotoQt can instead simply show the respective icon for the filetype. This requires much fewer resources and time but is not as user friendly.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="155"/>
      <source>use actual thumbnail images</source>
      <extracomment>The word actual is used with the same meaning as: real</extracomment>
      <translation type="unfinished">use actual thumbnail images</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="161"/>
      <source>use filetype icons</source>
      <translation type="unfinished">use filetype icons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="219"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="175"/>
      <source>Label</source>
      <extracomment>Settings title</extracomment>
      <translation>Etiqueta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="188"/>
      <source>show filename label</source>
      <translation type="unfinished">show filename label</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="220"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="236"/>
      <source>Tooltip</source>
      <extracomment>Settings title</extracomment>
      <translation>Descripción emergente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="242"/>
      <source>PhotoQt can show additional information about an image in the form of a tooltip that is shown when the mouse cursor hovers above a thumbnail. The displayed information includes the full file name, file size, and file type.</source>
      <translation type="unfinished">PhotoQt can show additional information about an image in the form of a tooltip that is shown when the mouse cursor hovers above a thumbnail. The displayed information includes the full file name, file size, and file type.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQImage.qml" line="249"/>
      <source>show tooltips</source>
      <translation>mostrar descripciones emergentes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="61"/>
      <source>PhotoQt can cache thumbnails so that each subsequent time they can be generated near instantaneously. PhotoQt implements the standard for thumbnails defined by freedesktop.org. On Windows it can also load (but not write) existing thumbnails from the thumbnail cache built into Windows.</source>
      <translation type="unfinished">PhotoQt can cache thumbnails so that each subsequent time they can be generated near instantaneously. PhotoQt implements the standard for thumbnails defined by freedesktop.org. On Windows it can also load (but not write) existing thumbnails from the thumbnail cache built into Windows.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="68"/>
      <source>enable cache</source>
      <translation>activar antememoria</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="245"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="80"/>
      <source>Exclude folders</source>
      <extracomment>Settings title</extracomment>
      <translation>Excluir carpetas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="44"/>
      <source>PhotoQt shows all images in the currently loaded folder as thumbnails along one of the screen edges. If you want to disable thumbnails altogether, you can do so by removing it from all screen edges in the interface settings.</source>
      <translation type="unfinished">PhotoQt shows all images in the currently loaded folder as thumbnails along one of the screen edges. If you want to disable thumbnails altogether, you can do so by removing it from all screen edges in the interface settings.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="86"/>
      <source>When an image is loaded PhotoQt preloads thumbnails for all images found in the current folder. Some cloud providers do not fully sync their files unless accessed. To avoid unnecessarily downloading large amount of files, it is possible to exclude specific directories from any sort of caching and preloading. Note that for files in these folders you will still see thumbnails consisting of filetype icons.</source>
      <translation type="unfinished">When an image is loaded PhotoQt preloads thumbnails for all images found in the current folder. Some cloud providers do not fully sync their files unless accessed. To avoid unnecessarily downloading large amount of files, it is possible to exclude specific directories from any sort of caching and preloading. Note that for files in these folders you will still see thumbnails consisting of filetype icons.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="97"/>
      <source>Cloud providers to exclude from caching:</source>
      <translation type="unfinished">Cloud providers to exclude from caching:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="141"/>
      <source>Do not cache these folders:</source>
      <translation>No prealmacenar estas carpetas:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="149"/>
      <source>One folder per line</source>
      <translation type="unfinished">One folder per line</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="161"/>
      <source>Add folder</source>
      <extracomment>Written on a button</extracomment>
      <translation>Añadir carpeta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="246"/>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="187"/>
      <source>How many threads</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">How many threads</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="193"/>
      <source>In order to speed up loading all the thumbnails in a folder PhotoQt uses multiple threads simultaneously. On more powerful systems, a larger number of threads can result in much faster loading of all the thumbnails of a folder. Too many threads, however, might make a system feel slow for a short time.</source>
      <translation type="unfinished">In order to speed up loading all the thumbnails in a folder PhotoQt uses multiple threads simultaneously. On more powerful systems, a larger number of threads can result in much faster loading of all the thumbnails of a folder. Too many threads, however, might make a system feel slow for a short time.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="222"/>
      <source>current value: %1 thread(s)</source>
      <extracomment>Important: Please do not forget the placeholder!</extracomment>
      <translation type="unfinished">current value: %1 thread(s)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSubCategory.qml" line="20"/>
      <source>subcategory</source>
      <translation>subcategoría</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSubCategory.qml" line="177"/>
      <source>Settings:</source>
      <translation>Preferencias:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQMainCategory.qml" line="17"/>
      <source>main category</source>
      <translation>categoría principal</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="38"/>
      <source>Export/Import configuration</source>
      <extracomment>Settings title</extracomment>
      <translation type="unfinished">Export/Import configuration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="44"/>
      <source>Here you can create a backup of the configuration for backup or for moving it to another install of PhotoQt. You can import a local backup below. After importing a backup file PhotoQt will automatically close as it will need to be restarted for the changes to take effect.</source>
      <translation type="unfinished">Here you can create a backup of the configuration for backup or for moving it to another install of PhotoQt. You can import a local backup below. After importing a backup file PhotoQt will automatically close as it will need to be restarted for the changes to take effect.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="56"/>
      <source>export configuration</source>
      <translation type="unfinished">export configuration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="69"/>
      <source>import configuration</source>
      <translation type="unfinished">import configuration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="72"/>
      <source>Restart required</source>
      <translation>Se requiere reiniciar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQExportImport.qml" line="73"/>
      <source>PhotoQt will now quit as it needs to be restarted for the changes to take effect.</source>
      <translation type="unfinished">PhotoQt will now quit as it needs to be restarted for the changes to take effect.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="38"/>
      <source>Reset settings and shortcuts</source>
      <extracomment>Settings title</extracomment>
      <translation>Restablecer configuración y atajos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="44"/>
      <source>Here the various configurations of PhotoQt can be reset to their defaults. Once you select one of the below categories you have a total of 5 seconds to cancel the action. After the 5 seconds are up the respective defaults will be set. This cannot be undone.</source>
      <translation type="unfinished">Here the various configurations of PhotoQt can be reset to their defaults. Once you select one of the below categories you have a total of 5 seconds to cancel the action. After the 5 seconds are up the respective defaults will be set. This cannot be undone.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="56"/>
      <source>reset settings</source>
      <translation type="unfinished">reset settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="73"/>
      <source>reset shortcuts</source>
      <translation type="unfinished">reset shortcuts</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="90"/>
      <source>reset enabled file formats</source>
      <translation type="unfinished">reset enabled file formats</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/settings/manage/PQReset.qml" line="123"/>
      <source>You can still cancel this action. Seconds remaining:</source>
      <translation>Todavía se puede cancelar esta acción. Segundos restantes:</translation>
    </message>
  </context>
  <context>
    <name>slideshow</name>
    <message>
      <location filename="../qml/actions/popout/PQSlideshowSetupPopout.qml" line="32"/>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="38"/>
      <source>Slideshow setup</source>
      <extracomment>Window title</extracomment>
      <translation>Configuración de pase de diapositivas</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="41"/>
      <source>Start slideshow</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Iniciar presentación</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="70"/>
      <source>interval</source>
      <extracomment>The interval between images in a slideshow</extracomment>
      <translation>intervalo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="106"/>
      <source>animation</source>
      <extracomment>This is referring to the in/out animation of images during a slideshow</extracomment>
      <translation>animación</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="118"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>desvanecer</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="120"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>Deslizar en el eje X</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="122"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>Deslizar en el eje Y</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="124"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>rotación</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="126"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>explosión</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="128"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>implosión</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="130"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>elegir uno al azar</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="149"/>
      <source>animation speed</source>
      <extracomment>The speed of transitioning from one image to another during slideshows</extracomment>
      <translation>velocidad de animación</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="171"/>
      <source>immediately, without animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>inmediatamente, sin animación</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="174"/>
      <source>pretty fast animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>animación bastante rápida</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="177"/>
      <source>not too fast and not too slow</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>ni muy rápido, ni muy lento</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="180"/>
      <source>very slow animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>animación muy lenta</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="189"/>
      <source>current speed</source>
      <extracomment>This refers to the currently set speed of transitioning from one image to another during slideshows</extracomment>
      <translation>velocidad actual</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="206"/>
      <source>looping</source>
      <translation>ciclar</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="219"/>
      <source>loop over all files</source>
      <extracomment>Loop over all images during slideshows</extracomment>
      <translation>ciclar todas las imágenes</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="235"/>
      <source>shuffle</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>Orden aleatorio</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="248"/>
      <source>shuffle all files</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>Usar orden aleatorio para todos los archivos</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="264"/>
      <source>subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>subcarpetas</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="277"/>
      <source>include images in subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>incluir imágenes en las subcarpetas</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="294"/>
      <source>status info</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation>información de estado</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="307"/>
      <source>hide status info during slideshow</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation>ocultar información de estado durante pase de diapositivas</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="324"/>
      <source>window buttons</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation>botones de ventana</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="337"/>
      <source>hide window buttons during slideshow</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation>ocultar los botones de ventana durante la presentación</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="354"/>
      <source>music</source>
      <extracomment>The music that is to be played during slideshows</extracomment>
      <translation>música</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="371"/>
      <source>enable music</source>
      <extracomment>Enable music to be played during slideshows</extracomment>
      <translation>habilitar música</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="381"/>
      <source>no file selected</source>
      <translation>no se ha seleccionado archivo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="383"/>
      <source>Click to select music file</source>
      <translation>Click para seleccionar archivo de música</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQSlideshowSetup.qml" line="384"/>
      <source>Click to change music file</source>
      <translation>Click para cambiar archivo de música</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/popout/PQSlideshowControlsPopout.qml" line="33"/>
      <source>Slideshow</source>
      <extracomment>Window title</extracomment>
      <translation>Pase de diapositivas</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="145"/>
      <source>Click to go to the previous image</source>
      <translation>Clic para ir a la imagen anterior</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="179"/>
      <source>Click to pause slideshow</source>
      <translation>Clic para pausar la presentación</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="180"/>
      <source>Click to play slideshow</source>
      <translation>Clic para reproducir la presentación</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="215"/>
      <source>Click to go to the next image</source>
      <translation>Clic para ir a la siguiente imagen</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQSlideshowControls.qml" line="247"/>
      <source>Click to exit slideshow</source>
      <translation>Clic para salir de la presentación</translation>
    </message>
  </context>
  <context>
    <name>startup</name>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="138"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="140"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="142"/>
      <source>Edit with %1</source>
      <extracomment>Used as in &apos;Edit with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Editar con %1</translation>
    </message>
    <message>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="144"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="146"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="148"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="150"/>
      <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="152"/>
      <source>Open in %1</source>
      <extracomment>Used as in &apos;Open with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Abrir en %1</translation>
    </message>
  </context>
  <context>
    <name>statusinfo</name>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="378"/>
      <source>Click anywhere to open a file</source>
      <translation>Pulse en cualquier sitio para abrir un archivo</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="146"/>
      <source>Click and drag to move window around</source>
      <translation>Pulse y arrastre para mover la ventana</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="147"/>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="191"/>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="317"/>
      <source>Click and drag to move status info around</source>
      <translation>Pulse y arrastre para mover la información de estado</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="218"/>
      <source>Click to remove filter</source>
      <translation>Pulse para quitar el filtro</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="282"/>
      <source>Filter:</source>
      <extracomment>This refers to the currently set filter</extracomment>
      <translation>Filtro:</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQStatusInfo.qml" line="369"/>
      <source>Connected to:</source>
      <extracomment>Used in tooltip for the chromecast icon</extracomment>
      <translation>Conectado con:</translation>
    </message>
  </context>
  <context>
    <name>streaming</name>
    <message>
      <location filename="../qml/actions/popout/PQChromeCastManagerPopout.qml" line="32"/>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="38"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>Window title</extracomment>
      <translation>Transmitiendo (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="44"/>
      <source>Connect</source>
      <extracomment>Used as in: Connect to chromecast device</extracomment>
      <translation>Conectar</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="55"/>
      <source>Disconnect</source>
      <extracomment>Used as in: Disconnect from chromecast device</extracomment>
      <translation>Desconectar</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="123"/>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="146"/>
      <source>No devices found</source>
      <extracomment>The devices here are chromecast devices</extracomment>
      <translation>No se encontró ningún dispositivo</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="139"/>
      <source>Status:</source>
      <extracomment>The status refers to whether the chromecast manager is currently scanning or idle</extracomment>
      <translation>Estado:</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="143"/>
      <source>Looking for Chromecast devices</source>
      <translation>Buscando dispositivos Chromecast</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQChromeCastManager.qml" line="145"/>
      <source>Select which device to connect to</source>
      <translation>Seleccione el dispositivo con el cual conectar</translation>
    </message>
  </context>
  <context>
    <name>thumbnails</name>
    <message>
      <location filename="../qml/ongoing/PQThumbnails.qml" line="436"/>
      <source>File size:</source>
      <translation>Tamaño de archivo:</translation>
    </message>
    <message>
      <location filename="../qml/ongoing/PQThumbnails.qml" line="437"/>
      <source>File type:</source>
      <translation>Tipo de archivo:</translation>
    </message>
  </context>
  <context>
    <name>unavailable</name>
    <message>
      <location filename="../qml/manage/PQLoader.qml" line="53"/>
      <source>The chromecast feature is not available in this build of PhotoQt.</source>
      <translation>La funcionalidad Chromecast no está disponible en esta versión de PhotoQt.</translation>
    </message>
    <message>
      <location filename="../qml/manage/PQLoader.qml" line="56"/>
      <source>The location feature is not available in this build of PhotoQt.</source>
      <translation>La funcionalidad de ubicación no está disponible en esta versión de PhotoQt.</translation>
    </message>
  </context>
  <context>
    <name>wallpaper</name>
    <message>
      <location filename="../qml/actions/popout/PQWallpaperPopout.qml" line="32"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="41"/>
      <source>Wallpaper</source>
      <extracomment>Window title</extracomment>
      <translation>Fondo de escritorio</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQWallpaper.qml" line="46"/>
      <source>Set as Wallpaper</source>
      <translation>Establecer como fondo de pantalla</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQWallpaper.qml" line="97"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="113"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="129"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="145"/>
      <location filename="../qml/actions/PQWallpaper.qml" line="161"/>
      <source>Click to choose %1</source>
      <extracomment>%1 is a placeholder for the name of a desktop environment (plasma, xfce, gnome, etc.)</extracomment>
      <translation>Clic para seleccionar %1</translation>
    </message>
    <message>
      <location filename="../qml/actions/PQWallpaper.qml" line="163"/>
      <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="57"/>
      <source>Other</source>
      <extracomment>Used as in: Other Desktop Environment</extracomment>
      <translation>Otro</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="70"/>
      <source>Warning: %1 module not activated</source>
      <translation>Advertencia: el módulo %1 no fue activado</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="78"/>
      <location filename="../qml/actions/wallpaperparts/PQGnome.qml" line="68"/>
      <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="71"/>
      <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="79"/>
      <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="68"/>
      <source>Warning: %1 not found</source>
      <translation>Advertencia: no se encontró %1</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="98"/>
      <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="88"/>
      <source>Set to which screens</source>
      <extracomment>As in: Set wallpaper to which screens</extracomment>
      <translation>En cuáles pantallas establecer</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="111"/>
      <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="100"/>
      <source>Screen</source>
      <extracomment>Used in wallpaper element</extracomment>
      <translation>Pantalla</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="134"/>
      <source>Set to which workspaces</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>En cuáles espacios de trabajo establecer</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="148"/>
      <source>Workspace:</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>Espacio de trabajo:</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQGnome.qml" line="80"/>
      <location filename="../qml/actions/wallpaperparts/PQWindows.qml" line="62"/>
      <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="123"/>
      <source>Choose picture option</source>
      <extracomment>picture option refers to how to format a pictrue when setting it as wallpaper</extracomment>
      <translation>Elija opción de imagen</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="97"/>
      <source>Tool:</source>
      <extracomment>Tool refers to a program that can be executed</extracomment>
      <translation>Herramienta:</translation>
    </message>
    <message>
      <location filename="../qml/actions/wallpaperparts/PQPlasma.qml" line="65"/>
      <source>The image will be set to all screens at the same time.</source>
      <translation>La imagen se establecerá en todas las pantallas al mismo tiempo.</translation>
    </message>
  </context>
</TS>
