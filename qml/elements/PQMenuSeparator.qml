import QtQuick
import QtQuick.Controls

MenuSeparator {
    contentItem: Rectangle {
        implicitWidth: 200
        implicitHeight: 1
        color: PQCLook.inverseColorHighlight
    }
}
