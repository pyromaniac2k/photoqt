import QtQuick

Item {
    property var source
    property bool blurEnabled
    property int blurMax
    property int blur
    property bool autoPaddingEnabled
    property real saturation
}
