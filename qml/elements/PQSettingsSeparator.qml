import QtQuick

Column {
    spacing: 10
    Item { width: 1; height: 5; }
    Rectangle {
        width: setting_top.width
        height: 1
        color: PQCLook.baseColorHighlight
    }
    Item { width: 1; height: 5; }
}
